################################################################################
# Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

metainfo = {
    'interfaces':
    {
        'Storyboard/MotionControlAction': [
            'FollowTrajectoryAction',
            'LaneChangeAction',
            'LaneOffsetAction',
            'LateralDistanceAction',
            'LongitudinalDistanceAction',
            'SpeedAction',
            'SynchronizeAction']
    },
    'selector':
    [
        # root, pattern, ose_type, dependencies
        ('InitActions', '\w+Action$', 'Storyboard/GenericAction', ['Environment']),
        ('Action', '\w+Action$', 'Storyboard/GenericAction', ['Environment']),
        ('ByEntityCondition', '\w+Condition$', 'Storyboard/ByEntityCondition', ['Environment']),
        ('ByValueCondition', '\w+Condition$', 'Storyboard/ByValueCondition', ['Environment'])
    ],
    'blackboard': {
        'set':
            [
                ('ManeuverGroup', 'entityBroker', 'EntityBroker::Ptr',
                 'Actors::SelectTriggeringEntities'),
                ('AbsoluteSpeed', 'environment',
                 'std::shared_ptr<mantle_api::IEnvironment>', 'ctor')
            ],
        'get':
            [
                ('RelativeLanePosition', 'environment',
                 'std::shared_ptr<mantle_api::IEnvironment>'),
                ('RelativeLanePosition', 'entityBroker', 'EntityBroker::Ptr')
            ]
    },
    'dependencies':
    {
        'Environment': {
            'name': 'environment',
            'type': 'std::shared_ptr<mantle_api::IEnvironment>',
            'include': '<MantleAPI/Execution/i_environment.h>'
        }
    },
    'converter':
    {
        'Rule':
        {
            'converter': 'ConvertScenarioRule',
            'return_type': 'Rule<double>',
            'include': '"Conversion/OscToMantle/ConvertScenarioRule.h"',
            'type': 'static',
            'dependencies': [],
            'consumes': ['value', 'dateTime']
        },
        'CoordinateSystem':
        {
            'converter': 'ConvertScenarioCoordinateSystem',
            'return_type': 'CoordinateSystem',
            'include': '"Conversion/OscToMantle/ConvertScenarioCoordinateSystem.h"',
            'type': 'static',
            'dependencies': [],
        },
        'AngleType':
        {
            'converter': 'ConvertScenarioAngleType',
            'return_type': 'AngleType',
            'include': '"Conversion/OscToMantle/ConvertScenarioAngleType.h"',
            'type': 'static',
            'dependencies': [],
        },
        'RelativeDistanceType':
        {
            'converter': 'ConvertScenarioRelativeDistanceType',
            'return_type': 'RelativeDistanceType',
            'include': '"Conversion/OscToMantle/ConvertScenarioRelativeDistanceType.h"',
            'type': 'static',
            'dependencies': [],
        },
        'Actors':
        {
            'converter': 'ConvertScenarioActors',
            'return_type': 'Actors',
            'include': '"Conversion/OscToMantle/ConvertScenarioActors.h"',
            'type': 'static',
            'dependencies': []
        },
        'Entity':
        {
            'converter': 'ConvertScenarioEntity',
            'return_type': 'Entity',
            'include': '"Conversion/OscToMantle/ConvertScenarioEntity.h"',
            'type': 'static',
            'dependencies': []
        },
        'EntityRef':
        {
            'converter': 'ConvertScenarioEntity',
            'return_type': 'Entity',
            'include': '"Conversion/OscToMantle/ConvertScenarioEntity.h"',
            'type': 'static',
            'dependencies': []
        },
        'TriggeringEntities':
        {
            'converter': 'ConvertScenarioEntity',
            'return_type': 'std::vector<std::string>',
            'include': '"Conversion/OscToMantle/ConvertScenarioEntity.h"',
            'type': 'static',
            'dependencies': []
        },
        'Position':
        {
            'converter': 'ConvertScenarioPosition',
            'return_type': 'std::optional<mantle_api::Pose>',
            'include': '"Conversion/OscToMantle/ConvertScenarioPosition.h"',
            'type': 'dynamic',
            'dependencies':
             [
                "Environment"
             ]
        },
        'DirectionOfTravelDistribution':
        {
            'converter': 'ConvertScenarioDirectionOfTravelDistribution',
            'return_type': 'std::optional<DirectionOfTravelDistribution>',
            'include': '"Conversion/OscToMantle/ConvertScenarioDirectionOfTravelDistribution.h"',
            'type': 'static',
            'dependencies': []
        },
        'TrafficDistribution':
        {
            'converter': 'ConvertScenarioTrafficDistribution',
            'return_type': 'TrafficDistribution',
            'include': '"Conversion/OscToMantle/ConvertScenarioTrafficDistribution.h"',
            'type': 'static',
            'dependencies': []
        },
        'TrafficArea':
        {
            'converter': 'ConvertScenarioTrafficArea',
            'return_type': 'TrafficArea',
            'include': '"Conversion/OscToMantle/ConvertScenarioTrafficArea.h"',
            'type': 'static',
            'dependencies': []
        },
        'Range':
        {
            'converter': 'ConvertScenarioRange',
            'return_type': 'std::optional<Range>',
            'include': '"Conversion/OscToMantle/ConvertScenarioRange.h"',
            'type': 'static',
            'dependencies': []
        },
        'SensorReferenceSet':
        {
            'converter': 'ConvertScenarioSensorReferenceSet',
            'return_type': 'SensorReferenceSet',
            'include': '"Conversion/OscToMantle/ConvertScenarioSensorReferenceSet.h"',
            'type': 'static',
            'dependencies': []
        },
        'FinalSpeed':
        {
            'converter': 'ConvertScenarioFinalSpeed',
            'return_type': 'mantle_api::Vec3<units::velocity::meters_per_second_t>',
            'include': '"Conversion/OscToMantle/ConvertScenarioFinalSpeed.h"',
            'type': 'static',
            'dependencies': [
                "Environment"
             ]
        },
        'Trajectory':
        {
            'converter': 'ConvertScenarioTrajectory',
            'return_type': 'mantle_api::Trajectory',
            'include': '"Conversion/OscToMantle/ConvertScenarioTrajectory.h"',
            'type': 'static',
            'dependencies': []
        },
        'ObjectController':
        {
            'converter': 'ConvertScenarioObjectController',
            'return_type': 'ObjectController',
            'include': '"Conversion/OscToMantle/ConvertScenarioObjectController.h"',
            'type': 'static',
            'dependencies': []
        },
        'Controller':
        {
            'converter': 'ConvertScenarioController',
            'return_type': 'Controller',
            'include': '"Conversion/OscToMantle/ConvertScenarioController.h"',
            'type': 'static',
            'dependencies': [],
            'consumes': ['catalogReference']
        },
        'Route':
        {
            'converter': 'ConvertScenarioRoute',
            'return_type': 'Route',
            'include': '"Conversion/OscToMantle/ConvertScenarioRoute.h"',
            'type': 'static',
            'dependencies': [
                "Environment"
            ],
            'consumes': ['catalogReference']
        },
        'DynamicConstraints':
        {
            'converter': 'ConvertScenarioDynamicConstraints',
            'return_type': 'DynamicConstraints',
            'include': '"Conversion/OscToMantle/ConvertScenarioDynamicConstraints.h"',
            'type': 'static',
            'dependencies': []
        },
        'TransitionDynamics':
        {
            'converter': 'ConvertScenarioTransitionDynamics',
            'return_type': 'TransitionDynamics',
            'include': '"Conversion/OscToMantle/ConvertScenarioTransitionDynamics.h"',
            'type': 'static',
            'dependencies': []
        },
        'LaneChangeTarget':
        {
            'converter': 'ConvertScenarioLaneChangeTarget',
            'return_type': 'mantle_api::UniqueId',
            'include': '"Conversion/OscToMantle/ConvertScenarioLaneChangeTarget.h"',
            'type': 'dynamic',
            'dependencies': ['Environment'],
        },
        'LaneOffsetActionDynamics':
        {
            'converter': 'ConvertScenarioLaneOffsetActionDynamics',
            'return_type': 'LaneOffsetActionDynamics',
            'include': '"Conversion/OscToMantle/ConvertScenarioLaneOffsetActionDynamics.h"',
            'type': 'static',
            'dependencies': []
        },
        'LaneOffsetTarget':
        {
            'converter': 'ConvertScenarioLaneOffsetTarget',
            'return_type': 'LaneOffsetTarget',
            'include': '"Conversion/OscToMantle/ConvertScenarioLaneOffsetTarget.h"',
            'type': 'dynamic',
            'dependencies': []
        },
        'SpeedActionTarget':
        {
            'converter': 'ConvertScenarioSpeedActionTarget',
            'return_type': 'SpeedActionTarget',
            'include': '"Conversion/OscToMantle/ConvertScenarioSpeedActionTarget.h"',
            'type': 'dynamic',
            'dependencies': [ "Environment" ]
        },
        'TimeReference':
        {
            'converter': 'ConvertScenarioTimeReference',
            'return_type': 'TimeReference',
            'include': '"Conversion/OscToMantle/ConvertScenarioTimeReference.h"',
            'type': 'static',
            'dependencies': []
        },
        'TrajectoryFollowingMode':
        {
            'converter': 'ConvertScenarioTrajectoryFollowingMode',
            'return_type': 'TrajectoryFollowingMode',
            'include': '"Conversion/OscToMantle/ConvertScenarioTrajectoryFollowingMode.h"',
            'type': 'static',
            'dependencies': []
        },
        'TrajectoryRef':
        {
            'converter': 'ConvertScenarioTrajectoryRef',
            'return_type': 'TrajectoryRef',
            'include': '"Conversion/OscToMantle/ConvertScenarioTrajectoryRef.h"',
            'type': 'static',
            'dependencies': [ "Environment" ],
            'consumes': ['catalogReference']
        },
        'Environment':
        {
            'converter': 'ConvertScenarioEnvironment',
            'return_type': 'Environment',
            'include': '"Conversion/OscToMantle/ConvertScenarioEnvironment.h"',
            'type': 'static',
            'dependencies': []
        },
        'ModifyRule':
        {
            'converter': 'ConvertScenarioModifyRule',
            'return_type': 'ModifyRule',
            'include': '"Conversion/OscToMantle/ConvertScenarioModifyRule.h"',
            'type': 'static',
            'dependencies': []
        },
        'CentralSwarmObject':
        {
            'converter': 'ConvertScenarioCentralSwarmObject',
            'return_type': 'CentralSwarmObject',
            'include': '"Conversion/OscToMantle/ConvertScenarioCentralSwarmObject.h"',
            'type': 'static',
            'dependencies': []
        },
       'Phase':
        {
            'converter': 'ConvertScenarioPhase',
            'return_type': 'Phase',
            'include': '"Conversion/OscToMantle/ConvertScenarioPhase.h"',
            'type': 'static',
            'dependencies': []
       },
       'TrafficDefinition':
        {
            'converter': 'ConvertScenarioTrafficDefinition',
            'return_type': 'TrafficDefinition',
            'include': '"Conversion/OscToMantle/ConvertScenarioTrafficDefinition.h"',
            'type': 'static',
            'dependencies': []
       },
       'TrafficSignalController':
        {
            'converter': 'ConvertScenarioTrafficSignalController',
            'return_type': 'TrafficSignalController',
            'include': '"Conversion/OscToMantle/ConvertScenarioTrafficSignalController.h"',
            'type': 'static',
            'dependencies': []
        },
       'ByObjectType':
        {
            'converter': 'ConvertScenarioByObjectType',
            'return_type': 'ByObjectType',
            'include': '"Conversion/OscToMantle/ConvertScenarioByObjectType.h"',
            'type': 'static',
            'dependencies': []
        },
       'TimeToCollisionConditionTarget':
        {
            'converter': 'ConvertScenarioTimeToCollisionConditionTarget',
            'return_type': 'TimeToCollisionConditionTarget',
            'include': '"Conversion/OscToMantle/ConvertScenarioTimeToCollisionConditionTarget.h"',
            'type': 'static',
            'dependencies':
             [
                "Environment"
             ]
        },
       'StoryboardElement':
        {
            'converter': 'ConvertScenarioStoryboardElement',
            'return_type': 'StoryboardElement',
            'include': '"Conversion/OscToMantle/ConvertScenarioStoryboardElement.h"',
            'type': 'static',
            'dependencies': []
        },
       'ParameterDeclaration':
        {
            'converter': 'ConvertScenarioParameterDeclaration',
            'return_type': 'ParameterDeclaration',
            'include': '"Conversion/OscToMantle/ConvertScenarioParameterDeclaration.h"',
            'type': 'static',
            'dependencies': []
        },
        'MonitorDeclaration':
        {
            'converter': 'ConvertScenarioMonitorDeclaration',
            'return_type': 'MonitorDeclaration',
            'include': '"Conversion/OscToMantle/ConvertScenarioMonitorDeclaration.h"',
            'type': 'static',
            'dependencies': []
        },
        'dateTime':
        {
            'converter': 'ConvertScenarioDateTime',
            'return_type': 'DateTime',
            'include': '"Conversion/OscToMantle/ConvertScenarioDateTime.h"',
            'type': 'static',
            'dependencies': []
        }
    },
    'annotations':
    {
        ('Orientation', 'mantle_api::Orientation3<units::angle::radian_t>',
         '"mappings/orientation.h"'),
        ('Trajectory', 'std::string', '<string>'),
        ('File', 'std::string', '<string>')
    }
}
