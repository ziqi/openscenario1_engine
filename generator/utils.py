################################################################################
# Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

import logging
import argparse

def parse_arguments():
    parser = argparse.ArgumentParser(description='OpenScenarioEngine CodeGenerator')
    parser.add_argument('--clang-format',
                        help="Specifies executable for clang-format",
                        dest='clang_format',
                        default=None)
    parser.add_argument('-l', '--log_file',
                        help="Name of log file",
                        default="generate.log")
    parser.add_argument('-d', '--debug',
                        help="Additionally print debug information",
                        action='store_true')
    parser.add_argument('-c', '--classes',
                        help="Restricts generation scope to given class (e.g. TeleportAction)",
                        default=None,
                        action='extend',
                        nargs="+",
                        type=str)
    parser.add_argument('-f', '--force',
                        help="Forces generation even for detected user specializations",
                        action='store_true')
    return parser.parse_args()


def setup_logging(write_debug_log, log_file):
    logging.basicConfig(
        level=logging.INFO if write_debug_log == False else logging.DEBUG,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.FileHandler(log_file),
            logging.StreamHandler()])
    logging.getLogger().info('Started')
