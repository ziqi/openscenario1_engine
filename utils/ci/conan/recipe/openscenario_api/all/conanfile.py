################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#               2024 Volkswagen AG
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building open scenario api with Conan
################################################################################
import os

from conan import ConanFile
from conan.tools.files import copy, export_conandata_patches, apply_conandata_patches
from conan.tools.scm import Git

required_conan_version = ">=1.53.0"

class OpenScenarioApiConan(ConanFile):
    name = "openscenario_api"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    _repo_source = None
    short_paths = True
    _artifact_path = None
    no_copy_source = True

    def export_sources(self):
        export_conandata_patches(self)

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def _get_url_sha(self):
        if self.version in self.conan_data["sources"]:
            url = self.conan_data["sources"][self.version]["url"]
            sha256 = self.conan_data["sources"][self.version]["sha256"]
        else:
            url = self.conan_data["sources"]["default"]["url"]
            sha256 = self.version
        return url, sha256

    def source(self):
        url, sha256 = self._get_url_sha()
        git = Git(self)
        git.clone(url=url, target=self.name)
        git.folder=self.name
        git.checkout(commit=sha256)
        self._repo_source = os.path.join(self.source_folder, self.name)
        self._artifact_path = os.path.join(self._repo_source, "cpp", "buildArtifact")
        apply_conandata_patches(self)


    def build(self):
        if self.settings.os == "Windows":
            os.chdir(os.path.join(self._repo_source, "cpp"))
            os.system('cmake -Wno-dev --preset="MSYS-shared-release"')
            os.system('cmake --build --preset="Build-MSYS-shared-release"')
        else:
            os.chdir(self._artifact_path)
            os.system('chmod +x generateLinux.sh')
            os.system('./generateLinux.sh shared release make')

    def package(self):
        copy(self, "*", src=os.path.join(self._repo_source, "cpp/common"), dst=os.path.join(self.package_folder, "include/common"))

        copy(self, "*", src=os.path.join(self._repo_source, "cpp/expressionsLib/inc"), dst=os.path.join(self.package_folder, "include/expressionsLib/inc"))

        copy(self, "*", src=os.path.join(self._repo_source, "cpp/externalLibs/Filesystem"), dst=os.path.join(self.package_folder, "include/externalLibs/Filesystem"))
        copy(self, "*", src=os.path.join(self._repo_source, "cpp/externalLibs/TinyXML2"), dst=os.path.join(self.package_folder, "include/externalLibs/TinyXML2"))

        copy(self, "*", src=os.path.join(self._repo_source, "cpp/openScenarioLib/src"), dst=os.path.join(self.package_folder, "include/openScenarioLib/src"))

        copy(self, "*", src=os.path.join(self._repo_source, "cpp/openScenarioLib/generated"), dst=os.path.join(self.package_folder, "include/openScenarioLib/generated"))

        copy(self, "*", src=os.path.join(self._repo_source, "cpp/build/cgReleaseMakeShared/antlr4_runtime/src/antlr4_runtime/runtime/Cpp/dist"), dst=os.path.join(self.package_folder, "lib"))
        copy(self, "lib*", src=os.path.join(self._repo_source, "cpp/build/cgReleaseMakeShared/openScenarioLib"), dst=os.path.join(self.package_folder, "lib"))
        copy(self, "lib*", src=os.path.join(self._repo_source, "cpp/build/cgReleaseMakeShared/expressionsLib"), dst=os.path.join(self.package_folder, "lib"))

        os.chdir(self.package_folder)
        os.system('find . -name "*.cpp" -exec rm {} \;')
