# *****************************************************************************
# Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# ******************************************************************************

cmake_minimum_required(VERSION 3.16.3)

option(USE_CCACHE "Automatically try to use CCACHE if available" ON)
# Add the custom CMake modules to CMake's module path
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/cmake)

set(PROJECT_NAME OpenScenarioEngine)
project(${PROJECT_NAME} VERSION 0.4)

# Set a default build type as release if none is specified
if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif()

# YASE
find_package(Yase REQUIRED)

# units https://github.com/nholthaus/units (dependency of MantleAPI)
find_package(units REQUIRED)

# googlemock
include(CPM)
CPMAddPackage(
  NAME GTest
  GITHUB_REPOSITORY google/googletest
  GIT_TAG v1.14.0
  VERSION 1.14.0
  OPTIONS "INSTALL_GTEST OFF" "gtest_force_shared_crt ON"
)

# MantleAPI https://gitlab.eclipse.org/eclipse/simopenpass/scenario_api
find_package(MantleAPI REQUIRED)

# see https://stackoverflow.com/a/58495612
set(CMAKE_INSTALL_RPATH $ORIGIN)

add_library(${PROJECT_NAME} SHARED "")

target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_17)

find_package(OpenScenarioAPI REQUIRED MODULE)
find_package(Antlr4Runtime REQUIRED MODULE)

include(cmake/generated_files.cmake)

target_sources(
  ${PROJECT_NAME} PRIVATE ${${PROJECT_NAME}_SOURCES} 
                          ${${PROJECT_NAME}_HEADERS}
                          ${CMAKE_CURRENT_LIST_DIR}/include/OpenScenarioEngine/OpenScenarioEngine.h
)

target_include_directories(
  ${PROJECT_NAME}
  PUBLIC $<INSTALL_INTERFACE:include>
  PRIVATE gen src 
            $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
)

target_link_libraries(${PROJECT_NAME} PUBLIC openscenario_api::shared 
                                             antlr4_runtime::shared 
                                             Yase::agnostic_behavior_tree 
                                             units::units 
                                             MantleAPI::MantleAPI)

if(USE_CCACHE)
  find_program(CCACHE_FOUND ccache)

  if(CCACHE_FOUND)
    message("Found ccache")
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
  endif(CCACHE_FOUND)
endif()

# Add -O1 to remove optimizations when using gcc and in debug mode
if(CMAKE_BUILD_TYPE STREQUAL "Debug")
  if(CMAKE_COMPILER_IS_GNUCC)
    message("Disable optimization")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O1")
    set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -O1")
  endif(CMAKE_COMPILER_IS_GNUCC)
endif()

include(GenerateExportHeader)
generate_export_header(${PROJECT_NAME})

install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Targets)

install(FILES ${CMAKE_CURRENT_LIST_DIR}/include/OpenScenarioEngine/OpenScenarioEngine.h
        DESTINATION include/OpenScenarioEngine
)

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/cmake/${PROJECT_NAME}ConfigVersion.cmake"
  VERSION ${Upstream_VERSION}
  COMPATIBILITY AnyNewerVersion
)

export(
  EXPORT ${PROJECT_NAME}Targets
  FILE "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/cmake/${PROJECT_NAME}Targets.cmake"
  NAMESPACE OpenScenarioEngine::
)

configure_file(
  cmake/${PROJECT_NAME}Config.cmake "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/cmake/${PROJECT_NAME}Config.cmake"
  COPYONLY
)

set(ConfigPackageLocation lib/cmake/${PROJECT_NAME})

install(FILES cmake/FindAntlr4Runtime.cmake cmake/FindOpenScenarioAPI.cmake DESTINATION ${ConfigPackageLocation}/deps)

install(FILES cmake/${PROJECT_NAME}Config.cmake DESTINATION ${ConfigPackageLocation})

install(
  EXPORT ${PROJECT_NAME}Targets
  NAMESPACE OpenScenarioEngine::
  DESTINATION ${ConfigPackageLocation}
)

# DEMO
add_executable(${PROJECT_NAME}Demo EXCLUDE_FROM_ALL ${CMAKE_CURRENT_LIST_DIR}/demo/main.cpp)

target_include_directories(
  ${PROJECT_NAME}Demo
  PRIVATE src
          $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
)

add_dependencies(${PROJECT_NAME}Demo ${PROJECT_NAME})
target_compile_features(${PROJECT_NAME}Demo PRIVATE cxx_std_17)

target_link_libraries(${PROJECT_NAME}Demo ${PROJECT_NAME} GTest::gmock_main)

# ---------------

enable_testing()

add_executable(${PROJECT_NAME}Test "")

target_sources(
  ${PROJECT_NAME}Test
  PRIVATE 
          ${CMAKE_CURRENT_LIST_DIR}/tests/builders/ActionBuilder.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/builders/condition_builder.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/builders/PositionBuilder.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioAbsoluteTargetLaneTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioCentralSwarmObjectTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioCoordinateSystemTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioEnvironmentTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioLateralDisplacementTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioLightStateTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioLightTypeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioPositionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioRelativeDistanceTypeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioRelativeTargetLaneTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioRuleTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioSpeedActionTargetTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioTimeReferenceTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioTimeToCollisionConditionTargetTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioTrafficDefinitionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioTrajectoryRefTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Node/AnyTriggeringEntityNodeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Node/ByEntityConditionNodeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Node/ConditionGroupsNodeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Node/ConditionNodeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Node/ConditionsNodeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Node/EntityConditionNodeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Node/TriggerableCompositeNodeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/OpenScenarioEngineTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/ActivateControllerActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/ByEntityCondition/DistanceConditionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/ByEntityCondition/ReachPositionConditionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/ByEntityCondition/RelativeDistanceConditionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/ByEntityCondition/RelativeSpeedConditionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/ByEntityCondition/SpeedConditionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/ByEntityCondition/TimeHeadwayConditionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/ByEntityCondition/TimeToCollisionConditionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/ByValueCondition/SimulationTimeConditionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/ByValueCondition/UserDefinedValueConditionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/AcquirePositionActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/AssignControllerActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/AssignRouteActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/CustomCommandActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/EnvironmentActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/LightStateActionTests.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/TeleportActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/TrafficSignalStateActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/TrafficSinkActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/TrafficSwarmActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/VisibilityActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/MotionControlAction/FollowTrajectoryActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/MotionControlAction/LaneChangeActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/MotionControlAction/LaneOffsetActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/MotionControlAction/LateralDistanceActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/MotionControlAction/LongitudinalDistanceActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/MotionControlAction/MotionControlActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/MotionControlAction/SpeedActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/TrafficSignalParsing/TrafficPhaseNodeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/TrafficSignalParsing/TreeGenerationTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Utils/ConstantsTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Utils/ControllerCreatorTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Utils/EllipseTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Utils/EntityCreatorTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Utils/EntityUtilsTest.cpp
)

add_custom_command(TARGET ${PROJECT_NAME}Test
                    POST_BUILD
                    COMMAND ${CMAKE_COMMAND} -E copy_directory
                      ${CMAKE_CURRENT_SOURCE_DIR}/tests/data
                      ${CMAKE_CURRENT_BINARY_DIR}/tests/data
)

get_property(MantleAPI_INCLUDE_DIR TARGET MantleAPI::MantleAPI PROPERTY INTERFACE_INCLUDE_DIRECTORIES)

target_compile_features(${PROJECT_NAME}Test PRIVATE cxx_std_17)
target_include_directories(
  ${PROJECT_NAME}Test
  PRIVATE gen
          src
          tests
          ${MantleAPI_INCLUDE_DIR}/../test
          $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
)

target_link_libraries(
  ${PROJECT_NAME}Test
  PRIVATE ${PROJECT_NAME}
          antlr4_runtime::shared
          GTest::gmock_main
          pthread
)

add_test(NAME ${PROJECT_NAME}Test COMMAND ${PROJECT_NAME}Test)

if(WIN32)
  get_property(OPENSCENARIO_API_SHARED_LIBRARY_DIR TARGET openscenario_api::shared PROPERTY IMPORTED_LOCATION)
  get_filename_component(OPENSCENARIO_API_SHARED_LIBRARY_DIR ${OPENSCENARIO_API_SHARED_LIBRARY_DIR} DIRECTORY)

  get_property(EXPRESSIONS_SHARED_LIBRARY_DIR TARGET expressions::shared PROPERTY IMPORTED_LOCATION)
  get_filename_component(EXPRESSIONS_SHARED_LIBRARY_DIR ${EXPRESSIONS_SHARED_LIBRARY_DIR} DIRECTORY)

  get_property(ANTLR4_RUNTIME_SHARED_LIBRARY_DIR TARGET antlr4_runtime::shared PROPERTY IMPORTED_LOCATION)
  get_filename_component(ANTLR4_RUNTIME_SHARED_LIBRARY_DIR ${ANTLR4_RUNTIME_SHARED_LIBRARY_DIR} DIRECTORY)

  set(TEST_DEP_PATHS ${ANTLR4_RUNTIME_SHARED_LIBRARY_DIR} ${OPENSCENARIO_API_SHARED_LIBRARY_DIR} ${EXPRESSIONS_SHARED_LIBRARY_DIR} $ENV{PATH})
  string(REGEX REPLACE "\;" "\\\;" TEST_DEP_PATHS "${TEST_DEP_PATHS}")

  set_tests_properties(${PROJECT_NAME}Test PROPERTIES ENVIRONMENT "PATH=${TEST_DEP_PATHS}")

endif()
