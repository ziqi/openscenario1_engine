/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Common/i_logger.h>
#include <MantleAPI/Common/log_utils.h>
#include <MantleAPI/Test/test_utils.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <filesystem>
#include <iostream>
#include <string>

#include "OpenScenarioEngine/OpenScenarioEngine.h"

using namespace units::literals;

using testing::_;
using testing::A;
using testing::Invoke;
using testing::NiceMock;
using testing::Return;
using testing::ReturnRef;

using namespace std::string_literals;

struct ConsoleLogger : public mantle_api::ILogger
{
  mantle_api::LogLevel GetCurrentLogLevel() const noexcept override
  {
    return mantle_api::LogLevel::kTrace;
  }

  void Log(mantle_api::LogLevel level, std::string_view message) noexcept override
  {
    std::cout << level << ":\n"
              << message << "\n";
  }
};

int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    std::cout << "Usage: "s << argv[0] << " <path_to_demo>/example/scenario.xosc\n"s;
    return 0;
  }

  auto consoleLogger = std::make_shared<ConsoleLogger>();
  std::filesystem::path scenario_file{argv[1]};

  if (!std::filesystem::exists(scenario_file))
  {
    consoleLogger->Log(mantle_api::LogLevel::kError, scenario_file.string() + " does not exist"s);
    return -1;
  }

  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();

  auto& mockEntityRepository = static_cast<mantle_api::MockEntityRepository&>(mockEnvironment->GetEntityRepository());

  mantle_api::MockVehicle mockVehicle1;
  ON_CALL(mockEntityRepository, Create("Vehicle1", A<const mantle_api::VehicleProperties&>())).WillByDefault(ReturnRef(mockVehicle1));

  mantle_api::MockVehicle mockVehicle2;
  ON_CALL(mockEntityRepository, Create("Vehicle2", A<const mantle_api::VehicleProperties&>())).WillByDefault(ReturnRef(mockVehicle2));

  auto& mockControllerRepository = mockEnvironment->GetControllerRepository();

  mantle_api::MockController mockController;
  auto controller_name("demo_controller"s);
  ON_CALL(mockControllerRepository, Create(_)).WillByDefault(ReturnRef(mockController));
  ON_CALL(mockController, GetName()).WillByDefault(ReturnRef(controller_name));
  ON_CALL(mockController, GetUniqueId()).WillByDefault(Return(1234));

  OpenScenarioEngine::v1_3::OpenScenarioEngine openScenarioEngine(scenario_file.string(), mockEnvironment, consoleLogger);

  mantle_api::Time current_time{0_s};
  ON_CALL(*mockEnvironment, GetSimulationTime())
      .WillByDefault(Invoke([&current_time]() { return current_time; }));
  ON_CALL(*mockEnvironment, HasControlStrategyGoalBeenReached(_, mantle_api::ControlStrategyType::kFollowVelocitySpline))
      .WillByDefault(Invoke([&current_time]() -> bool { return current_time > 5.5_s; }));

  try
  {
    openScenarioEngine.Init();
    while (!openScenarioEngine.IsFinished())
    {
      openScenarioEngine.Step();
      current_time += 500_ms;
    }
  }
  catch(const std::runtime_error& e)
  {
    consoleLogger->Log(mantle_api::LogLevel::kError, e.what());
  }
}
