load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

ECLIPSE_GITLAB = "https://gitlab.eclipse.org/eclipse"

MANTLE_API_TAG = "v6.0.0"
UNITS_TAG = "2.3.3"
YASE_TAG = "v0.0.1"

def osc_engine_deps():
    """Load dependencies"""
    maybe(
        http_archive,
        name = "mantle_api",
        url = ECLIPSE_GITLAB + "/openpass/mantle-api/-/archive/{tag}/mantle-api-{tag}.tar.gz".format(tag = MANTLE_API_TAG),
        sha256 = "d6365096eceefd6848f7429382279b337e9cb8292a984ef54137dbdebd4ce599",
        strip_prefix = "mantle-api-{tag}".format(tag = MANTLE_API_TAG),
    )

    maybe(
        http_archive,
        name = "units_nhh",
        url = "https://github.com/nholthaus/units/archive/refs/tags/v{tag}.tar.gz".format(tag = UNITS_TAG),
        sha256 = "b1f3c1dd11afa2710a179563845ce79f13ebf0c8c090d6aa68465b18bd8bd5fc",
        strip_prefix = "./units-{tag}".format(tag = UNITS_TAG),
        build_file = "//bazel:units.BUILD",
    )

    maybe(
        http_archive,
        name = "yase",
        url = ECLIPSE_GITLAB + "/openpass/yase/-/archive/{tag}/yase-{tag}.tar.gz".format(tag = YASE_TAG),
        sha256 = "243d710172dff4c4e7ab95e5bd68a6d8335cf8a3fdd1efa87d118250e3b85ee3",
        strip_prefix = "yase-{tag}".format(tag = YASE_TAG),
    )

    http_archive(
        name = "open_scenario_parser",
        build_file = "//bazel:openscenario_api.BUILD",
        url = "https://github.com/RA-Consulting-GmbH/openscenario.api.test/releases/download/v1.4.0/OpenSCENARIO_API_LinuxSharedRelease_2024.11.18.tgz",
        sha256 = "7a4cdb82ccaaeed5fccf12efd94cf4f9c9d3ac0fc7d7feedd4c0babe2404f853",
        strip_prefix = "OpenSCENARIO_API_LinuxSharedRelease",
    )
