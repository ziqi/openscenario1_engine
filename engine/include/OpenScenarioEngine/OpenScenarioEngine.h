/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <ApiClassInterfacesV1_3.h>
#include <MantleAPI/Common/i_logger.h>
#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Execution/i_scenario_engine.h>
#include <openScenarioLib/src/common/SimpleMessageLogger.h>

#include <memory>
#include <string>

namespace OpenScenarioEngine::v1_3
{
class ControllerCreator;
class EntityCreator;
class IProbabilityService;

namespace Node
{
class RootNode;
}  // namespace Node

class OpenScenarioEngine : public mantle_api::IScenarioEngine
{
public:
  using OpenScenarioPtr = std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IOpenScenario>;
  using ScenarioDefinitionPtr = std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IScenarioDefinition>;
  using SimpleMessageLoggerPtr = std::shared_ptr<NET_ASAM_OPENSCENARIO::SimpleMessageLogger>;

  /// Creates an instance of the OpenSCENARIO Engine (OSE)
  ///
  /// @param scenario_file_path  path to the scenario description (*.xosc)
  /// @param environment         interface to the environment, which is controlled by the OSE
  /// @param logger              instance of a potential logger
  /// @param initial_seed        initial seed for internally used random generators
  ///                            (defaults to 0) @sa SetManualRandomSeed
  OpenScenarioEngine(
      const std::string& scenario_file_path,
      std::shared_ptr<mantle_api::IEnvironment> environment,
      std::shared_ptr<mantle_api::ILogger> logger = nullptr,
      unsigned int initial_seed = 0);

  /// Parses and validates the scenario and referenced catalogs.
  /// @note A map file referenced from the scenario is not parsed.
  ///       The map file path can be retrieved after calling this method with GetScenarioInfo().
  /// @throw If the scenario or catalogs contain errors.
  void Init() override;

  /// Sets up all dynamic content
  ///
  /// SetupDynamicContent can be called multiple times to reset the dynamic content
  /// of the engine, allowing to reuse a loaded scenario multiple times
  /// without the need for re-validation and map loading. Consequently
  /// it will reset the all entities, controllers and the internal
  /// storyboard.
  ///
  /// @note SetupDynamicContent will auto-increment the initial random at every consecutive
  ///       every call, potentially leading to altered but deterministic results
  ///       Use OverrideRandomSeed to for manual control.
  void SetupDynamicContent() override;

  [[nodiscard]] mantle_api::ScenarioInfo GetScenarioInfo() const override;

  void Step(mantle_api::Time delta_time) override;

  [[nodiscard]] std::optional<mantle_api::Time> GetDesiredDeltaTime() const noexcept override;

  [[nodiscard]] bool IsFinished() const override;

  /// Enable override for user defined controllers
  ///
  /// If activated, will enforce creation of an user defined controller
  /// for every entity, which name is "Ego", "Host", or starts with "M1:"
  /// (compatibility for converted dSPACE Model Desk scenarios).
  /// @note This value will not be reset at Init()
  void ActivateExternalHostControl() override;

  int ValidateScenario() override;

  OpenScenarioPtr GetScenario() { return scenario_data_ptr_; }

  /// Sets a manual random seed, which will be used at initialization
  ///
  /// The given value will be hold until ResetProbabilityService is called
  /// during Inititialization. It has no direct impact on a currently running
  /// OpenScenarioEngine. Use this method, when you like to control the
  /// seed manually over consecutive invocations without reloading the scenario
  /// (= skipping validation of the scenario and loading the map), e.g.
  /// Construct Engine -> ...
  /// 1st Invocation:  -> SetRandomSeed() -> Init() -> Execute() -> ...
  /// 2nd Invocation:  -> SetRandomSeed() -> Init() -> Execute() -> ...
  ///
  /// Note that the engine is invoked with a random seed per default, which will
  /// be incremented in between two initializations unless overridden.
  /// @see Init, @see ResetProbabilityService
  ///
  /// @param seed   New random seed
  void OverrideRandomSeed(unsigned int seed);

protected:
  std::string scenario_file_path_;
  OpenScenarioPtr scenario_data_ptr_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::SimpleMessageLogger> catalog_message_logger_;
  std::shared_ptr<NET_ASAM_OPENSCENARIO::SimpleMessageLogger> message_logger_;
  void LoadScenarioData();

private:
  /// If the path cannot be resolved by the scenario loader, override this function to resolve the path before loading
  /// in ParseScenarioFile().
  ///
  /// @param Scenario file in any given form
  /// @return Resolved path to the scenario file
  [[nodiscard]] virtual std::string ResolveScenarioPath(const std::string& scenario_path) const
  {
    return scenario_path;
  }

  /// If the map path cannot be resolved by the environment, override this function to resolve the path before calling
  /// the environment's CreateMap().
  ///
  /// @param Scenario file in any given form
  /// @return Resolved path to the scenario file
  [[nodiscard]] virtual std::string ResolveMapPath(const std::string& map_path) const
  {
    return map_path;
  }

  /// If a different standard output system is used, override this function with the proper output
  ///
  /// @param message_logger Where the messages are extracted from
  virtual void LogParsingMessages(SimpleMessageLoggerPtr message_logger);

  void ParseScenarioFile();

  /// Instantly sets the random seed of the probability service
  ///
  /// - At first call this method will set the random seed to the given
  ///   value during initialization of the OSE. The initial seed will
  ///   then be incremented at every consecutive call.
  ///
  /// - If a SetRandomSeed has been called before, the given value will
  ///   be used instead
  void ResetProbabilityService();

  /// Reset all entities and create new according to scenario description
  void ResetAndCreateEntities();

  /// Reset all controllers and create new according to scenario description
  void ResetAndCreateControllers();

  /// Reset the storyboard and create new according to scenario description
  void ResetAndCreateStoryboard();

  /// The default environmental conditions are excellent, no precipitation, illumination as other,
  /// humidity at 60 percent, temperature at 15 degrees celsius and atmospheric pressure at sea level.
  /// The time of the day is considered as midday.
  /// The default routing behavior according to the standard is random.
  void SetEnvironmentDefaults();

  void SetScenarioDefinitionPtr(OpenScenarioPtr open_scenario_ptr);
  [[nodiscard]] mantle_api::Time GetDuration() const;

  /// Read the map details (e.g used area) and return it
  std::unique_ptr<mantle_api::MapDetails> GetMapDetails() const;

  std::shared_ptr<mantle_api::IEnvironment> environment_;
  std::shared_ptr<EntityCreator> entity_creator_;
  std::shared_ptr<ControllerCreator> controller_creator_;
  std::shared_ptr<IProbabilityService> probability_service_;
  std::shared_ptr<mantle_api::ILogger> logger_{nullptr};
  std::shared_ptr<Node::RootNode> root_node_{nullptr};
  const mantle_api::ICoordConverter* coordinate_converter_{nullptr};
  ScenarioDefinitionPtr scenario_definition_ptr_{nullptr};

  bool finished_{false};

  std::optional<unsigned int> random_seed_value_override_{std::nullopt};
};
}  // namespace OpenScenarioEngine::v1_3
