################################################################################
# Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
#
# Find Package Adapter for the OpenScenarioAPI
#
# Creates the follwoing imported targets (if available):
# - openscenario_api::shared

set(OPENSCENARIO_API_SHARED_NAMES
  OpenScenarioLib.lib
  libOpenScenarioLib.dll.a
  libOpenScenarioLib.so
)

find_library(OPENSCENARIO_API_SHARED_LIBRARY NAMES ${OPENSCENARIO_API_SHARED_NAMES}
  PATHS
    ${PREFIX_PATH}
    /usr/local
    /usr
  PATH_SUFFIXES
    lib
    lib64
)

set(EXPRESSIONS_SHARED_LIBRARY_NAMES
  ExpressionsLib.lib
  libExpressionsLib.dll.a
  libExpressionsLib.so
)

find_library(EXPRESSIONS_SHARED_LIBRARY NAMES ${EXPRESSIONS_SHARED_LIBRARY_NAMES}
  PATHS
    ${PREFIX_PATH}
    /usr/local
    /usr
  PATH_SUFFIXES
    lib
    lib64
)

if(OPENSCENARIO_API_SHARED_LIBRARY AND EXPRESSIONS_SHARED_LIBRARY)
  get_filename_component(OPENSCENARIO_API_SHARED_LIBRARY_DIR "${OPENSCENARIO_API_SHARED_LIBRARY}" DIRECTORY)

  if(EXISTS ${OPENSCENARIO_API_SHARED_LIBRARY_DIR}/../../../../openScenarioLib/generated)
    set(OPENSCENARIO_API_CPP_BUILD_DIR ${OPENSCENARIO_API_SHARED_LIBRARY_DIR}/../../../..)
  elseif(EXISTS ${OPENSCENARIO_API_SHARED_LIBRARY_DIR}/../include/openScenarioLib/generated)
    set(OPENSCENARIO_API_CPP_BUILD_DIR ${OPENSCENARIO_API_SHARED_LIBRARY_DIR}/../include)
  elseif(EXISTS ${OPENSCENARIO_API_SHARED_LIBRARY_DIR}/../../../openScenarioLib/generated)
    set(OPENSCENARIO_API_CPP_BUILD_DIR ${OPENSCENARIO_API_SHARED_LIBRARY_DIR}/../../..)
  else()
    message(FATAL_ERROR "Unable to detect header files for OPENSCENARIO_API")
  endif()

  set(EXPRESSIONS_CPP_BUILD_DIR_HDR ${OPENSCENARIO_API_CPP_BUILD_DIR}/expressionsLib/inc)

  set(OPENSCENARIO_API_CPP_BUILD_DIR_HDR
    ${OPENSCENARIO_API_CPP_BUILD_DIR}
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/common
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/externalLibs/Filesystem
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/externalLibs/TinyXML2
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/generated/v1_3/api
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/generated/v1_3/api/writer
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/generated/v1_3/checker
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/generated/v1_3/checker/impl
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/generated/v1_3/common
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/generated/v1_3/impl
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/src/api
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/src/checker/tree
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/src/common
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/src/loader
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/src/expression
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/src/impl
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/src/parameter
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/src/parser
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/src/checker
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/src/v1_3/checker
    # old reference from IScenarioLoader.h?
    ${OPENSCENARIO_API_CPP_BUILD_DIR}/openScenarioLib/generated/v1_0/api)

  add_library(expressions::shared IMPORTED SHARED)
  set_target_properties(expressions::shared
    PROPERTIES
    IMPORTED_LOCATION ${EXPRESSIONS_SHARED_LIBRARY}
    IMPORTED_IMPLIB ${EXPRESSIONS_SHARED_LIBRARY}
    INTERFACE_INCLUDE_DIRECTORIES "${EXPRESSIONS_CPP_BUILD_DIR_HDR}"
    INTERFACE_LINK_DIRECTORIES ${EXPRESSIONS_SHARED_LIBRARY}
    )

  add_library(openscenario_api::shared IMPORTED SHARED)
  set_target_properties(openscenario_api::shared
                        PROPERTIES
                          IMPORTED_LOCATION ${OPENSCENARIO_API_SHARED_LIBRARY}
                          IMPORTED_IMPLIB ${OPENSCENARIO_API_SHARED_LIBRARY}
                          INTERFACE_INCLUDE_DIRECTORIES "${OPENSCENARIO_API_CPP_BUILD_DIR_HDR}"
                          INTERFACE_LINK_DIRECTORIES ${OPENSCENARIO_API_SHARED_LIBRARY_DIR}
                          INTERFACE_LINK_LIBRARIES "expressions::shared;antlr4_runtime::shared"
                          )
  set(OpenScenarioAPI_FOUND 1 CACHE INTERNAL "OpenScenarioAPI found" FORCE)
else()
  message(FATAL_ERROR "Didn't find OpenScenario and Expressions library (shared)\nMaybe you forgot to add the corresponding path to CMAKE_PREFIX_PATH")
endif()

unset(OPENSCENARIO_API_CPP_BUILD_DIR)
unset(OPENSCENARIO_API_CPP_BUILD_DIR_HDR)
unset(EXPRESSIONS_CPP_BUILD_DIR_HDR)
unset(OPENSCENARIO_API_SHARED_LIBRARY)
unset(EXPRESSIONS_SHARED_LIBRARY)
unset(OPENSCENARIO_API_SHARED_NAMES)
unset(EXPRESSIONS_SHARED_LIBRARY_NAMES)
