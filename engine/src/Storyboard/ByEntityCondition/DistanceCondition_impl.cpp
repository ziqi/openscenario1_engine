/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/ByEntityCondition/DistanceCondition_impl.h"

#include <string>

#include "Utils/EntityUtils.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{
namespace detail
{

template <typename Tag>
class IssueOnce
{
public:
  constexpr IssueOnce(std::string_view message) noexcept
  {
    if (!error_issued)
    {
      Logger::Error(message);
      error_issued = true;
    }
  }

private:
  static inline bool error_issued{false};
};

[[nodiscard]] bool CheckFreespace(bool freespace)
{
  if (freespace)
  {
    IssueOnce<decltype(CheckFreespace)>("DistanceCondition: Freespace not implemented. Falling back to default (false).");
  }

  return false;
}

}  // namespace detail

bool DistanceCondition::IsSatisfied() const
{
  const auto pose = values.GetPosition();
  if (!pose)
  {
    throw std::runtime_error("DistanceCondition: DistanceCondition cannot be satisfied (no Position defined). Please adjust scenario.");
  }

  [[maybe_unused]] auto freespace = detail::CheckFreespace(values.freespace);

  if (values.relativeDistanceType == RelativeDistanceType::kLongitudinal &&
      values.coordinateSystem == CoordinateSystem::kEntity)
  {
    const auto& triggeringEntity = EntityUtils::GetEntityByName(mantle.environment, values.triggeringEntity);
    const auto distance = EntityUtils::CalculateRelativeLongitudinalDistance(
        mantle.environment, triggeringEntity, pose.value().position);
    return values.rule.IsSatisfied(distance.value());
  }

  Logger::Error("DistanceCondition: Selected relativeDistanceType or coordinateSystem not implemented yet. Only \"longitudinal\" distances and \"entity\" coordinate systems are supported for now. Returning true.");
  return true;
}
}  // namespace OpenScenarioEngine::v1_3
