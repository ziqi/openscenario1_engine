/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <utility>

#include "Storyboard/MotionControlAction/LaneChangeAction_base.h"

namespace OpenScenarioEngine::v1_3
{
class LaneChangeAction : public LaneChangeActionBase
{
public:
  LaneChangeAction(Values values, Interfaces interfaces)
      : LaneChangeActionBase{std::move(values), std::move(interfaces)} {};

  virtual ~LaneChangeAction() = default;

  void SetControlStrategy() override;
  bool HasControlStrategyGoalBeenReached(const std::string& actor) override;
  [[nodiscard]] mantle_api::MovementDomain GetMovementDomain() const override;

private:
  void SetupControlStrategy();

  std::shared_ptr<mantle_api::PerformLaneChangeControlStrategy> control_strategy_{
      std::make_shared<mantle_api::PerformLaneChangeControlStrategy>()};
};

}  // namespace OpenScenarioEngine::v1_3