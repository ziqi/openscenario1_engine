/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>
#include <utility>

#include "Storyboard/MotionControlAction/LongitudinalDistanceAction_impl.h"
#include "Storyboard/MotionControlAction/MotionControlAction.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_3::Node
{
class LongitudinalDistanceAction : public yase::ActionNode
{
public:
  explicit LongitudinalDistanceAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILongitudinalDistanceAction> longitudinalDistanceAction)
      : yase::ActionNode{"LongitudinalDistanceAction"},
        longitudinalDistanceAction_{std::move(longitudinalDistanceAction)}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const auto entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_3::MotionControlAction<OpenScenarioEngine::v1_3::LongitudinalDistanceAction>>(
        OpenScenarioEngine::v1_3::LongitudinalDistanceAction::Values{
            entityBroker->GetEntities(),
            longitudinalDistanceAction_->GetContinuous(),
            longitudinalDistanceAction_->GetFreespace(),
            [=]() {
              return ConvertScenarioLongitudinalDistance(
                  environment,
                  longitudinalDistanceAction_->IsSetDistance(),
                  longitudinalDistanceAction_->GetDistance(),
                  longitudinalDistanceAction_->IsSetTimeGap(),
                  longitudinalDistanceAction_->GetTimeGap(),
                  longitudinalDistanceAction_->GetEntityRef());
            },
            ConvertScenarioLongitudinalDisplacement(longitudinalDistanceAction_->GetDisplacement()),
            ConvertScenarioDynamicConstraints(longitudinalDistanceAction_->IsSetDynamicConstraints(), longitudinalDistanceAction_->GetDynamicConstraints()),
            ConvertScenarioEntity(longitudinalDistanceAction_->GetEntityRef()),
            ConvertScenarioCoordinateSystem(longitudinalDistanceAction_->GetCoordinateSystem())},
        OpenScenarioEngine::v1_3::LongitudinalDistanceAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_3::MotionControlAction<OpenScenarioEngine::v1_3::LongitudinalDistanceAction>> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILongitudinalDistanceAction> longitudinalDistanceAction_;
};

}  // namespace OpenScenarioEngine::v1_3::Node