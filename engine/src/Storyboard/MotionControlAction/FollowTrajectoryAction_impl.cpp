/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/MotionControlAction/FollowTrajectoryAction_impl.h"

#include <MantleAPI/Traffic/entity_helper.h>

#include "Utils/EntityUtils.h"

namespace OpenScenarioEngine::v1_3
{

namespace detail
{

mantle_api::Trajectory ConvertPolyLine(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const std::string& entity,
    TrajectoryRef trajectoryRef)
{
  const auto& refEntity = EntityUtils::GetEntityByName(environment, entity);
  const auto geometric_center = refEntity.GetProperties()->bounding_box.geometric_center;

  // modifies the copy of the trajectory ref!
  for (auto& polyLinePoint : std::get<mantle_api::PolyLine>(trajectoryRef->type))
  {
    polyLinePoint.pose.position = environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
        polyLinePoint.pose.position,
        polyLinePoint.pose.orientation,
        geometric_center);
  }
  return trajectoryRef.value();
}

}  // namespace detail

void FollowTrajectoryAction::SetControlStrategy()
{
  if (!values.trajectoryRef)
  {
    throw std::runtime_error(
        "FollowTrajectoryAction does not contain TrajectoryRef. "
        "Note: Deprecated element Trajectory is not supported.");
  }

  for (const auto& entity : values.entities)
  {
    auto control_strategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
    control_strategy->timeReference = values.timeReference;
    control_strategy->movement_domain = values.timeReference
                                            ? mantle_api::MovementDomain::kBoth
                                            : mantle_api::MovementDomain::kLateral;
    control_strategy->trajectory = detail::ConvertPolyLine(mantle.environment, entity, values.trajectoryRef);
    const auto entity_id = mantle.environment->GetEntityRepository().Get(entity)->get().GetUniqueId();
    mantle.environment->UpdateControlStrategies(entity_id, {control_strategy});
  }
}

bool FollowTrajectoryAction::HasControlStrategyGoalBeenReached(const std::string& actor)
{
  if (auto entity = mantle.environment->GetEntityRepository().Get(actor))
  {
    return mantle.environment->HasControlStrategyGoalBeenReached(
        entity.value().get().GetUniqueId(),
        mantle_api::ControlStrategyType::kFollowTrajectory);
  }
  return false;
}

mantle_api::MovementDomain FollowTrajectoryAction::GetMovementDomain() const
{
  return mantle_api::MovementDomain::kBoth;
}

}  // namespace OpenScenarioEngine::v1_3