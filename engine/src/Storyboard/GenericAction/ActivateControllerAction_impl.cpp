/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/ActivateControllerAction_impl.h"

#include "Utils/EntityUtils.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{

bool ActivateControllerAction::Step()
{
  const auto& entity_repository = mantle.environment->GetEntityRepository();

  for (const auto& entity_name : values.entities)
  {
    // entities might have been despawned, leading to a dangling reference to the controllers
    // so we check if the entity is still part of the simulation, before we access the controllers
    if (auto entity_ref = entity_repository.Get(entity_name))
    {
      const auto entity_id = entity_ref->get().GetUniqueId();
      if (const auto& entity_controllers = ose_services.controllerService->GetControllers(entity_id))
      {
        auto controller_id = ose_services.controllerService->GetControllerId(
            values.controllerRef, *entity_controllers);
        ose_services.controllerService->ChangeState(
            entity_id, controller_id, values.lateral_state, values.longitudinal_state);
      }
      else
      {
        Logger::Error("ActivateControllerAction: No user defined controller available for " + entity_name);
      }
    }
  }

  return true;
}

}  // namespace OpenScenarioEngine::v1_3
