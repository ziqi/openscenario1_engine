/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/AssignControllerAction_impl.h"

#include "Utils/EntityUtils.h"

namespace OpenScenarioEngine::v1_3
{
bool AssignControllerAction::Step()
{
  for (const auto& entity_name : values.entities)
  {
    // TODO: How do we incorporate Longitudinal and Lateral here?
    auto& entity = EntityUtils::GetEntityByName(mantle.environment, entity_name);
    auto& controller = mantle.environment->GetControllerRepository().Create(
        std::make_unique<mantle_api::ExternalControllerConfig>(values.controller));
    mantle.environment->AddEntityToController(entity, controller.GetUniqueId());
  }
  return true;
}

}  // namespace OpenScenarioEngine::v1_3