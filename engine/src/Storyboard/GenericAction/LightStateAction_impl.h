/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "Storyboard/GenericAction/LightStateAction_base.h"

namespace OpenScenarioEngine::v1_3
{

class LightStateAction : public LightStateActionBase
{
public:
  LightStateAction(Values values, Interfaces interfaces)
      : LightStateActionBase{std::move(values), std::move(interfaces)} {};

  bool Step() override;

private:
  void SetControlStrategy();

  std::shared_ptr<mantle_api::VehicleLightStatesControlStrategy> control_strategy_{
      std::make_shared<mantle_api::VehicleLightStatesControlStrategy>()};
};

}  // namespace OpenScenarioEngine::v1_3