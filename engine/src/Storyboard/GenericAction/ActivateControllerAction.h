/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <agnostic_behavior_tree/action_node.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include "Storyboard/GenericAction/ActivateControllerAction_impl.h"

namespace OpenScenarioEngine::v1_3::Node
{
class ActivateControllerAction : public yase::ActionNode
{
public:
  ActivateControllerAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IActivateControllerAction> activateControllerAction);
  void onInit() override;

private:
  yase::NodeStatus tick() override;
  void lookupAndRegisterData(yase::Blackboard& blackboard) final;

  std::unique_ptr<OpenScenarioEngine::v1_3::ActivateControllerAction> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IActivateControllerAction> activateControllerAction_;
};

}  // namespace OpenScenarioEngine::v1_3::Node