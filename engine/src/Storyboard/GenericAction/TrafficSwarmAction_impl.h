/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "Storyboard/GenericAction/TrafficSwarmAction_base.h"

namespace OpenScenarioEngine::v1_3
{
class TrafficSwarmAction : public TrafficSwarmActionBase
{
public:
  TrafficSwarmAction(Values values, Interfaces interfaces, Services services);
  bool Step() override;

private:
  void DespawnEntities();
  void SpawnEntities();
  void UpdateEnvironment();
  void RemoveEntity(const std::vector<std::pair<mantle_api::UniqueId, mantle_api::UniqueId>>::iterator& iterator);
  mantle_api::VehicleClass GetVehicleClassification();
  mantle_api::ExternalControllerConfig GetControllerConfiguration();
  mantle_api::Pose GetSpawningAreaPose() const;
  void ValidateScenarioParameters(const OpenScenarioEngine::v1_3::TrafficSwarmActionBase::Values& parameters);
  units::velocity::meters_per_second_t GetSpawnedVehicleSpeed(mantle_api::ITrafficSwarmService::RelativePosition relative_position,
                                                              double central_entity_speed,
                                                              double lower_speed_limit,
                                                              double upper_speed_limit) const;

  double vehicle_category_distribution_weights_sum_{0.0};
  double controller_distribution_weights_sum_{0.0};

  mantle_api::IEntity& central_entity_;
  size_t spawned_entities_count_{0};
  std::vector<std::pair<mantle_api::UniqueId, mantle_api::UniqueId>> entity_and_controller_id_list_;
};

}  // namespace OpenScenarioEngine::v1_3