/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "Storyboard/GenericAction/ActivateControllerAction_base.h"

namespace OpenScenarioEngine::v1_3
{
class ActivateControllerAction : public ActivateControllerActionBase
{
public:
  ActivateControllerAction(Values values, Interfaces interfaces, OseServices ose_services)
      : ActivateControllerActionBase{
            std::move(values),
            std::move(interfaces),
            std::move(ose_services)} {};

  bool Step() override;
};

}  // namespace OpenScenarioEngine::v1_3
