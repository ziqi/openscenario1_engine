/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <cassert>
#include <optional>
#include <utility>

#include "Storyboard/GenericAction/TrafficSwarmAction_impl.h"

namespace OpenScenarioEngine::v1_3::Node
{
class TrafficSwarmAction : public yase::ActionNode
{
public:
  TrafficSwarmAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficSwarmAction> trafficSwarmAction)
      : yase::ActionNode{"TrafficSwarmAction"},
        trafficSwarmAction_{trafficSwarmAction}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    auto probabilityService = blackboard.get<std::shared_ptr<IProbabilityService>>("ProbabilityService");

    impl_ = std::make_unique<OpenScenarioEngine::v1_3::TrafficSwarmAction>(
        OpenScenarioEngine::v1_3::TrafficSwarmAction::Values{
            trafficSwarmAction_->GetInnerRadius(),
            trafficSwarmAction_->GetNumberOfVehicles(),
            trafficSwarmAction_->GetOffset(),
            trafficSwarmAction_->GetSemiMajorAxis(),
            trafficSwarmAction_->GetSemiMinorAxis(),
            trafficSwarmAction_->IsSetVelocity()
                ? std::make_optional(trafficSwarmAction_->GetVelocity())
                : std::nullopt,
            ConvertScenarioCentralSwarmObject(trafficSwarmAction_->GetCentralObject()),
            ConvertScenarioTrafficDefinition(trafficSwarmAction_->GetTrafficDefinition()),
            trafficSwarmAction_->IsSetInitialSpeedRange()
                ? std::make_optional(ConvertScenarioRange(trafficSwarmAction_->GetInitialSpeedRange()))
                : std::nullopt,
            trafficSwarmAction_->IsSetDirectionOfTravelDistribution()
                ? std::make_optional(ConvertScenarioDirectionOfTravelDistribution(trafficSwarmAction_->GetDirectionOfTravelDistribution()))
                : std::nullopt},
        OpenScenarioEngine::v1_3::TrafficSwarmAction::Interfaces{
            environment},
        OpenScenarioEngine::v1_3::TrafficSwarmAction::Services{
            probabilityService});
  }

  std::unique_ptr<OpenScenarioEngine::v1_3::TrafficSwarmAction> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficSwarmAction> trafficSwarmAction_;
};

}  // namespace OpenScenarioEngine::v1_3::Node