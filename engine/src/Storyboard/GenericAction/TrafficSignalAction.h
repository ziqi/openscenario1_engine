/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

namespace OpenScenarioEngine::v1_3::Node
{
class TrafficSignalAction
{
public:
  TrafficSignalAction(std::shared_ptr<mantle_api::IEnvironment> environment,
                      std::vector<std::pair<std::string, std::string>> traffic_signal_states)
      : environment_{std::move(environment)},
        traffic_signal_states_{std::move(traffic_signal_states)} {}

  bool Step()
  {
    for (const auto& [name, state] : traffic_signal_states_)
    {
      environment_->SetTrafficSignalState(name, state);
    }
    return true;
  }

private:
  std::shared_ptr<mantle_api::IEnvironment> environment_;
  std::vector<std::pair<std::string, std::string>> traffic_signal_states_;
};

}  // namespace OpenScenarioEngine::v1_3::Node