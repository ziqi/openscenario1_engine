/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/AcquirePositionAction_impl.h"

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Traffic/i_entity.h>

#include <stdexcept>
#include <string_view>

#include "Utils/EntityUtils.h"

namespace OpenScenarioEngine::v1_3
{
namespace detail
{
mantle_api::RouteDefinition GenerateRouteDefinition(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    std::string&& entity_name,
    mantle_api::Vec3<units::length::meter_t>&& position)
{
  const auto& entity = EntityUtils::GetEntityByName(environment, entity_name);
  const auto& current_pos = entity.GetPosition();

  return {{{std::move(current_pos), mantle_api::RouteStrategy::kShortest},
           {std::move(position), mantle_api::RouteStrategy::kShortest}}};
}
}  // namespace detail

bool AcquirePositionAction::Step()
{
  const auto position = values.GetPosition();
  if (!position)
  {
    throw std::runtime_error("AcquirePositionAction: Target position has no value (potentially outside of world).");
  }

  for (const auto& actor : values.entities)
  {
    const auto& entity = EntityUtils::GetEntityByName(mantle.environment, actor);
    const auto& current_pos = entity.GetPosition();

    mantle_api::RouteDefinition route_definition{
        {{std::move(current_pos), mantle_api::RouteStrategy::kShortest},
         {std::move(position->position), mantle_api::RouteStrategy::kShortest}}};

    mantle.environment->AssignRoute(entity.GetUniqueId(), route_definition);
  }
  return true;
}

}  // namespace OpenScenarioEngine::v1_3
