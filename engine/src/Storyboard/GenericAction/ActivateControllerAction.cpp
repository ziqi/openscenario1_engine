/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ActivateControllerAction.h"

#include <MantleAPI/Execution/i_environment.h>

#include <cassert>
#include <utility>

#include "Utils/EntityBroker.h"
#include "Utils/IControllerService.h"

namespace OpenScenarioEngine::v1_3::Node
{

namespace detail
{
template <typename TargetState>
auto convert_to(bool is_set, bool value)
{
  if (is_set)
  {
    return value ? TargetState::kActivate : TargetState::kDeactivate;
  }
  return TargetState::kNoChange;
}
}  // namespace detail

ActivateControllerAction::ActivateControllerAction(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IActivateControllerAction> activateControllerAction)
    : yase::ActionNode{"ActivateControllerAction"},
      activateControllerAction_{activateControllerAction}
{
}

void ActivateControllerAction::onInit() {}

yase::NodeStatus ActivateControllerAction::tick()
{
  assert(impl_);
  const auto is_finished = impl_->Step();
  return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
}

void ActivateControllerAction::lookupAndRegisterData(yase::Blackboard& blackboard)
{
  auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
  const auto entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");
  auto controllerService = blackboard.get<ControllerServicePtr>("ControllerService");

  impl_ = std::make_unique<OpenScenarioEngine::v1_3::ActivateControllerAction>(
      OpenScenarioEngine::v1_3::ActivateControllerAction::Values{
          entityBroker->GetEntities(),

          activateControllerAction_->IsSetAnimation() ? std::make_optional<bool>(activateControllerAction_->GetAnimation()) : std::nullopt,

          ConvertScenarioControllerRef(activateControllerAction_->GetControllerRef()),

          detail::convert_to<mantle_api::IController::LateralState>(
              activateControllerAction_->IsSetLateral(),
              activateControllerAction_->GetLateral()),

          activateControllerAction_->IsSetLighting()
              ? std::make_optional<bool>(activateControllerAction_->GetLighting())
              : std::nullopt,

          detail::convert_to<mantle_api::IController::LongitudinalState>(
              activateControllerAction_->IsSetLongitudinal(),
              activateControllerAction_->GetLongitudinal())

      },  // Values
      OpenScenarioEngine::v1_3::ActivateControllerAction::Interfaces{
          environment},
      OpenScenarioEngine::v1_3::ActivateControllerAction::OseServices{
          controllerService});
}

}  // namespace OpenScenarioEngine::v1_3::Node
