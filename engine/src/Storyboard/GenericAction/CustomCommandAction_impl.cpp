/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/CustomCommandAction_impl.h"

namespace OpenScenarioEngine::v1_3
{
bool CustomCommandAction::Step()
{
  if (!command_executed_)
  {
    mantle.environment->ExecuteCustomCommand(values.entities, values.type, values.content);
    command_executed_ = true;
  }
  return true;
}

}  // namespace OpenScenarioEngine::v1_3