/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2021 Max Paul Bauer - Robert Bosch GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <utility>

#include "Node/TriggerableCompositeNode.h"

namespace OpenScenarioEngine::v1_3::Node
{
class EventNode : public TriggerableCompositeNode
{
public:
  EventNode(const std::string& name,
            yase::BehaviorNode::Ptr actions,
            yase::BehaviorNode::Ptr start_trigger = nullptr)
      : TriggerableCompositeNode{name}
  {
    set(std::move(actions), StopTriggerPtr{nullptr}, StartTriggerPtr{std::move(start_trigger)});
  }
};

}  // namespace OpenScenarioEngine::v1_3::Node