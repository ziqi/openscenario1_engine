/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Node/TrafficSignalPhaseNode.h"

namespace OpenScenarioEngine::v1_3::Node
{
using namespace yase;

NodeStatus TrafficSignalPhaseNode::tick()
{
  assert(impl_);
  if (WithinPeriod(environment_->GetSimulationTime()))
  {
    impl_->Step();
    return NodeStatus::kSuccess;
  }
  return NodeStatus::kRunning;
}

void TrafficSignalPhaseNode::lookupAndRegisterData(Blackboard& blackboard)
{
  environment_ = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
  impl_ = std::make_unique<TrafficSignalAction>(environment_,
                                                std::move(traffic_signal_states_));
}

bool TrafficSignalPhaseNode::WithinPeriod(mantle_api::Time current_time) const
{
  auto compare_time = units::math::fmod(current_time, period_);
  return (compare_time >= trigger_time_ && compare_time < trigger_time_ + duration_);
}

}  // namespace OpenScenarioEngine::v1_3::Node