/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Node/RootNode.h"

#include "Conversion/OscToNode/ParseStoryboard.h"
#include "Conversion/OscToNode/ParseTrafficSignals.h"
#include "Utils/IControllerService.h"

namespace OpenScenarioEngine::v1_3::Node
{
RootNode::RootNode(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IScenarioDefinition> scenarioDefinition,
                   std::shared_ptr<mantle_api::IEnvironment> environment,
                   std::shared_ptr<IControllerService> controller_service,
                   std::shared_ptr<IProbabilityService> probability_service,
                   std::shared_ptr<EngineAbortFlags> engine_abort_flags)
    : yase::ParallelNode{NAME_NODE_ROOT},
      environment_{environment},
      controller_service_{controller_service},
      probability_service_{probability_service},
      engine_abort_flags_{engine_abort_flags}
{
  if (auto storyboard = scenarioDefinition->GetStoryboard(); storyboard)
  {
    addChild(OpenScenarioEngine::v1_3::parse(storyboard));
  }
  if (const auto& traffic_signals = scenarioDefinition->GetRoadNetwork()->GetTrafficSignals(); !traffic_signals.empty())
  {
    addChild(OpenScenarioEngine::v1_3::parse(traffic_signals));
  }
  distributeData();
}

void RootNode::lookupAndRegisterData(yase::Blackboard& blackboard)
{
  blackboard.set("Environment", environment_);
  blackboard.set("ControllerService", controller_service_);
  blackboard.set("ProbabilityService", probability_service_);
  blackboard.set("EngineAbortFlags", engine_abort_flags_);
}

}  // namespace OpenScenarioEngine::v1_3::Node
