/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include <agnostic_behavior_tree/composite_node.h>

namespace OpenScenarioEngine::v1_3::Node
{
// The Repeating Sequence node
//
// The Repeating Sequence Node executes all children once in sequence.
// If a child behavior returns kRunning, it is executed in the next step again.
// Once it returns kSuccess, the next child is executed immediately in the same step.
// No child behavior is allowed to fail, if this happens, the whole Sequence fails
// As soon as the sequence succeeds, it will be repeated again
class RepeatingSequenceNode : public yase::CompositeNode
{
public:
  explicit RepeatingSequenceNode(const std::string& name = "Unnamed")
      : CompositeNode(std::string("RepeatingSequence[").append(name).append("]")){};

  ~RepeatingSequenceNode() override = default;

  void onInit() override
  {
    m_current_child = 0;
    if (childrenCount() != 0)
    {
      this->child(m_current_child).onInit();
    }
  }

  /// \brief Get the last running child
  [[nodiscard]] size_t getCurrentChild() const { return m_current_child; }

private:
  yase::NodeStatus tick() final
  {
    // Loop over all children
    for (size_t index = m_current_child; index < childrenCount(); index++)
    {
      auto& child = this->child(index);

      const yase::NodeStatus child_status = child.executeTick();

      switch (child_status)
      {
        case yase::NodeStatus::kRunning:
        {
          return child_status;
        }
        case yase::NodeStatus::kFailure:
        {
          child.onTerminate();
          return child_status;
        }
        case yase::NodeStatus::kSuccess:
        {
          // Terminate current child and init subsequent one
          child.onTerminate();
          m_current_child++;
          if (m_current_child < childrenCount())
          {
            this->child(m_current_child).onInit();
          }
          else
          {
            onInit();
          }
          break;
        }
        default:
        {
          std::string error_msg = "The child node [";
          error_msg.append(child.name());
          error_msg.append("] returned unknown NodeStatus.");
          throw std::invalid_argument(error_msg);
        }
      }
    }
    return yase::NodeStatus::kRunning;
  }

  size_t m_current_child{0};
};

}  // namespace OpenScenarioEngine::v1_3::Node