/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <agnostic_behavior_tree/composite_node.h>

namespace OpenScenarioEngine::v1_3::Node
{
class AnyTriggeringEntityNode : public yase::CompositeNode
{
public:
  explicit AnyTriggeringEntityNode(const std::string &name = "Unnamed")
      : AnyTriggeringEntityNode(name, nullptr){};

  explicit AnyTriggeringEntityNode(yase::Extension::UPtr extension_ptr)
      : AnyTriggeringEntityNode("Unnamed", std::move(extension_ptr)){};

  AnyTriggeringEntityNode(const std::string &name, yase::Extension::UPtr extension_ptr)
      : CompositeNode(std::string("Selector::").append(name), std::move(extension_ptr)){};

  ~AnyTriggeringEntityNode() override = default;

  void onInit() override
  {
    m_last_initialized_child = -1;
  }

private:
  yase::NodeStatus tick() final
  {
    // Loop over all children
    for (size_t index = 0; index < childrenCount(); index++)
    {
      auto& child = this->child(index);

      // Initialize child before executing it
      if (index > m_last_initialized_child)
      {
        child.onInit();
        m_last_initialized_child = index;   // NOLINT
      }

      const yase::NodeStatus child_status = child.executeTick();

      switch (child_status)
      {
        case yase::NodeStatus::kRunning:
        {
          continue;
        }
        case yase::NodeStatus::kFailure:
        {
          std::string error_msg = "The child node [";
          error_msg.append(child.name());
          error_msg.append("] returned failed NodeStatus.");
          throw std::invalid_argument(error_msg);
        }
        case yase::NodeStatus::kSuccess:
        {
          return child_status;
        }
        default:
        {
          std::string error_msg = "The child node [";
          error_msg.append(child.name());
          error_msg.append("] returned unkown NodeStatus.");
          throw std::invalid_argument(error_msg);
        }
      }
    }

    // None of the child entity conditions evaluated to true
    return yase::NodeStatus::kRunning;
  }

  // Index of last running child - negative index indicates none
  int m_last_initialized_child{-1};
};

}  // namespace OpenScenarioEngine::v1_3::Node
