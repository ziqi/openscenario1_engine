/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>

#include "Storyboard/GenericAction/TrafficSignalAction.h"

namespace OpenScenarioEngine::v1_3::Node
{
class TrafficSignalPhaseNode : public yase::ActionNode
{
public:
  TrafficSignalPhaseNode(const std::string& name,
                         mantle_api::Time period,         // NOLINT(bugprone-easily-swappable-parameters)
                         mantle_api::Time trigger_time,   // NOLINT(bugprone-easily-swappable-parameters)
                         mantle_api::Time duration,       // NOLINT(bugprone-easily-swappable-parameters)
                         std::vector<std::pair<std::string, std::string>> traffic_signal_states)
      : ActionNode{name},
        period_{period},
        trigger_time_{trigger_time},
        duration_{duration},
        traffic_signal_states_{std::move(traffic_signal_states)}
  {
  }

  void onInit() override{};
  yase::NodeStatus tick() override;
  void lookupAndRegisterData(yase::Blackboard& blackboard) final;

private:
  [[nodiscard]] bool WithinPeriod(mantle_api::Time current_time) const;

  mantle_api::Time period_;
  mantle_api::Time trigger_time_;
  mantle_api::Time duration_;
  std::vector<std::pair<std::string, std::string>> traffic_signal_states_;

  std::shared_ptr<mantle_api::IEnvironment> environment_;
  std::unique_ptr<TrafficSignalAction> impl_;
};

}  // namespace OpenScenarioEngine::v1_3::Node