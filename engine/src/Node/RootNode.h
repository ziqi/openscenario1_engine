/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/behavior_node.h>
#include <agnostic_behavior_tree/composite/parallel_node.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <memory>

#include "Utils/EngineAbortFlags.h"
#include "Utils/IProbabilityService.h"

namespace OpenScenarioEngine::v1_3
{

class IControllerService;

static constexpr auto NAME_NODE_ROOT{"OpenScenarioEngine"};

yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IStoryboard>);

namespace Node
{
class RootNode : public yase::ParallelNode
{
public:
  RootNode(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IScenarioDefinition> scenarioDefinition,
           std::shared_ptr<mantle_api::IEnvironment> environment,
           std::shared_ptr<IControllerService> controller_service,
           std::shared_ptr<IProbabilityService> probability_service,
           std::shared_ptr<EngineAbortFlags> engine_abort_flags);

private:
  void lookupAndRegisterData(yase::Blackboard &blackboard) override;

  std::shared_ptr<mantle_api::IEnvironment> environment_;
  std::shared_ptr<IControllerService> controller_service_;
  std::shared_ptr<IProbabilityService> probability_service_;
  std::shared_ptr<EngineAbortFlags> engine_abort_flags_;
};

}  // namespace Node

}  // namespace OpenScenarioEngine::v1_3