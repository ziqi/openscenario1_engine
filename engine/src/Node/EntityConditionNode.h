/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <agnostic_behavior_tree/decorator_node.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <memory>

#include "Conversion/OscToMantle/ConvertScenarioEntity.h"

namespace OpenScenarioEngine::v1_3::Node
{
class EntityConditionNode : public yase::DecoratorNode
{
public:
  EntityConditionNode(
      std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IEntityCondition> entityCondition,
      Entity triggeringEntity);

private:
  void lookupAndRegisterData(yase::Blackboard& blackboard) final;
  yase::NodeStatus tick() final;

  Entity triggeringEntity_;
};

}  // namespace OpenScenarioEngine::v1_3::Node
