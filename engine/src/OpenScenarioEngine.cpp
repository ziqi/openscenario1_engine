/*******************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "OpenScenarioEngine/OpenScenarioEngine.h"

#include <agnostic_behavior_tree/behavior_node.h>
#include <agnostic_behavior_tree/utils/tree_print.h>
#include <openScenarioLib/src/common/ErrorLevel.h>
#include <openScenarioLib/src/loader/FileResourceLocator.h>
#include <openScenarioLib/src/v1_3/loader/XmlScenarioImportLoaderFactoryV1_3.h>

#include <sstream>
#include <utility>

#include "Conversion/OscToMantle/ConvertScenarioPosition.h"
#include "Node/RootNode.h"
#include "Utils/Constants.h"
#include "Utils/ControllerCreator.h"
#include "Utils/EngineAbortFlags.h"
#include "Utils/EntityCreator.h"
#include "Utils/Logger.h"
#include "Utils/ProbabilityService.h"

namespace OpenScenarioEngine::v1_3
{
namespace detail
{
std::pair<size_t, size_t> CountWarningsAndErrors(const OpenScenarioEngine::SimpleMessageLoggerPtr& logger)
{
  const auto warnings = logger->GetMessagesFilteredByErrorLevel(NET_ASAM_OPENSCENARIO::ErrorLevel::WARNING);
  const auto errors = logger->GetMessagesFilteredByWorseOrEqualToErrorLevel(NET_ASAM_OPENSCENARIO::ErrorLevel::ERROR);
  return std::make_pair(errors.size(), warnings.size());
}

int SumAndPrintWarningsAndErrors(const OpenScenarioEngine::SimpleMessageLoggerPtr& message_logger,
                                 const OpenScenarioEngine::SimpleMessageLoggerPtr& catalog_logger)
{
  const auto message = detail::CountWarningsAndErrors(message_logger);
  const auto catalog = detail::CountWarningsAndErrors(catalog_logger);

  const auto total_errors = message.first + catalog.first;
  if (total_errors == 0)
  {
    Logger::Info("Scenario validation succeeded with 0 errors, " +
                 std::to_string(message.second) + " warnings, and " +
                 std::to_string(catalog.second) + " catalog warnings");
  }
  else
  {
    Logger::Error("Scenario validation failed with " +
                  std::to_string(message.first) + " errors, " +
                  std::to_string(catalog.first) + " catalog_errors with " +
                  std::to_string(message.second) + " warnings, and " +
                  std::to_string(catalog.second) + " catalog_warnings");
  }
  return static_cast<int>(total_errors);
}

void TraceTree(const yase::BehaviorNode& root_node)
{
  std::stringstream ss;
  yase::printTreeWithStates(root_node, ss);
  Logger::Trace(ss.str());
}

mantle_api::MapRegionType ConvertScenarioUsedAreaTypeString(const std::string& used_area_type)
{
  if (used_area_type == "route")
  {
    return mantle_api::MapRegionType::kRoute;
  }
  if (used_area_type == "polygon")
  {
    return mantle_api::MapRegionType::kPolygon;
  }
  throw std::runtime_error("Unknown used_area_type. Supported types are 'route' or 'polygon'. Please adjust the scenario.");
}

mantle_api::MapRegionType ExtractMapRegionType(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IFileHeader> file_header)
{
  if (file_header->IsSetProperties())
  {
    for (const auto& property : file_header->GetProperties()->GetProperties())
    {
      if (property->GetName() == "used_area_type")
      {
        return ConvertScenarioUsedAreaTypeString(property->GetValue());
      }
    }
  }
  return mantle_api::MapRegionType::kUndefined;
}

}  // namespace detail

OpenScenarioEngine::OpenScenarioEngine(const std::string& scenario_file_path,
                                       std::shared_ptr<mantle_api::IEnvironment> environment,
                                       std::shared_ptr<mantle_api::ILogger> logger,
                                       unsigned int initial_seed)
    : scenario_file_path_{scenario_file_path},
      environment_{std::move(environment)},
      probability_service_{std::make_shared<ProbabilityService>(initial_seed)}
{
  Logger::SetLogger(std::move(logger));

  if (!environment_)
  {
    throw std::runtime_error("Unable to initialize OpenScenarioEngine: No valid environment (nullptr)");
  }
  controller_creator_ = std::make_shared<ControllerCreator>(*environment_);
}

void OpenScenarioEngine::OverrideRandomSeed(unsigned int random_seed)
{
  random_seed_value_override_ = random_seed;
}

void OpenScenarioEngine::ResetProbabilityService()
{
  // upcast to implementation
  auto probability_service = std::dynamic_pointer_cast<ProbabilityService>(probability_service_);

  if (random_seed_value_override_)
  {
    probability_service->SetRandomSeed(random_seed_value_override_.value());
  }
  else
  {
    static bool first_run{true};
    if (first_run)
    {
      first_run = false;
    }
    else
    {
      probability_service->SetRandomSeed(probability_service->GetRandomSeed() + 1);
    }
  }
}

void OpenScenarioEngine::Init()
{
  environment_->SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute);
  ParseScenarioFile();
}

void OpenScenarioEngine::SetupDynamicContent()
{
  ResetProbabilityService();
  ResetAndCreateEntities();
  ResetAndCreateControllers();
  ResetAndCreateStoryboard();
  SetEnvironmentDefaults();
}

mantle_api::ScenarioInfo OpenScenarioEngine::GetScenarioInfo() const
{
  mantle_api::ScenarioInfo info{};

  info.description = scenario_data_ptr_->GetFileHeader()->GetDescription();
  info.scenario_timeout_duration = GetDuration();
  info.additional_information.emplace("full_scenario_path", ResolveScenarioPath(scenario_file_path_));

  if (scenario_definition_ptr_->GetCatalogLocations()->GetVehicleCatalog() != nullptr)
  {
    const auto& vehicle_catalog_path =
        scenario_definition_ptr_->GetCatalogLocations()->GetVehicleCatalog()->GetDirectory()->GetPath();
    info.additional_information.emplace("vehicle_catalog_path", vehicle_catalog_path);
  }

  if (scenario_definition_ptr_->GetRoadNetwork()->GetLogicFile() != nullptr)
  {
    info.full_map_path = ResolveMapPath(scenario_definition_ptr_->GetRoadNetwork()->GetLogicFile()->GetFilepath());
  }

  if (scenario_definition_ptr_->GetRoadNetwork()->GetSceneGraphFile() != nullptr)
  {
    info.map_model_reference = scenario_definition_ptr_->GetRoadNetwork()->GetSceneGraphFile()->GetFilepath();
  }

  info.map_details = GetMapDetails();
  return info;
}

void OpenScenarioEngine::Step(mantle_api::Time delta_time)
{
  std::ignore = delta_time;  // delta not needed. Current time accessible via environment_->GetSimulationTime()
  using namespace std::chrono_literals;
  const auto nodeStatus = root_node_->executeTick();

  finished_ =
      nodeStatus == yase::NodeStatus::kFailure ||
      nodeStatus == yase::NodeStatus::kSuccess;

  if (Logger::GetCurrentLogLevel() == mantle_api::LogLevel::kTrace)
  {
    detail::TraceTree(*root_node_);
  }
}

std::optional<mantle_api::Time> OpenScenarioEngine::GetDesiredDeltaTime() const noexcept
{
  return std::nullopt;
}

bool OpenScenarioEngine::IsFinished() const
{
  return finished_;
}

void OpenScenarioEngine::LoadScenarioData()
{
  const std::string resolved_scenario_file_path = ResolveScenarioPath(scenario_file_path_);
  message_logger_ =
      std::make_shared<NET_ASAM_OPENSCENARIO::SimpleMessageLogger>(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO);
  catalog_message_logger_ =
      std::make_shared<NET_ASAM_OPENSCENARIO::SimpleMessageLogger>(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO);
  auto loader_factory = NET_ASAM_OPENSCENARIO::v1_3::XmlScenarioImportLoaderFactory(catalog_message_logger_,
                                                                                    resolved_scenario_file_path);
  const auto loader = loader_factory.CreateLoader(std::make_shared<NET_ASAM_OPENSCENARIO::FileResourceLocator>());

  scenario_data_ptr_ = std::static_pointer_cast<NET_ASAM_OPENSCENARIO::v1_3::IOpenScenario>(
      loader->Load(message_logger_)->GetAdapter(typeid(NET_ASAM_OPENSCENARIO::v1_3::IOpenScenario).name()));
}

void OpenScenarioEngine::ParseScenarioFile()
{
  if (ValidateScenario() > 0)
  {
    throw std::runtime_error("Scenario file contains errors.");
  }
}

int OpenScenarioEngine::ValidateScenario()
{
  if (scenario_data_ptr_)
  {
    const auto scenario_errors = message_logger_->GetMessagesFilteredByWorseOrEqualToErrorLevel(
        NET_ASAM_OPENSCENARIO::ErrorLevel::ERROR);
    const auto catalog_errors = catalog_message_logger_->GetMessagesFilteredByWorseOrEqualToErrorLevel(
        NET_ASAM_OPENSCENARIO::ErrorLevel::ERROR);
    return scenario_errors.size() + catalog_errors.size();
  }

  LoadScenarioData();
  LogParsingMessages(message_logger_);
  LogParsingMessages(catalog_message_logger_);
  const auto total_errors = detail::SumAndPrintWarningsAndErrors(message_logger_, catalog_message_logger_);
  SetScenarioDefinitionPtr(scenario_data_ptr_);
  return total_errors;
}

void OpenScenarioEngine::LogParsingMessages(SimpleMessageLoggerPtr message_logger)
{
  using namespace std::string_literals;

  for (auto&& log_message : message_logger->GetMessagesFilteredByWorseOrEqualToErrorLevel(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO))
  {
    const auto text_marker = log_message.GetTextmarker();
    const auto message = "(File:"s + text_marker.GetFilename() + ") "s +
                         log_message.GetMsg() + " ("s +
                         std::to_string(text_marker.GetLine()) + ","s +
                         std::to_string(text_marker.GetColumn()) + ")"s;

    switch (log_message.GetErrorLevel())
    {
      case NET_ASAM_OPENSCENARIO::ErrorLevel::DEBUG:
        Logger::Debug(message);
        break;
      case NET_ASAM_OPENSCENARIO::ErrorLevel::INFO:
        Logger::Info(message);
        break;
      case NET_ASAM_OPENSCENARIO::ErrorLevel::WARNING:
        Logger::Warning(message);
        break;
      case NET_ASAM_OPENSCENARIO::ErrorLevel::ERROR:
        Logger::Error(message);
        break;
      case NET_ASAM_OPENSCENARIO::ErrorLevel::FATAL:
        Logger::Critical(message);
        break;
      default:
        std::runtime_error("Unknown ErrorLevel of SimpleMessageLogger message");
    }
  }
}

std::unique_ptr<mantle_api::MapDetails> OpenScenarioEngine::GetMapDetails() const
{
  auto road_network_ptr = scenario_definition_ptr_->GetRoadNetwork();
  auto used_area = road_network_ptr->GetUsedArea();
  if (used_area == nullptr)
  {
    return std::make_unique<mantle_api::MapDetails>();
  }

  auto map_details = std::make_unique<mantle_api::MapDetails>();
  map_details->map_region_type = detail::ExtractMapRegionType(scenario_data_ptr_->GetFileHeader());

  for (const auto& position : used_area->GetPositions())
  {
    if (const auto& geo_position = position->GetGeoPosition())
    {
      mantle_api::LatLonPosition lat_lon_position = ConvertToMantleLatLonPosition(*geo_position);
      map_details->map_region.push_back(lat_lon_position);
    }
    else
    {
      Logger::Error("GetMapDetails: Unexpected 'Position' type. Only 'GeoPosition' supported.");
    }
  }
  return map_details;
}

void OpenScenarioEngine::ResetAndCreateEntities()
{
  if (!scenario_definition_ptr_->GetEntities())
  {
    throw std::runtime_error("Scenario does not contain entities.");
  }

  environment_->GetEntityRepository().Reset();
  entity_creator_ = std::make_shared<EntityCreator>(environment_);

  // Add scenario objects to environment
  for (auto&& scenario_object : scenario_definition_ptr_->GetEntities()->GetScenarioObjects())
  {
    if (!scenario_object)
    {
      continue;
    }

    entity_creator_->CreateEntity(scenario_object);
  }

  // TODO: What to do with the entity selections?
}

void OpenScenarioEngine::ResetAndCreateControllers()
{
  environment_->GetControllerRepository().Reset();
  controller_creator_->GetControllerService()->ResetControllerMappings();

  if (const auto entities = scenario_definition_ptr_->GetEntities())
  {
    controller_creator_->CreateControllers(entities->GetScenarioObjects());
  }
}

void OpenScenarioEngine::ResetAndCreateStoryboard()
{
  if (!scenario_definition_ptr_->GetStoryboard())
  {
    throw std::runtime_error("Scenario does not contain storyboard.");
  }

  auto engine_abort_flags = std::make_shared<EngineAbortFlags>(EngineAbortFlags::kNoAbort);
  root_node_ = std::make_shared<Node::RootNode>(
      scenario_definition_ptr_,
      environment_,
      controller_creator_->GetControllerService(),
      probability_service_,
      engine_abort_flags);
}

void OpenScenarioEngine::SetEnvironmentDefaults()
{
  environment_->SetDateTime(mantle_api::Time{43200000});
  environment_->SetWeather(GetDefaultWeather());
}

void OpenScenarioEngine::SetScenarioDefinitionPtr(OpenScenarioPtr open_scenario_ptr)
{
  if (!(open_scenario_ptr && open_scenario_ptr->GetOpenScenarioCategory() &&
        open_scenario_ptr->GetOpenScenarioCategory()->GetScenarioDefinition()))
  {
    throw std::runtime_error("Scenario file not found or file does not contain scenario definition.");
  }

  scenario_definition_ptr_ = open_scenario_ptr->GetOpenScenarioCategory()->GetScenarioDefinition();
}

mantle_api::Time OpenScenarioEngine::GetDuration() const
{
  auto storyboard = scenario_definition_ptr_->GetStoryboard();
  auto condition_groups = storyboard->GetStopTrigger()->GetConditionGroups();
  std::vector<double> durations{};
  for (const auto& condition_group : condition_groups)
  {
    auto conditions = condition_group->GetConditions();
    std::for_each(conditions.begin(), conditions.end(), [&durations](const auto& condition)
                  {
            if (const auto& by_val_condition = condition->GetByValueCondition())
            {
                if (const auto& sim_time_condition = by_val_condition->GetSimulationTimeCondition())
                {
                    durations.push_back(sim_time_condition->GetValue());
                }
            } });
  }
  std::sort(durations.begin(), durations.end(), std::greater<double>());
  return durations.empty() ? mantle_api::Time{std::numeric_limits<double>::max()}
                           : mantle_api::Time{units::time::second_t{durations.at(0)}};
}

void OpenScenarioEngine::ActivateExternalHostControl()
{
  controller_creator_->EnableUserDefinedControllerOverride();
}

}  // namespace OpenScenarioEngine::v1_3
