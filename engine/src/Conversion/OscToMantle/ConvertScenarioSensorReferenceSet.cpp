/********************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioSensorReferenceSet.h"

namespace OpenScenarioEngine::v1_3
{
SensorReferenceSet ConvertScenarioSensorReferenceSet(
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ISensorReferenceSet>& sensorReferenceSet)
{
  if (!sensorReferenceSet)
  {
    return {};
  }

  SensorReferenceSet sensorReferences;
  sensorReferences.reserve(static_cast<std::size_t>(sensorReferenceSet->GetSensorReferencesSize()));
  for (const auto& sensorReference : sensorReferenceSet->GetSensorReferences())
  {
    sensorReferences.emplace_back(sensorReference->GetName());
  }
  return sensorReferences;
}

}  // namespace OpenScenarioEngine::v1_3
