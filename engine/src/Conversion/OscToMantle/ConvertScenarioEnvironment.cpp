/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioEnvironment.h"

#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>
#include <openScenarioLib/generated/v1_3/catalog/CatalogHelperV1_3.h>

#include <cmath>

namespace OpenScenarioEngine::v1_3
{
namespace detail
{

// Normalize to [0,2PI]
units::angle::radian_t normalize_angle(const units::angle::radian_t& angle)
{
  constexpr units::angle::radian_t TWO_PI{2 * units::constants::detail::PI_VAL};
  units::angle::radian_t normalized_angle = units::math::fmod(angle, TWO_PI);
  if (normalized_angle.value() < 0)
  {
    normalized_angle += TWO_PI;
  }
  return normalized_angle;
}

template <typename T>
T Discretize(const double current_value, const std::vector<std::pair<double, T>> thresholds)
{
  for (const auto& [threshold, discrete_value] : thresholds)
  {
    if (current_value < threshold)
    {
      return discrete_value;
    }
  }
  return T::kOther;
}

mantle_api::Fog ConvertFog(const double range)
{
  return Discretize(range, std::vector<std::pair<double, mantle_api::Fog>>{
                                       {0, mantle_api::Fog::kOther},
                                       {50.0, mantle_api::Fog::kDense},
                                       {200, mantle_api::Fog::kThick},
                                       {1000, mantle_api::Fog::kLight},
                                       {2000, mantle_api::Fog::kMist},
                                       {4000, mantle_api::Fog::kPoorVisibility},
                                       {10000, mantle_api::Fog::kModerateVisibility},
                                       {40000, mantle_api::Fog::kGoodVisibility},
                                       {std::numeric_limits<double>::max(), mantle_api::Fog::kExcellentVisibility}});
}

mantle_api::Precipitation ConvertPrecipitation(const double intensity)
{
  return Discretize(intensity, std::vector<std::pair<double, mantle_api::Precipitation>>{
                                           {0, mantle_api::Precipitation::kOther},
                                           {0.1, mantle_api::Precipitation::kNone},
                                           {0.5, mantle_api::Precipitation::kVeryLight},
                                           {1.9, mantle_api::Precipitation::kLight},
                                           {8.1, mantle_api::Precipitation::kModerate},
                                           {34, mantle_api::Precipitation::kHeavy},
                                           {149, mantle_api::Precipitation::kVeryHeavy},
                                           {std::numeric_limits<double>::max(), mantle_api::Precipitation::kExtreme}});
}

mantle_api::Sun ConvertSun(const NET_ASAM_OPENSCENARIO::v1_3::ISun& parsed_sun, const mantle_api::Sun& default_sun)
{
  mantle_api::Sun sun{default_sun};

  // check IsSetAzimuth isn't required because field is mandatory and loader already checks existance
  // conversion from clockwise to counter-clockwise because mismatching between OSI and OpenScenario
  sun.azimuth = normalize_angle(units::angle::radian_t{-parsed_sun.GetAzimuth()});
  // check IsSetElevation isn't required because field is mandatory and loader already checks existance
  sun.elevation = units::angle::radian_t{parsed_sun.GetElevation()};
  if (parsed_sun.IsSetIlluminance())
  {
    sun.intensity = units::illuminance::lux_t{parsed_sun.GetIlluminance()};
  }
  return sun;
}

[[nodiscard]] mantle_api::Weather ConvertWeather(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IWeather> weather)
{
  mantle_api::Weather mantle_weather = GetDefaultWeather();
  return {weather->IsSetFog()
              ? ConvertFog(weather->GetFog()->GetVisualRange())
              : mantle_weather.fog,
          weather->IsSetPrecipitation() && weather->GetPrecipitation()->IsSetPrecipitationIntensity()
              ? ConvertPrecipitation(weather->GetPrecipitation()->GetPrecipitationIntensity())
              : mantle_weather.precipitation,
          mantle_weather.illumination,
          mantle_weather.humidity,
          weather->IsSetTemperature()
              ? units::temperature::kelvin_t{weather->GetTemperature()}
              : mantle_weather.temperature,
          weather->IsSetAtmosphericPressure()
              ? units::pressure::pascal_t{weather->GetAtmosphericPressure()}
              : mantle_weather.atmospheric_pressure,
          weather->IsSetSun()
              ? ConvertSun(*weather->GetSun(), mantle_weather.sun)
              : mantle_weather.sun};
}

const NET_ASAM_OPENSCENARIO::v1_3::IEnvironment& ResolveChoice(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IEnvironment>& environment,
                                                               const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference>& catalogReference)
{
  if (environment)
  {
    return *environment;
  }

  if (catalogReference)
  {
    auto ref = catalogReference->GetRef();
    return *NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::AsEnvironment(ref);
  }

  throw std::runtime_error("ConvertScenarioEnvironment: No environment defined or referenced. Please adjust the scenario.");
}

Environment ConvertEnvironment(const NET_ASAM_OPENSCENARIO::v1_3::IEnvironment& scenario_environment)
{
  return {
      scenario_environment.IsSetTimeOfDay()
          ? std::optional(units::make_unit<units::time::second_t>(
                NET_ASAM_OPENSCENARIO::DateTime::GetSeconds(scenario_environment.GetTimeOfDay()->GetDateTime())))
          : std::nullopt,
      scenario_environment.IsSetWeather()
          ? std::optional(ConvertWeather(scenario_environment.GetWeather()))
          : std::nullopt};
}

}  // namespace detail

Environment ConvertScenarioEnvironment(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IEnvironment>& environment,
                                       const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference>& catalogReference)
{
  return detail::ConvertEnvironment(
      detail::ResolveChoice(environment, catalogReference));
}

}  // namespace OpenScenarioEngine::v1_3
