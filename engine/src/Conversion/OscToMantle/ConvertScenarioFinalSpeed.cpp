/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioFinalSpeed.h"

namespace OpenScenarioEngine::v1_3
{
mantle_api::Vec3<units::velocity::meters_per_second_t> ConvertScenarioFinalSpeed(const std::shared_ptr<mantle_api::IEnvironment>& /*environment*/,
                                                                                 const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IFinalSpeed>& /*finalSpeed*/)
{
  return {};
}

}  // namespace OpenScenarioEngine::v1_3
