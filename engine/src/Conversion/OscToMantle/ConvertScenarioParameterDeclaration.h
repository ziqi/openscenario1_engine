
/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <memory>
#include <string>

namespace OpenScenarioEngine::v1_3
{
using ParameterDeclaration = std::string;

inline ParameterDeclaration ConvertScenarioParameterDeclaration(const std::shared_ptr<NET_ASAM_OPENSCENARIO::INamedReference<NET_ASAM_OPENSCENARIO::v1_3::IParameterDeclaration>>& parameterDeclaration)
{
  return parameterDeclaration->GetNameRef();
}

inline ParameterDeclaration ConvertScenarioParameterDeclaration(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IParameterDeclaration>& parameterDeclaration)
{
  return parameterDeclaration->GetName();
}

}  // namespace OpenScenarioEngine::v1_3