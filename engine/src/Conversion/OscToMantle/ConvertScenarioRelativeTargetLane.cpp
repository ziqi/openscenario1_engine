/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioRelativeTargetLane.h"

#include <MantleAPI/Common/pose.h>
#include <units.h>

#include "Conversion/OscToMantle/ConvertScenarioEntity.h"
#include "Utils/EntityUtils.h"

namespace OpenScenarioEngine::v1_3
{
mantle_api::LaneId ConvertScenarioRelativeTargetLane(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IRelativeTargetLane>& relativeTargetLane)
{
  const auto relative_entity_name = relativeTargetLane->GetEntityRef()->GetNameRef();
  const auto& relative_entity = EntityUtils::GetEntityByName(environment, relative_entity_name);
  const auto relative_pose = mantle_api::Pose{relative_entity.GetPosition(),
                                              relative_entity.GetOrientation()};
  if (const auto target_lane_id =
          environment->GetQueryService().GetRelativeLaneId(relative_pose, relativeTargetLane->GetValue()))
  {
    return target_lane_id.value();
  }

  throw std::runtime_error("ConvertScenarioRelativeTargetLane: Cannot find the target lane to the reference entity \"" +
                           relative_entity_name + "\". Please adjust the scenario.");
}

}  // namespace OpenScenarioEngine::v1_3