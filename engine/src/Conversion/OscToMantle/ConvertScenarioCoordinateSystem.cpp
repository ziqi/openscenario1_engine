/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioCoordinateSystem.h"

#include <stdexcept>

namespace OpenScenarioEngine::v1_3
{
CoordinateSystem ConvertScenarioCoordinateSystem(const NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem& coordinateSystem)
{
  switch (NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::GetFromLiteral(coordinateSystem.GetLiteral()))
  {
    case NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::UNKNOWN:
      return CoordinateSystem::kUnknown;
    case NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::ENTITY:
      return CoordinateSystem::kEntity;
    case NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::LANE:
      return CoordinateSystem::kLane;
    case NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::ROAD:
      return CoordinateSystem::kRoad;
    case NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::TRAJECTORY:
      return CoordinateSystem::kTrajectory;
    case NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::WORLD:
      return CoordinateSystem::kWorld;
    default:
      return CoordinateSystem::kUnknown;
  }
  throw std::runtime_error("ConvertScenarioCoordinateSystem: Unknown CoordinateSystem");
}

}  // namespace OpenScenarioEngine::v1_3