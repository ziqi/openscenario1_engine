/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/floating_point_helper.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <exception>
#include <stdexcept>
#include <string>
#include <type_traits>

#include "DateTime.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{
namespace detail
{
/// @brief Epsilon for equality of floating point types
static constexpr double RULE_FLOATING_POINT_EPSILON{1e-6};

template <typename ReturnValueType, typename T>
ReturnValueType EvalCompareFunction(const NET_ASAM_OPENSCENARIO::v1_3::Rule& rule)
{
  if (rule == NET_ASAM_OPENSCENARIO::v1_3::Rule::GREATER_THAN)
  {
    return [](T lhs, T rhs) { return lhs > rhs; };
  }
  if (rule == NET_ASAM_OPENSCENARIO::v1_3::Rule::LESS_THAN)
  {
    return [](T lhs, T rhs) { return lhs < rhs; };
  }
  if (rule == NET_ASAM_OPENSCENARIO::v1_3::Rule::EQUAL_TO)
  {
    return [](T lhs, T rhs) {
      if constexpr (std::is_floating_point_v<T>)
      {
        return mantle_api::AlmostEqual(lhs, rhs, RULE_FLOATING_POINT_EPSILON);
      }
      else
      {
        return lhs == rhs;
      }
    };
  }
  if (rule == NET_ASAM_OPENSCENARIO::v1_3::Rule::GREATER_OR_EQUAL)
  {
    return [](T lhs, T rhs) {
      if constexpr (std::is_floating_point_v<T>)
      {
        return lhs > rhs || mantle_api::AlmostEqual(lhs, rhs, RULE_FLOATING_POINT_EPSILON);
      }
      else
      {
        return lhs >= rhs;
      }
    };
  }
  if (rule == NET_ASAM_OPENSCENARIO::v1_3::Rule::LESS_OR_EQUAL)
  {
    return [](T lhs, T rhs) {
      if constexpr (std::is_floating_point_v<T>)
      {
        return lhs < rhs || mantle_api::AlmostEqual(lhs, rhs, RULE_FLOATING_POINT_EPSILON);
      }
      else
      {
        return lhs <= rhs;
      }
    };
  }
  if (rule == NET_ASAM_OPENSCENARIO::v1_3::Rule::NOT_EQUAL_TO)
  {
    return [](T lhs, T rhs) {
      if constexpr (std::is_floating_point_v<T>)
      {
        return !mantle_api::AlmostEqual(lhs, rhs, RULE_FLOATING_POINT_EPSILON);
      }
      else
      {
        return lhs != rhs;
      }
    };
  }
  throw std::runtime_error("Unknown rule used in scenario. Please adjust scenario.");
}

}  // namespace detail

/**
 * @brief Evaluator for Rule
 * @see NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum
 *
 * @tparam T floating point or integer type
 * @note for equality of floating point types @ref RULE_FLOATING_POINT_EPSILON is used
 */
template <typename T>
class Rule
{
public:
  Rule(NET_ASAM_OPENSCENARIO::v1_3::Rule rule, T value)
      : compare_{detail::EvalCompareFunction<decltype(compare_), T>(rule)},
        value_{value} {};

  /**
   * @brief Evaluates the stored rule, e.g. is current_value GREATHER_THAN(3)?
   *
   * @param current_value current value
   * @return true if current value meets rule, else false
   */
  [[nodiscard]] bool IsSatisfied(T current_value) const { return compare_(current_value, value_); }

private:
  bool (*compare_)(T, T);  ///!< comparison method
  T value_;                ///!< comparison value
};

template <typename T>
struct ConvertScenarioRule_
{
  NET_ASAM_OPENSCENARIO::v1_3::Rule rule;
  T value;

  operator Rule<std::string>() { return Rule<std::string>(rule, value); } // NOLINT(google-explicit-constructor)
  operator auto()                                                         // NOLINT(google-explicit-constructor)
  {
    if constexpr (std::is_same_v<T, std::string>)
    {
      try
      {
        return Rule<double>(rule, std::stod(value));
      }
      catch (const std::exception&)
      {
        throw std::runtime_error("ConvertScenarioRule: String value \"" + value + "\" cannot be unambiguously converted into a scalar value.");
      }
    }
    else if constexpr (std::is_same_v<T, double>)
    {
      return Rule<double>(rule, value);
    }
    else if constexpr (std::is_same_v<T, NET_ASAM_OPENSCENARIO::DateTime>)
    {
      Logger::Error("ConvertScenarioRule for DateTime not implemented yet. Returning 0");
      return Rule<double>(rule, 0.0);
    }
  }
};

template <typename ValueType>
inline auto ConvertScenarioRule(NET_ASAM_OPENSCENARIO::v1_3::Rule rule, ValueType value)
{
  return ConvertScenarioRule_<ValueType>{rule, value};
}

}  // namespace OpenScenarioEngine::v1_3
