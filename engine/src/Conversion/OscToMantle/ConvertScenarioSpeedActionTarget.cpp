
/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioSpeedActionTarget.h"

namespace OpenScenarioEngine::v1_3
{
SpeedActionTarget ConvertScenarioSpeedActionTarget(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ISpeedActionTarget>& speedActionTarget)
{
  const auto absolute_target_speed = speedActionTarget->GetAbsoluteTargetSpeed();
  if (absolute_target_speed)
  {
    return units::velocity::meters_per_second_t(absolute_target_speed->GetValue());
  }

  const auto relative_target_speed = speedActionTarget->GetRelativeTargetSpeed();
  if (relative_target_speed)
  {
    /// TODO: continuous need to be taken into consideration at some point as well
    const auto &relative_entity_name = relative_target_speed->GetEntityRef()->GetNameRef();

    if (!environment->GetEntityRepository().Get(relative_entity_name).has_value())
    {
      throw std::runtime_error(
          "Relative Entity with name \"" + relative_entity_name +
          "\" of SpeedAction  does not exist. Please adjust the "
          "scenario.");
    }

    auto target_base = environment->GetEntityRepository().Get(relative_entity_name).value().get().GetVelocity();
    units::velocity::meters_per_second_t base_magnitude{target_base.Length()};

    if (relative_target_speed->GetSpeedTargetValueType() ==
        NET_ASAM_OPENSCENARIO::v1_3::SpeedTargetValueType::DELTA)
    {
      return base_magnitude + units::velocity::meters_per_second_t(relative_target_speed->GetValue());
    }
    if (relative_target_speed->GetSpeedTargetValueType() ==
        NET_ASAM_OPENSCENARIO::v1_3::SpeedTargetValueType::FACTOR)
    {
      return base_magnitude * relative_target_speed->GetValue();
    }
  }

  throw std::runtime_error("speedActionTarget needs to have either Relative or Absolute target speed");
}

}  // namespace OpenScenarioEngine::v1_3