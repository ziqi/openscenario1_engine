
/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>
#include <units.h>

#include <memory>
#include <string>

#include "Conversion/OscToMantle/ConvertScenarioEntity.h"

namespace OpenScenarioEngine::v1_3
{
using SpeedActionTarget = units::velocity::meters_per_second_t;

SpeedActionTarget ConvertScenarioSpeedActionTarget(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ISpeedActionTarget>& speedActionTarget);

}  // namespace OpenScenarioEngine::v1_3