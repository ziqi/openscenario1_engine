/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioConditionEdge.h"

#include "Utils/ConditionEdgeEvaluator.h"

namespace OpenScenarioEngine::v1_3
{
ConditionEdge ConvertScenarioConditionEdge(const NET_ASAM_OPENSCENARIO::v1_3::ConditionEdge& conditionEdge)
{
  if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_3::ConditionEdge::RISING)
  {
    return ConditionEdge::kRising;
  }
  if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_3::ConditionEdge::FALLING)
  {
    return ConditionEdge::kFalling;
  }
  if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_3::ConditionEdge::RISING_OR_FALLING)
  {
    return ConditionEdge::kRisingOrFalling;
  }
  if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_3::ConditionEdge::NONE)
  {
    return ConditionEdge::kNone;
  }

  throw std::runtime_error("ConvertScenarioConditionEdge: Unsupported ConditionEdge");
}

}  // namespace OpenScenarioEngine::v1_3
