
/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioTransitionDynamics.h"

#include <MantleAPI/Traffic/entity_helper.h>

#include <optional>

namespace OpenScenarioEngine::v1_3
{
namespace detail
{
FollowingMode ConvertToOse(const NET_ASAM_OPENSCENARIO::v1_3::FollowingMode& from)
{
  if (from == NET_ASAM_OPENSCENARIO::v1_3::FollowingMode::UNKNOWN)
  {
    // see https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine/-/merge_requests/134#note_1149414
    return FollowingMode::kPosition;
  }
  if (from == NET_ASAM_OPENSCENARIO::v1_3::FollowingMode::FOLLOW)
  {
    return FollowingMode::kFollow;
  }
  if (from == NET_ASAM_OPENSCENARIO::v1_3::FollowingMode::POSITION)
  {
    return FollowingMode::kPosition;
  }

  throw std::runtime_error("ConvertScenarioTransitionDynamics: Unsupported followingMode");
}

mantle_api::Shape ConvertToMantleApi(const NET_ASAM_OPENSCENARIO::v1_3::DynamicsShape& from)
{
  if (from == NET_ASAM_OPENSCENARIO::v1_3::DynamicsShape::STEP)
  {
    return mantle_api::Shape::kStep;
  }
  if (from == NET_ASAM_OPENSCENARIO::v1_3::DynamicsShape::CUBIC)
  {
    return mantle_api::Shape::kCubic;
  }
  if (from == NET_ASAM_OPENSCENARIO::v1_3::DynamicsShape::LINEAR)
  {
    return mantle_api::Shape::kLinear;
  }
  if (from == NET_ASAM_OPENSCENARIO::v1_3::DynamicsShape::SINUSOIDAL)
  {
    return mantle_api::Shape::kSinusoidal;
  }

  return mantle_api::Shape::kUndefined;
}

mantle_api::Dimension ConvertToMantleApi(const NET_ASAM_OPENSCENARIO::v1_3::DynamicsDimension& from)
{
  if (from == NET_ASAM_OPENSCENARIO::v1_3::DynamicsDimension::TIME)
  {
    return mantle_api::Dimension::kTime;
  }
  if (from == NET_ASAM_OPENSCENARIO::v1_3::DynamicsDimension::DISTANCE)
  {
    return mantle_api::Dimension::kDistance;
  }
  if (from == NET_ASAM_OPENSCENARIO::v1_3::DynamicsDimension::RATE)
  {
    return mantle_api::Dimension::kRate;
  }
  return mantle_api::Dimension::kUndefined;
}
}  // namespace detail

TransitionDynamics ConvertScenarioTransitionDynamics(
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITransitionDynamics>& transitionDynamics)
{
  return {
      {/*.dimension = */ detail::ConvertToMantleApi(transitionDynamics->GetDynamicsDimension()),
       /*.shape =     */ detail::ConvertToMantleApi(transitionDynamics->GetDynamicsShape()),
       /*.value =     */ transitionDynamics->GetValue()},
      transitionDynamics->IsSetFollowingMode()
          ? std::make_optional(detail::ConvertToOse(transitionDynamics->GetFollowingMode()))
          : std::nullopt};
}

}  // namespace OpenScenarioEngine::v1_3