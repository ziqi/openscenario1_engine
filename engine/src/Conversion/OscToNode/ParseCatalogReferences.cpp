/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseCatalogReferences.h"

#include <agnostic_behavior_tree/composite/parallel_node.h>

#include <memory>

#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{
yase::BehaviorNode::Ptr parse(std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference>> catalogReferences)
{
  // Method declaration is not changed as parameter catalogReferences seems to be used when the method is implemented.
  std::ignore = catalogReferences;
  Logger::Error("Parsing of std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference>> not implemented yet");
  return std::make_shared<yase::ParallelNode>("CatalogReferences");
}

}  // namespace OpenScenarioEngine::v1_3
