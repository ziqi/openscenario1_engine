/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseParameterDeclarations.h"

#include <agnostic_behavior_tree/composite/parallel_node.h>

#include <memory>

#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{
yase::BehaviorNode::Ptr parse(std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IParameterDeclaration>> parameterDeclarations)
{
  // Method declaration is not changed as parameter parameterDeclarations seems to be used when the method is implemented.
  std::ignore = parameterDeclarations;
  Logger::Error("Parsing of std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IParameterDeclaration>> not implemented yet");
  return std::make_shared<yase::ParallelNode>("ParameterDeclarations");
}

}  // namespace OpenScenarioEngine::v1_3
