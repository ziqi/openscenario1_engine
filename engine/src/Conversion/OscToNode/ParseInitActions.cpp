
/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseInitActions.h"

#include <agnostic_behavior_tree/composite/parallel_node.h>

#include "Conversion/OscToNode/ParseGlobalAction.h"
#include "Conversion/OscToNode/ParsePrivate.h"
#include "Conversion/OscToNode/ParseUserDefinedAction.h"

namespace OpenScenarioEngine::v1_3
{
yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IInitActions> initActions)
{
  auto initActionsNode = std::make_shared<yase::ParallelNode>("InitActions");
  for (const auto& action : initActions->GetPrivates())
  {
    initActionsNode->addChild(parse(action));
  }
  for (const auto& action : initActions->GetGlobalActions())
  {
    initActionsNode->addChild(parse(action));
  }
  for (const auto& action : initActions->GetUserDefinedActions())
  {
    initActionsNode->addChild(parse(action));
  }
  return initActionsNode;
}

}  // namespace OpenScenarioEngine::v1_3
