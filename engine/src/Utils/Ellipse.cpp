/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Utils/Ellipse.h"

namespace OpenScenarioEngine::v1_3
{

bool IsPointOutsideOfEllipse(const mantle_api::Vec3<units::length::meter_t>& point,
                             const mantle_api::Vec3<units::length::meter_t>& ellipse_center,
                             units::length::meter_t ellipse_semi_major_axis,
                             units::length::meter_t ellipse_semi_minor_axis,
                             units::angle::radian_t ellipse_angle,
                             units::length::meter_t tolerance)
{
    ellipse_semi_major_axis += tolerance; 
    ellipse_semi_minor_axis += tolerance; 

    const auto cos_angle{std::cos(ellipse_angle())};
    const auto sin_angle{std::sin(ellipse_angle())};

    const auto x_diff{point.x - ellipse_center.x};
    const auto y_diff{point.y - ellipse_center.y};

    const auto a{std::pow((cos_angle * x_diff()) + (sin_angle * y_diff()), 2)};
    const auto b{std::pow((sin_angle * x_diff()) - (cos_angle * y_diff()), 2)};

    const auto maj_sq{ellipse_semi_major_axis * ellipse_semi_major_axis};
    const auto min_sq{ellipse_semi_minor_axis * ellipse_semi_minor_axis};

    const auto ellipse{(a / maj_sq()) + (b / min_sq())};

    return ellipse > 1.0;
}

}  // namespace OpenScenarioEngine::v1_3