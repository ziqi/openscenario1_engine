/********************************************************************************
 * Copyright (c) 2022-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Utils/EntityUtils.h"

#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{
using namespace units::literals;

namespace detail
{
inline units::length::meter_t GetXValue(const mantle_api::Vec3<units::length::meter_t>& element)
{
  return element.x;
}

inline units::length::meter_t GetYValue(const mantle_api::Vec3<units::length::meter_t>& element)
{
  return element.y;
}

/// @brief calculate the component-wise distance between closest bounding box points of the two entities
/// in the base entity's coordinate system
/// @tparam GetValue function given a 3D vector in meters, providing the respective component of the vector
/// @param environment environment interface
/// @param base_entity entity where its coordinate system will be used as local coordinate system
/// @param target_entity entity to return distance to
/// @return minimum component-wise distance between two entities, calculated in the base's entity coordinate system
template <units::length::meter_t (*GetValue)(const mantle_api::Vec3<units::length::meter_t>&)>
static units::length::meter_t CalculateFreeSpaceDistance(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const mantle_api::IEntity& base_entity,
    const mantle_api::IEntity& target_entity)
{
  const auto local_entity_corner_points = EntityUtils::GetBoundingBoxCornerPoints(target_entity);
  const auto global_entity_corner_points = EntityUtils::GetBoundingBoxCornerPointsInGlobal(
      environment, target_entity.GetPosition(), target_entity.GetOrientation(), local_entity_corner_points);
  auto relative_entity_corner_points = environment->GetGeometryHelper()->TransformPolylinePointsFromWorldToLocal(
      global_entity_corner_points, base_entity.GetPosition(), base_entity.GetOrientation());

  auto less_than = [](const auto& lhs, const auto& rhs)
  { return GetValue(lhs) < GetValue(rhs); };

  std::sort(relative_entity_corner_points.begin(), relative_entity_corner_points.end(), less_than);
  const auto target_min = GetValue(relative_entity_corner_points[0]);
  const auto target_max = GetValue(relative_entity_corner_points[relative_entity_corner_points.size() - 1]);

  auto base_entity_corner_points = EntityUtils::GetBoundingBoxCornerPoints(base_entity);
  std::sort(base_entity_corner_points.begin(), base_entity_corner_points.end(), less_than);
  const auto base_min = GetValue(base_entity_corner_points[0]);
  const auto base_max = GetValue(base_entity_corner_points[base_entity_corner_points.size() - 1]);

  if ((base_min > target_min && base_min < target_max) ||
      (base_max > target_min && base_max < target_max))
  {
    Logger::Warning("CalculateFreeSpaceDistance: Entity \"" + base_entity.GetName() +
                    "\" and \"" + target_entity.GetName() + "\" are overlapping");
    return units::length::meter_t{0.0};
  }

  const auto minimum_distance = std::min({std::abs(base_min.value() - target_min.value()),
                                          std::abs(base_min.value() - target_max.value()),
                                          std::abs(base_max.value() - target_min.value()),
                                          std::abs(base_max.value() - target_max.value())});
  return units::length::meter_t{minimum_distance};
}
}  // namespace detail

std::vector<mantle_api::Vec3<units::length::meter_t>> EntityUtils::GetBoundingBoxCornerPoints(
    const mantle_api::IEntity& ref_entity)
{
  const auto bounding_box = ref_entity.GetProperties()->bounding_box;
  const auto half_length = bounding_box.dimension.length * 0.5;
  const auto half_width = bounding_box.dimension.width * 0.5;
  const auto half_height = bounding_box.dimension.height * 0.5;

  return {{half_length, half_width, -half_height},
          {half_length, -half_width, -half_height},
          {half_length, half_width, half_height},
          {half_length, -half_width, half_height},
          {-half_length, half_width, -half_height},
          {-half_length, -half_width, -half_height},
          {-half_length, half_width, half_height},
          {-half_length, -half_width, half_height}};
}

std::vector<mantle_api::Vec3<units::length::meter_t>> EntityUtils::GetBoundingBoxCornerPointsInGlobal(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const mantle_api::Vec3<units::length::meter_t>& position,
    const mantle_api::Orientation3<units::angle::radian_t>& orientation,
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& local_corner_points)
{
  std::vector<mantle_api::Vec3<units::length::meter_t>> corner_points;
  corner_points.reserve(local_corner_points.size());
  std::transform(local_corner_points.begin(),
                 local_corner_points.end(),
                 std::back_insert_iterator(corner_points),
                 [environment, position, orientation](const auto& point)
                 {
                   return environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
                       position, orientation, point);
                 });

  return corner_points;
}

std::vector<mantle_api::Vec3<units::length::meter_t>>
EntityUtils::GetCornerPositionsInLocalSortedByLongitudinalDistance(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const mantle_api::IEntity& entity,
    const mantle_api::Vec3<units::length::meter_t>& local_origin,
    const mantle_api::Orientation3<units::angle::radian_t>& local_orientation)
{
  const auto corner_points_global = GetBoundingBoxCornerPointsInGlobal(
      environment, entity.GetPosition(), entity.GetOrientation(), GetBoundingBoxCornerPoints(entity));

  auto corner_points_local = environment->GetGeometryHelper()->TransformPolylinePointsFromWorldToLocal(
      corner_points_global, local_origin, local_orientation);

  std::sort(
      corner_points_local.begin(), corner_points_local.end(), [](const auto& a, const auto& b)
      { return a.x < b.x; });

  return corner_points_local;
}

units::length::meter_t EntityUtils::CalculateLongitudinalFreeSpaceDistance(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const mantle_api::IEntity& base_entity,
    const mantle_api::IEntity& target_entity)
{
  return detail::CalculateFreeSpaceDistance<detail::GetXValue>(environment, base_entity, target_entity);
}

units::length::meter_t EntityUtils::CalculateLateralFreeSpaceDistance(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const mantle_api::IEntity& base_entity,
    const mantle_api::IEntity& target_entity)
{
  return detail::CalculateFreeSpaceDistance<detail::GetYValue>(environment, base_entity, target_entity);
}

units::length::meter_t EntityUtils::CalculateRelativeLongitudinalDistance(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const mantle_api::IEntity& base_entity,
    const mantle_api::IEntity& target_entity)

{
  const auto base_entity_origin_global_position = environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
      base_entity.GetPosition(),
      base_entity.GetOrientation(),
      -base_entity.GetProperties()->bounding_box.geometric_center);

  const auto target_entity_origin_global_position =
      environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
          target_entity.GetPosition(),
          target_entity.GetOrientation(),
          -target_entity.GetProperties()->bounding_box.geometric_center);

  const auto ref_entity_origin_local_position = environment->GetGeometryHelper()->TransformPositionFromWorldToLocal(
      target_entity_origin_global_position, base_entity_origin_global_position, base_entity.GetOrientation());

  return units::math::abs(ref_entity_origin_local_position.x);
}

units::length::meter_t EntityUtils::CalculateRelativeLongitudinalDistance(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const mantle_api::IEntity& base_entity,
    const mantle_api::Vec3<units::length::meter_t>& position)

{
  const auto base_entity_origin_global_position = environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
      base_entity.GetPosition(),
      base_entity.GetOrientation(),
      -base_entity.GetProperties()->bounding_box.geometric_center);

  const auto local_position = environment->GetGeometryHelper()->TransformPositionFromWorldToLocal(
      position, base_entity_origin_global_position, base_entity.GetOrientation());

  return units::math::abs(local_position.x);
}

mantle_api::IEntity& EntityUtils::GetEntityByName(const std::shared_ptr<mantle_api::IEnvironment>& environment,
                                                  const std::string& entity_name)
{
  if (auto entity = environment->GetEntityRepository().Get(entity_name); entity)
  {
    return entity.value().get();
  }
  throw std::runtime_error("EntityUtils: Entity with name \"" + entity_name +
                           "\" does not exist. Please adjust the scenario.");
}

void EntityUtils::RemoveEntity(const std::shared_ptr<mantle_api::IEnvironment>& environment,
                               const mantle_api::UniqueId& entity_id,
                               const std::vector<mantle_api::UniqueId>& controller_ids)
{
  for (const auto id : controller_ids)
  {
    environment->RemoveEntityFromController(entity_id, id);
    environment->GetControllerRepository().Delete(id);
  }

  if (entity_id == environment->GetEntityRepository().GetHost().GetUniqueId())
  {
    throw std::runtime_error("EntityUtils::RemoveEntity: Removing Ego entity is considered invalid. Please adjust the scenario.");
  }
  environment->GetEntityRepository().Delete(entity_id);
}

}  // namespace OpenScenarioEngine::v1_3
