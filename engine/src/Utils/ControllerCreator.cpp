/*******************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ControllerCreator.h"

#include <openScenarioLib/generated/v1_3/catalog/CatalogHelperV1_3.h>

#include <cassert>

#include "MantleAPI/Traffic/i_controller_repository.h"
#include "Utils/Constants.h"
#include "Utils/ControllerService.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{
namespace detail
{
bool IsCatalogReferenceControllableObject(
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference>& catalog_reference)
{
  if (auto catalog_element = catalog_reference->GetRef())
  {
    return NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::IsVehicle(catalog_element) ||
           NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::IsPedestrian(catalog_element);
  }
  throw std::runtime_error("ControllerCreator: CatalogReference \"" + catalog_reference->GetEntryName() + "\"" +
                           " for entity object cannot be resolved.");
}

auto CreateExternalControllerConfig(
    const std::string& controller_name,
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IController>& controller)
{
  auto external_config = std::make_unique<mantle_api::ExternalControllerConfig>();

  if (controller)
  {
    external_config->name = controller_name;
    for (const auto& property : controller->GetProperties()->GetProperties())
    {
      external_config->parameters.emplace(property->GetName(), property->GetValue());
    }
  }

  return external_config;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IController> Parse(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IObjectController>& object_controller)
{
  if (auto controller = object_controller->GetController())
  {
    return controller;
  }
  if (auto catalog_reference = object_controller->GetCatalogReference())
  {
    if (auto ref = catalog_reference->GetRef();
        NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::IsController(ref))
    {
      const auto& controller = NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::AsController(ref);
      return controller;
    }
    throw std::runtime_error("ControllerCreator: CatalogReference \"" + catalog_reference->GetEntryName() + "\"" +
                             " for object controller cannot be resolved.");
  }
  throw std::runtime_error(
      "ControllerCreator: Object controller without controller or "
      "catalog reference defined in the scenario. "
      "Please correct the scenario.");
}

auto CreateExternalControllerConfig(
    const std::string& controller_name,
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IObjectController>& object_controller)
{
  return detail::CreateExternalControllerConfig(controller_name, detail::Parse(object_controller));
}

bool IsNotControllable(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IScenarioObject>& scenario_object)
{
  auto entity_object = scenario_object->GetEntityObject();

  if (!entity_object)
  {
    throw std::runtime_error("ControllerCreator: EntityObject missing in ScenarioObject \"" + scenario_object->GetName() + "\"");
  }

  if (entity_object->GetVehicle() || entity_object->GetPedestrian())
  {
    return false;
  }

  if (entity_object->GetCatalogReference())
  {
    return !detail::IsCatalogReferenceControllableObject(entity_object->GetCatalogReference());
  }

  return true;
}

}  // namespace detail

ControllerCreator::ControllerCreator(mantle_api::IEnvironment& environment)
    : environment_{environment},
      controller_service_{std::make_shared<ControllerService>()}
{
}

void ControllerCreator::EnableUserDefinedControllerOverride() noexcept
{
  user_defined_controller_override_ = true;
}

ControllerServicePtr ControllerCreator::GetControllerService()
{
  return controller_service_;
}

void ControllerCreator::CreateControllers(const std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IScenarioObject>>& scenario_objects)
{
  for (auto&& scenario_object : scenario_objects)
  {
    if (!scenario_object)
    {
      continue;
    }

    const auto entity_name = scenario_object->GetName();
    if (detail::IsNotControllable(scenario_object))
    {
      Logger::Info("ControllerCreator: No controller created for non-vehicle and non-pedestrian type scenario object \"" + entity_name + "\"");
      continue;
    }

    const auto entity = environment_.GetEntityRepository().Get(entity_name);
    if (!entity.has_value())
    {
      throw std::runtime_error("ControllerCreator: Trying to create controller for unavailable entity \"" + entity_name + "\"");
    }

    auto& controller_repository = environment_.GetControllerRepository();
    ControllerRegistrar registrar{
        *entity,
        entity_name,
        scenario_object->GetObjectController(),
        environment_,
        controller_repository,
        controller_service_};

    registrar.CreateDefaultController();
    registrar.CreateUserDefinedControllers(user_defined_controller_override_);
  }
}

ControllerRegistrar::ControllerRegistrar(
    mantle_api::IEntity& entity,
    const std::string& entity_name,
    std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IObjectController>>&& object_controllers,
    mantle_api::IEnvironment& environment,
    mantle_api::IControllerRepository& controller_repository,
    std::shared_ptr<ControllerService>& controller_service)
    : entity_{entity},
      entity_name_{entity_name},
      object_controllers_{std::move(object_controllers)},
      environment_{environment},
      controller_repository_{controller_repository},
      controller_service_{controller_service}
{
}

void ControllerRegistrar::CreateDefaultController()
{
  Logger::Info("ControllerCreator: Setting up internal controller for entity \"" + entity_name_ + "\"");

  auto default_config = std::make_unique<mantle_api::InternalControllerConfig>();
  default_config->control_strategies.push_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());
  default_config->control_strategies.push_back(std::make_unique<mantle_api::KeepLaneOffsetControlStrategy>());
  default_config->name = CONTROLLER_NAME_DEFAULT;
  auto& controller = controller_repository_.Create(std::move(default_config));
  controller.ChangeState(mantle_api::IController::LateralState::kActivate, mantle_api::IController::LongitudinalState::kActivate);
  RegisterDefaultController(controller);
}

void ControllerRegistrar::CreateUserDefinedControllers(bool control_override)
{
  if (object_controllers_.empty())
  {
    if (control_override && IsDrivingFunctionControlledEntity(entity_name_))
    {
      auto external_config = std::make_unique<mantle_api::ExternalControllerConfig>();
      external_config->name = CONTROLLER_NAME_OVERRIDE;

      Logger::Info("ControllerRegistrar: Setting up external controller \"" + external_config->name + "\" for entity \"" + entity_name_ + "\"");
      auto& controller = controller_repository_.Create(std::move(external_config));
      RegisterUserDefinedController(controller);
      controller_service_->ChangeState(entity_.GetUniqueId(), controller.GetUniqueId(), mantle_api::IController::LateralState::kActivate, mantle_api::IController::LongitudinalState::kActivate);
    }
  }
  else
  {
    for (const auto& object_controller : object_controllers_)
    {
      auto controller_name = detail::Parse(object_controller)->GetName();
      auto external_config = detail::CreateExternalControllerConfig(controller_name, object_controller);

      Logger::Info("ControllerRegistrar: Setting up external controller \"" + external_config->name + "\" for entity \"" + entity_name_ + "\"");
      auto& controller = controller_repository_.Create(std::move(external_config));
      RegisterUserDefinedController(controller);
    }
  }
}

void ControllerRegistrar::RegisterController(mantle_api::IController& controller, bool is_default)
{
  environment_.AddEntityToController(entity_, controller.GetUniqueId());
  auto& entity_controllers = controller_service_->controllers[entity_.GetUniqueId()];
  if (is_default)
  {
    entity_controllers.internal = {controller.GetUniqueId(), &controller};
  }
  else
  {
    assert(entity_controllers.internal.second != nullptr);
    entity_controllers.user_defined[controller.GetUniqueId()] = &controller;
  }
  controller_service_->mapping[controller.GetName()] = controller.GetUniqueId();
}

void ControllerRegistrar::RegisterDefaultController(mantle_api::IController& controller)
{
  RegisterController(controller, true);
}

void ControllerRegistrar::RegisterUserDefinedController(mantle_api::IController& controller)
{
  RegisterController(controller, false);
}

}  // namespace OpenScenarioEngine::v1_3
