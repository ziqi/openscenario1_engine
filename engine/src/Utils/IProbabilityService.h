/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

namespace OpenScenarioEngine::v1_3
{
/// Provides methods for centralized handling of probability
class IProbabilityService
{
public:
  /// Default destructor
  virtual ~IProbabilityService() = default;

  /// Sample from an real uniform distribution
  ///
  /// @param scale description
  /// @returns a value between [0 and scale]
  [[nodiscard]] virtual double SampleRealUniformValue(double scale) noexcept = 0;
};

}  // namespace OpenScenarioEngine::v1_3