/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <random>

#include "IProbabilityService.h"

namespace OpenScenarioEngine::v1_3
{
class ProbabilityService : public IProbabilityService
{
public:
  explicit ProbabilityService(std::random_device::result_type seed);

  [[nodiscard]] double SampleRealUniformValue(double scale) noexcept override;

  /// Set the random seed and instantiate a new generator
  /// @param seed   new random seed
  void SetRandomSeed(std::random_device::result_type seed);

  /// Get the currently used random seed
  /// @return seed
  std::random_device::result_type GetRandomSeed() const;

private:
  std::random_device::result_type seed_;
  std::mt19937 generator_;
  std::uniform_real_distribution<double> uniform_real_distribution;
};

}  // namespace OpenScenarioEngine::v1_3