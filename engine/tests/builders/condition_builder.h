/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

namespace testing::OpenScenarioEngine::v1_3
{
class FakeSimulationTimeCondition : public NET_ASAM_OPENSCENARIO::v1_3::ISimulationTimeCondition
{
  public:
    FakeSimulationTimeCondition(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum rule_enum, double value);
    double GetValue() const override;
    NET_ASAM_OPENSCENARIO::v1_3::Rule GetRule() const override;
    bool IsSetRule() const override { return true; }
    bool IsSetValue() const override { return true; }

  private:
    NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum rule_enum_{};
    double value_{};
};

class FakeByValueCondition : public NET_ASAM_OPENSCENARIO::v1_3::IByValueCondition
{
  public:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IParameterCondition> GetParameterCondition() const override;
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITimeOfDayCondition> GetTimeOfDayCondition() const override;
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ISimulationTimeCondition> GetSimulationTimeCondition() const override;
    void SetSimulationTimeCondition(const FakeSimulationTimeCondition& simulation_time_condition);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IStoryboardElementStateCondition> GetStoryboardElementStateCondition()
        const override;
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IUserDefinedValueCondition> GetUserDefinedValueCondition()
        const override;
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficSignalCondition> GetTrafficSignalCondition() const override;
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficSignalControllerCondition>
    GetTrafficSignalControllerCondition() const override;
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IVariableCondition> GetVariableCondition() const override;

    bool IsSetParameterCondition() const override { return true; }
    bool IsSetTimeOfDayCondition() const override { return true; }
    bool IsSetSimulationTimeCondition() const override { return true; }
    bool IsSetStoryboardElementStateCondition() const override { return true; }
    bool IsSetUserDefinedValueCondition() const override { return true; }
    bool IsSetTrafficSignalCondition() const override { return true; }
    bool IsSetTrafficSignalControllerCondition() const override { return true; }
    bool IsSetVariableCondition() const override { return true; }

  private:
    std::shared_ptr<FakeSimulationTimeCondition> simulation_time_condition_{nullptr};
};

class FakeByValueConditionBuilder
{
  public:
    FakeByValueConditionBuilder& WithSimulationTimeCondition(
        const FakeSimulationTimeCondition& simulation_time_condition);
    FakeByValueCondition Build();

  private:
    FakeByValueCondition by_value_condition_{};
};

class FakeCondition : public NET_ASAM_OPENSCENARIO::v1_3::ICondition
{
  public:
    std::string GetName() const override;
    double GetDelay() const override;
    NET_ASAM_OPENSCENARIO::v1_3::ConditionEdge GetConditionEdge() const override;
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IByEntityCondition> GetByEntityCondition() const override
    {
        return nullptr;
    }
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IByValueCondition> GetByValueCondition() const override;
    void SetByValueCondition(const FakeByValueCondition& by_value_condition);
    bool IsSetByEntityCondition() const override { return true; }
    bool IsSetByValueCondition() const override { return true; }
    bool IsSetConditionEdge() const override { return true; }
    bool IsSetDelay() const override { return true; }
    bool IsSetName() const override { return true; }

  private:
    NET_ASAM_OPENSCENARIO::v1_3::ConditionEdge::ConditionEdgeEnum condition_edge_enum_{
        NET_ASAM_OPENSCENARIO::v1_3::ConditionEdge::ConditionEdgeEnum::NONE};
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IByValueCondition> by_value_condition_{};
};

class FakeConditionBuilder
{
  public:
    FakeConditionBuilder& WithByValueCondition(const FakeByValueCondition& by_value_condition);
    FakeCondition Build();

  private:
    FakeCondition condition_{};
};

class FakeConditionGroup : public NET_ASAM_OPENSCENARIO::v1_3::IConditionGroup
{
  public:
    std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICondition>> GetConditions() const override;
    void SetConditions(std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICondition>>& conditions);
    void AddCondition(FakeCondition condition);
    bool IsSetConditions() const override { return true; }

  private:
    std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICondition>> conditions_{};
};

class FakeConditionGroupBuilder
{
  public:
    FakeConditionGroupBuilder& WithCondition(FakeCondition condition);
    FakeConditionGroup Build();

  private:
    FakeConditionGroup condition_group_{};
};
}  // namespace testing::OpenScenarioEngine::v1_3
