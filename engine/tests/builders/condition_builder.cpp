/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "builders/condition_builder.h"

#include <openScenarioLib/src/impl/NamedReferenceProxy.h>

namespace testing::OpenScenarioEngine::v1_3
{
FakeSimulationTimeCondition::FakeSimulationTimeCondition(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum rule_enum,
                                                         double value)
    : rule_enum_(std::move(rule_enum)), value_(value)
{
}

double FakeSimulationTimeCondition::GetValue() const
{
    return value_;
}

NET_ASAM_OPENSCENARIO::v1_3::Rule FakeSimulationTimeCondition::GetRule() const
{
  return NET_ASAM_OPENSCENARIO::v1_3::Rule(rule_enum_);
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IParameterCondition> FakeByValueCondition::GetParameterCondition() const
{
    return IByValueCondition::GetParameterCondition();
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITimeOfDayCondition> FakeByValueCondition::GetTimeOfDayCondition() const
{
    return IByValueCondition::GetTimeOfDayCondition();
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ISimulationTimeCondition>
FakeByValueCondition::GetSimulationTimeCondition() const
{
    return simulation_time_condition_;
}

void FakeByValueCondition::SetSimulationTimeCondition(const FakeSimulationTimeCondition& simulation_time_condition)
{
    simulation_time_condition_ = std::make_shared<FakeSimulationTimeCondition>(simulation_time_condition);
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IStoryboardElementStateCondition>
FakeByValueCondition::GetStoryboardElementStateCondition() const
{
    return IByValueCondition::GetStoryboardElementStateCondition();
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IUserDefinedValueCondition>
FakeByValueCondition::GetUserDefinedValueCondition() const
{
    return IByValueCondition::GetUserDefinedValueCondition();
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficSignalCondition> FakeByValueCondition::GetTrafficSignalCondition()
    const
{
    return IByValueCondition::GetTrafficSignalCondition();
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficSignalControllerCondition>
FakeByValueCondition::GetTrafficSignalControllerCondition() const
{
    return IByValueCondition::GetTrafficSignalControllerCondition();
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IVariableCondition>
FakeByValueCondition::GetVariableCondition() const
{
    return IByValueCondition::GetVariableCondition();
}

FakeByValueConditionBuilder& FakeByValueConditionBuilder::WithSimulationTimeCondition(
    const FakeSimulationTimeCondition& simulation_time_condition)
{
    by_value_condition_.SetSimulationTimeCondition(simulation_time_condition);
    return *this;
}

FakeByValueCondition FakeByValueConditionBuilder::Build()
{
    return by_value_condition_;
}

std::string FakeCondition::GetName() const
{
  return NET_ASAM_OPENSCENARIO::v1_3::ICondition::GetName();
}

double FakeCondition::GetDelay() const
{
    return 0.0;
}

NET_ASAM_OPENSCENARIO::v1_3::ConditionEdge FakeCondition::GetConditionEdge() const
{
  return NET_ASAM_OPENSCENARIO::v1_3::ConditionEdge(condition_edge_enum_);
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IByValueCondition> FakeCondition::GetByValueCondition() const
{
    return by_value_condition_;
}

void FakeCondition::SetByValueCondition(const FakeByValueCondition& by_value_condition)
{
    by_value_condition_ = std::make_shared<FakeByValueCondition>(by_value_condition);
}

FakeConditionBuilder& FakeConditionBuilder::WithByValueCondition(const FakeByValueCondition& by_value_condition)
{
    condition_.SetByValueCondition(by_value_condition);
    return *this;
}

FakeCondition FakeConditionBuilder::Build()
{
    return condition_;
}

std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICondition>> FakeConditionGroup::GetConditions() const
{
    return conditions_;
}

void FakeConditionGroup::SetConditions(
    std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICondition>>& conditions)
{
    conditions_ = std::move(conditions);
}

void FakeConditionGroup::AddCondition(FakeCondition condition)
{
    conditions_.push_back(std::make_shared<FakeCondition>(std::move(condition)));
}

FakeConditionGroupBuilder& FakeConditionGroupBuilder::WithCondition(FakeCondition condition)
{
    condition_group_.AddCondition(std::move(condition));
    return *this;
}

FakeConditionGroup FakeConditionGroupBuilder::Build()
{
    return condition_group_;
}

}  // namespace testing::OpenScenarioEngine::v1_3
