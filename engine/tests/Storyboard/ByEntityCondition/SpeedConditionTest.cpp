/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

#include "Conversion/OscToMantle/ConvertScenarioRule.h"
#include "Storyboard/ByEntityCondition/SpeedCondition.h"
#include "TestUtils/FakeCompositeNode.h"

using namespace mantle_api;
using units::literals::operator""_mps;

class SpeedConditionTestFixture : public ::testing::Test
{
protected:
  void SetUp() override
  {
    mock_environment_ = std::make_shared<MockEnvironment>();
    osc_api_speed_condition_ = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::SpeedConditionImpl>();
    osc_api_speed_condition_->SetRule(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum::LESS_THAN);
    osc_api_speed_condition_->SetValue(1.0);
  }

  std::shared_ptr<MockEnvironment> mock_environment_;
  OpenScenarioEngine::v1_3::SpeedCondition::Values condition_values_{.triggeringEntity = "vehicle1",
                                                                     .rule = OpenScenarioEngine::v1_3::Rule(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum::LESS_THAN, 1.0)};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::SpeedConditionImpl> osc_api_speed_condition_{nullptr};
};

TEST_F(SpeedConditionTestFixture, GivenSpeedConditionNodeWithoutEntityBrokerInParentBlackboard_WhenCheckingCondition_ThenDoesNotThrow)
{
  auto fake_composite_node = testing::OpenScenarioEngine::v1_3::FakeCompositeNode(mock_environment_);
  auto speed_condition = std::make_shared<OpenScenarioEngine::v1_3::Node::SpeedCondition>(osc_api_speed_condition_);
  fake_composite_node.addChild(speed_condition);
  fake_composite_node.distributeData();
  fake_composite_node.executeTick();
  EXPECT_NO_THROW(speed_condition->distributeData());
  EXPECT_NO_THROW(speed_condition->executeTick());
}

TEST_F(SpeedConditionTestFixture, GivenSpeedConditionNodeWithEntityBrokerInParentBlackboard_WhenCheckingCondition_ThenEntityBrokerContainsTriggeringEntity)
{
  auto fake_composite_node = testing::OpenScenarioEngine::v1_3::FakeCompositeNode(mock_environment_, true);
  auto speed_condition = std::make_shared<OpenScenarioEngine::v1_3::Node::SpeedCondition>(osc_api_speed_condition_);
  fake_composite_node.addChild(speed_condition);
  fake_composite_node.distributeData();
  fake_composite_node.executeTick();
  EXPECT_NO_THROW(speed_condition->distributeData());
  EXPECT_NO_THROW(speed_condition->executeTick());
  EXPECT_GE(fake_composite_node.getEntityBroker()->GetEntities().size(), 1);
}

TEST_F(SpeedConditionTestFixture, GivenSpeedConditionImplWithMockedEntities_WhenCheckingCondition_ThenDoesNotThrow)
{
  OpenScenarioEngine::v1_3::SpeedCondition speedCondition(condition_values_,
                                                          {mock_environment_});

  EXPECT_NO_THROW([[maybe_unused]] auto _ = speedCondition.IsSatisfied());
}

TEST_F(SpeedConditionTestFixture, GivenSpeedConditionImplWithSatisfcatoryVelocity_WhenCheckingCondition_ThenReturnsTrue)
{
  OpenScenarioEngine::v1_3::SpeedCondition speedCondition(condition_values_,
                                                          {mock_environment_});

  EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle &>(mock_environment_->GetEntityRepository().Get(condition_values_.triggeringEntity).value().get()),
              GetVelocity())
      .WillOnce(::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{0.0_mps, 0_mps, 0_mps}));

  EXPECT_TRUE(speedCondition.IsSatisfied());
}

TEST_F(SpeedConditionTestFixture, GivenSpeedConditionImplWithUnsatisfcatoryVelocity_WhenCheckingCondition_ThenReturnsFalse)
{
  OpenScenarioEngine::v1_3::SpeedCondition speedCondition(condition_values_,
                                                          {mock_environment_});

  EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle &>(mock_environment_->GetEntityRepository().Get(condition_values_.triggeringEntity).value().get()),
              GetVelocity())
      .WillOnce(::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{10.0_mps, 0_mps, 0_mps}));

  EXPECT_THAT(speedCondition.IsSatisfied(), false);
}
