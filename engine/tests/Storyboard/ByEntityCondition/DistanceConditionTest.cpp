/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioRule.h"
#include "Storyboard/ByEntityCondition/DistanceCondition_impl.h"
#include "TestUtils/TestLogger.h"

using namespace mantle_api;
using namespace units::literals;

using testing::_;
using testing::HasSubstr;
using testing::Return;

class DistanceConditionTestFixture : public ::testing::Test
{
protected:
  void SetUp() override
  {
    LOGGER = std::make_unique<testing::OpenScenarioEngine::v1_3::TestLogger>();
    mock_environment_ = std::make_shared<MockEnvironment>();
    reference_pose_ = {{10.0_m, 20.0_m, 30.0_m}, {}};
  }

  std::shared_ptr<MockEnvironment> mock_environment_;
  Pose reference_pose_;
  OpenScenarioEngine::v1_3::DistanceCondition::Values condition_values_{.triggeringEntity = "vehicle",
                                                                        .alongRoute = false,
                                                                        .freespace = false,
                                                                        .GetPosition = [&]() { return reference_pose_; },
                                                                        .coordinateSystem = OpenScenarioEngine::v1_3::CoordinateSystem::kEntity,
                                                                        .relativeDistanceType = OpenScenarioEngine::v1_3::RelativeDistanceType::kLongitudinal,
                                                                        .rule = OpenScenarioEngine::v1_3::Rule(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum::LESS_OR_EQUAL, 10.0)};

  std::unique_ptr<testing::OpenScenarioEngine::v1_3::TestLogger> LOGGER;
};

TEST_F(DistanceConditionTestFixture, GivenUnsupportedCoordinateSystem_WhenDistanceConditionIsCalled_ThenReturnWithError)
{
  condition_values_.coordinateSystem = OpenScenarioEngine::v1_3::CoordinateSystem::kUnknown;

  OpenScenarioEngine::v1_3::DistanceCondition distance_condition(condition_values_,
                                                                 {mock_environment_});

  distance_condition.IsSatisfied();

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kError);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("Selected relativeDistanceType or coordinateSystem not implemented yet"));
}

TEST_F(DistanceConditionTestFixture, GivenUnsupportedRelativeDistanceType_WhenDistanceConditionIsCalled_ThenReturnWithError)
{
  condition_values_.relativeDistanceType = OpenScenarioEngine::v1_3::RelativeDistanceType::kLateral;

  OpenScenarioEngine::v1_3::DistanceCondition distance_condition(condition_values_,
                                                                 {mock_environment_});

  distance_condition.IsSatisfied();

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kError);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("Selected relativeDistanceType or coordinateSystem not implemented yet"));
}

TEST_F(
    DistanceConditionTestFixture,
    GivenConditionWithLongitudinalDistanceWithEntityCoordinateSystemWithFreeSpaceFalse_WhenDistanceConditionIsCalled_ThenReturnTrue)
{
  Pose reference_pose = {{1.0_m, 2.0_m, 3.0_m}, {}};
  condition_values_.GetPosition = [&]()
  { return reference_pose; };

  auto rule = OpenScenarioEngine::v1_3::Rule(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum::LESS_OR_EQUAL, 1.0);
  condition_values_.rule = rule;

  OpenScenarioEngine::v1_3::DistanceCondition distance_condition(condition_values_,
                                                                 {mock_environment_});
  ASSERT_TRUE(distance_condition.IsSatisfied());
}

TEST_F(
    DistanceConditionTestFixture,
    GivenConditionWithLongitudinalDistanceWithEntityCoordinateSystemWithFreeSpaceTrue_WhenDistanceConditionIsCalled_ThenReturnTrue)
{
  condition_values_.freespace = true;

  OpenScenarioEngine::v1_3::DistanceCondition distance_condition(condition_values_,
                                                                 {mock_environment_});
  ASSERT_TRUE(distance_condition.IsSatisfied());

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kError);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("Freespace not implemented. Falling back to default (false)."));
}

TEST_F(
    DistanceConditionTestFixture,
    GivenDistanceLessThanDistanceBetweenEntityAndPosition_WhenDistanceConditionIsCalled_ThenReturnFalse)
{
  auto rule = OpenScenarioEngine::v1_3::Rule(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum::LESS_OR_EQUAL, 9.0);
  condition_values_.rule = rule;

  OpenScenarioEngine::v1_3::DistanceCondition distance_condition(condition_values_,
                                                                 {mock_environment_});
  ASSERT_FALSE(distance_condition.IsSatisfied());
}
