/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Storyboard/ByEntityCondition/ReachPositionCondition_impl.h"

using namespace mantle_api;
using namespace units::literals;

TEST(ReachPositionCondition_UnitTest, GivenPositionWithinTolerance_ReturnsTrue)
{
  OpenScenarioEngine::v1_3::ReachPositionCondition
      reachPositionCondition({{""},
                              4,
                              []() { return std::make_optional<Pose>({{1.0_m, 2.0_m, 3.0_m}, {}}); }},
                             {std::make_shared<MockEnvironment>()});

  ASSERT_TRUE(reachPositionCondition.IsSatisfied());
}

TEST(ReachPositionCondition_UnitTest, GivenPositionOutsideTolerance_ReturnFalse)
{
  OpenScenarioEngine::v1_3::ReachPositionCondition
      reachPositionCondition({{""},
                              1.2,
                              []() { return std::make_optional<Pose>({{1.0_m, 2.0_m, 3.0_m}, {}}); }},
                             {std::make_shared<MockEnvironment>()});

  ASSERT_FALSE(reachPositionCondition.IsSatisfied());
}