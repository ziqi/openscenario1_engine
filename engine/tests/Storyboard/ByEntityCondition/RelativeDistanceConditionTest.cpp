/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioRule.h"
#include "Storyboard/ByEntityCondition/RelativeDistanceCondition_impl.h"
#include "TestUtils/TestLogger.h"

using namespace mantle_api;
using namespace units::literals;

using testing::Combine;
using testing::HasSubstr;
using testing::Return;
using testing::Values;

class RelativeDistanceConditionTestFixture : public ::testing::Test
{
protected:
  void SetUp() override
  {
    mock_environment_ = std::make_shared<MockEnvironment>();
  }

  mantle_api::MockVehicle& GET_VEHICLE_MOCK_WITH_NAME(const std::string& name)
  {
    return dynamic_cast<mantle_api::MockVehicle&>(mock_environment_->GetEntityRepository().Get(name).value().get());
  }

  std::shared_ptr<MockEnvironment> mock_environment_;
  OpenScenarioEngine::v1_3::RelativeDistanceCondition::Values condition_values_{.triggeringEntity = "vehicle1",
                                                                                .freespace = true,
                                                                                .entityRef = "Ego",
                                                                                .coordinateSystem = OpenScenarioEngine::v1_3::CoordinateSystem::kEntity,
                                                                                .relativeDistanceType = OpenScenarioEngine::v1_3::RelativeDistanceType::kLongitudinal,
                                                                                .rule = OpenScenarioEngine::v1_3::Rule(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum::EQUAL_TO, 10.0)};
};

TEST_F(RelativeDistanceConditionTestFixture, GivenConditionWithLateralDistanceWithEntityCoordinateSystemWithFreeSpaceTrue_WhenRelativeDistanceConditionIsCalled_ThenMockVehicleIsCalled)
{
  condition_values_.relativeDistanceType = OpenScenarioEngine::v1_3::RelativeDistanceType::kLateral;

  auto& mock_vehicle = GET_VEHICLE_MOCK_WITH_NAME(condition_values_.entityRef);

  EXPECT_CALL(mock_vehicle, GetPosition())
      .WillOnce(Return(mantle_api::Vec3<units::length::meter_t>{5_m, 0_m, 0_m}))
      .WillOnce(Return(mantle_api::Vec3<units::length::meter_t>{15_m, 5_m, 0_m}));

  EXPECT_CALL(mock_vehicle, GetOrientation())
      .WillRepeatedly(Return(mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 0_rad}));

  OpenScenarioEngine::v1_3::RelativeDistanceCondition relative_distance_condition(condition_values_,
                                                                                  {mock_environment_});
  // Note: The actual return value of the condition is not being checked,
  // as the MockEnvironment cannot calculate relative distances correctly.
  [[maybe_unused]] auto _ = relative_distance_condition.IsSatisfied();
}

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionWithLongitudinalDistanceWithEntityCoordinateSystemWithFreeSpaceFalse_WhenRelativeDistanceConditionIsCalled_ThenMockVehicleIsCalled)
{
  condition_values_.freespace = false;

  auto& mock_vehicle = GET_VEHICLE_MOCK_WITH_NAME(condition_values_.entityRef);

  EXPECT_CALL(mock_vehicle, GetPosition())
      .WillOnce(Return(mantle_api::Vec3<units::length::meter_t>{5_m, 0_m, 0_m}))
      .WillOnce(Return(mantle_api::Vec3<units::length::meter_t>{15_m, 0_m, 0_m}));

  EXPECT_CALL(mock_vehicle, GetOrientation())
      .WillRepeatedly(Return(mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 0_rad}));

  OpenScenarioEngine::v1_3::RelativeDistanceCondition relative_distance_condition(condition_values_,
                                                                                  {mock_environment_});
  // Note: The actual return value of the condition is not being checked,
  // as the MockEnvironment cannot calculate relative distances correctly.
  [[maybe_unused]] auto _ = relative_distance_condition.IsSatisfied();
}

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionWithLongitudinalDistanceWithEntityCoordinateSystemWithFreeSpaceTrue_WhenRelativeDistanceConditionIsCalled_ThenMockVehicleIsCalled)
{
  auto& mock_vehicle = GET_VEHICLE_MOCK_WITH_NAME(condition_values_.entityRef);

  EXPECT_CALL(mock_vehicle, GetPosition())
      .WillOnce(Return(mantle_api::Vec3<units::length::meter_t>{5_m, 0_m, 0_m}))
      .WillOnce(Return(mantle_api::Vec3<units::length::meter_t>{15_m, 0_m, 0_m}));

  EXPECT_CALL(mock_vehicle, GetOrientation())
      .WillRepeatedly(Return(mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 0_rad}));

  OpenScenarioEngine::v1_3::RelativeDistanceCondition relative_distance_condition(condition_values_,
                                                                                  {mock_environment_});
  // Note: The actual return value of the condition is not being checked,
  // as the MockEnvironment cannot calculate relative distances correctly.
  [[maybe_unused]] auto _ = relative_distance_condition.IsSatisfied();
}

class UnsupportedRelativeDistanceConditionTestFixture : public testing::TestWithParam<std::tuple<OpenScenarioEngine::v1_3::CoordinateSystem, bool, OpenScenarioEngine::v1_3::RelativeDistanceType>>
{
protected:
  void SetUp() override
  {
    LOGGER = std::make_unique<testing::OpenScenarioEngine::v1_3::TestLogger>();
    mock_environment_ = std::make_shared<MockEnvironment>();
  }

  std::shared_ptr<MockEnvironment> mock_environment_;
  OpenScenarioEngine::v1_3::RelativeDistanceCondition::Values condition_values_{.triggeringEntity = "vehicle1",
                                                                                .freespace = true,
                                                                                .entityRef = "Ego",
                                                                                .coordinateSystem = OpenScenarioEngine::v1_3::CoordinateSystem::kEntity,
                                                                                .relativeDistanceType = OpenScenarioEngine::v1_3::RelativeDistanceType::kLongitudinal,
                                                                                .rule = OpenScenarioEngine::v1_3::Rule(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum::EQUAL_TO, 10.0)};

  std::unique_ptr<testing::OpenScenarioEngine::v1_3::TestLogger> LOGGER;
};

INSTANTIATE_TEST_SUITE_P(UnsupportedRelativeDistanceConditionTest, UnsupportedRelativeDistanceConditionTestFixture, Combine(Values(OpenScenarioEngine::v1_3::CoordinateSystem::kUnknown, OpenScenarioEngine::v1_3::CoordinateSystem::kLane, OpenScenarioEngine::v1_3::CoordinateSystem::kTrajectory, OpenScenarioEngine::v1_3::CoordinateSystem::kLane), Values(true, false), Values(OpenScenarioEngine::v1_3::RelativeDistanceType::kLateral, OpenScenarioEngine::v1_3::RelativeDistanceType::kLongitudinal, OpenScenarioEngine::v1_3::RelativeDistanceType::kCartesian_distance, OpenScenarioEngine::v1_3::RelativeDistanceType::kEuclidian_distance)));
INSTANTIATE_TEST_SUITE_P(UnsupportedRelativeLateralDistanceConditionTest, UnsupportedRelativeDistanceConditionTestFixture, Values(std::make_tuple(OpenScenarioEngine::v1_3::CoordinateSystem::kEntity, false, OpenScenarioEngine::v1_3::RelativeDistanceType::kLateral)));

TEST_P(
    UnsupportedRelativeDistanceConditionTestFixture,
    GivenUnsupportedParameters_WhenRelativeDistanceConditionIsCalled_ThenPreconditionIsNotMetAndReturnsFalse)
{
  std::tie(condition_values_.coordinateSystem, condition_values_.freespace, condition_values_.relativeDistanceType) = GetParam();

  OpenScenarioEngine::v1_3::RelativeDistanceCondition relative_distance_condition(condition_values_,
                                                                                  {mock_environment_});
  EXPECT_FALSE(relative_distance_condition.IsSatisfied());

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kError);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("Selected relativeDistanceType or coordinateSystem not implemented yet"));
}
