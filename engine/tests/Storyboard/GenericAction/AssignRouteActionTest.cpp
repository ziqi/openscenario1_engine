/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include "builders/ActionBuilder.h"
#include "builders/PositionBuilder.h"
#include "Conversion/OscToMantle/ConvertScenarioRoute.h"
#include "Storyboard/GenericAction/AssignRouteAction.h"
#include "Storyboard/GenericAction/AssignRouteAction_impl.h"
#include "TestUtils.h"
#include "gmock/gmock.h"

using namespace mantle_api;
using testing::_;
using testing::NotNull;
using testing::Return;
using testing::SizeIs;
using namespace units::literals;
using testing::OpenScenarioEngine::v1_3::OpenScenarioEngineLibraryTestBase;
using testing::OpenScenarioEngine::v1_3::FakeRootNode;

class AssignRouteActionTestFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override { OpenScenarioEngineLibraryTestBase::SetUp(); }
};

MATCHER_P(IsRouteDefinitionSame, route_definition, "Waypoints or route strategy do not match")
{
    bool are_waypoints_equal = false;
    bool are_route_strategies_equal = false;

    for (size_t j = 0; j < route_definition.waypoints.

                           size();

         ++j)
    {
        are_waypoints_equal = (arg.waypoints.at(j).waypoint.x == route_definition.waypoints.at(j).waypoint.x &&
                               arg.waypoints.at(j).waypoint.y == route_definition.waypoints.at(j).waypoint.y &&
                               arg.waypoints.at(j).waypoint.z == route_definition.waypoints.at(j).waypoint.z);
        are_route_strategies_equal =
            (arg.waypoints.at(j).route_strategy == route_definition.waypoints.at(j).route_strategy);
        if (are_waypoints_equal == true && are_route_strategies_equal == true)
        {
            continue;
        }
        else
        {
            return false;
        }
    }
    return true;
}

TEST_F(AssignRouteActionTestFixture, GivenAssignRouteAction_WhenStartAction_ThenRouteAssignedAndNoExceptionInStep)
{
    using namespace testing::OpenScenarioEngine::v1_3;

    auto fake_assign_route_action = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::AssignRouteActionImpl>();

    auto route = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::RouteImpl>();

    auto way_point_one = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::WaypointImpl>();
    auto way_point_two = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::WaypointImpl>();

    auto position_one = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PositionImpl>();
    auto position_two = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PositionImpl>();

    auto world_position_one = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::WorldPositionImpl>();
    auto world_position_two = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::WorldPositionImpl>();

    world_position_one->SetX(1.0);
    world_position_one->SetY(0.0);
    world_position_one->SetZ(0.0);

    world_position_two->SetX(42.0);
    world_position_two->SetY(0.0);
    world_position_two->SetZ(10.0);

    position_one->SetWorldPosition(world_position_one);
    position_two->SetWorldPosition(world_position_two);

    way_point_one->SetPosition(position_one);
    way_point_one->SetRouteStrategy(NET_ASAM_OPENSCENARIO::v1_3::RouteStrategy::RouteStrategyEnum::FASTEST);
    way_point_two->SetPosition(position_two);
    way_point_two->SetRouteStrategy(NET_ASAM_OPENSCENARIO::v1_3::RouteStrategy::RouteStrategyEnum::SHORTEST);

    std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IWaypointWriter>> route_way_points = {way_point_one,
                                                                                                   way_point_two};
    route->SetWaypoints(route_way_points);
    fake_assign_route_action->SetRoute(route);

    auto assign_route_action = std::make_shared<OpenScenarioEngine::v1_3::Node::AssignRouteAction>(fake_assign_route_action);

    mantle_api::RouteDefinition expected_route_definition{};
    expected_route_definition.waypoints.push_back(mantle_api::RouteWaypoint{
        mantle_api::Vec3<units::length::meter_t>{1_m, 0_m, 0_m}, mantle_api::RouteStrategy::kFastest});
    expected_route_definition.waypoints.push_back(mantle_api::RouteWaypoint{
        mantle_api::Vec3<units::length::meter_t>{42_m, 0_m, 10_m}, mantle_api::RouteStrategy::kShortest});

    EXPECT_CALL(*env_, AssignRoute(testing::_, IsRouteDefinitionSame(expected_route_definition)))
        .WillOnce(testing::SaveArg<1>(&expected_route_definition));

    auto engine_abort_flags = std::make_shared<OpenScenarioEngine::v1_3::EngineAbortFlags>(OpenScenarioEngine::v1_3::EngineAbortFlags::kNoAbort);
    auto entity_broker = std::make_shared<OpenScenarioEngine::v1_3::EntityBroker>(false);
    entity_broker->add("Ego");
    auto root_node = std::make_shared<FakeRootNode>(env_, engine_abort_flags, entity_broker);
    root_node->setChild(assign_route_action);
    root_node->distributeData();
    EXPECT_NO_THROW(root_node->onInit());
    EXPECT_NO_THROW(root_node->executeTick());
}

TEST(AssignRouteAction, IsStepped_ReturnsTrue)
{
  OpenScenarioEngine::v1_3::AssignRouteAction assignRouteAction({std::vector<std::string>{"Vehicle1"},
                                                                 OpenScenarioEngine::v1_3::Route{}},
                                                                {std::make_shared<MockEnvironment>()});
  ASSERT_TRUE(assignRouteAction.Step());
}

TEST(DISABLED_AssignRouteAction, GivenClosedRoute_NotImplementedYet)
{
  OpenScenarioEngine::v1_3::Route route{};
  route.closed = true;
  OpenScenarioEngine::v1_3::AssignRouteAction assignRouteAction({std::vector<std::string>{"Vehicle1"},
                                                                 route},
                                                                {std::make_shared<MockEnvironment>()});
}

TEST(AssignRouteAction, IsStepped_PropagatesWaypoints)
{
  Vec3<units::length::meter_t> expected_position{1.2_m, 3.4_m, 5.6_m};

  OpenScenarioEngine::v1_3::Route route{};
  route.waypoints.push_back(RouteWaypoint{expected_position, RouteStrategy::kUndefined});

  mantle_api::RouteDefinition route_definition{};

  auto mockEnvironment = std::make_shared<MockEnvironment>();
  EXPECT_CALL(*mockEnvironment, AssignRoute(_, _))
      .WillOnce(testing::SaveArg<1>(&route_definition));

  OpenScenarioEngine::v1_3::AssignRouteAction assignRouteAction({{"Vehicle1"}, route},
                                                                {mockEnvironment});

  assignRouteAction.Step();

  ASSERT_THAT(route_definition.waypoints, SizeIs(1));
  EXPECT_THAT(route_definition.waypoints[0].waypoint, expected_position);
}
