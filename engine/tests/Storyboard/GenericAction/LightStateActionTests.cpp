/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "builders/ActionBuilder.h"
#include "Storyboard/GenericAction/LightStateAction.h"
#include "TestUtils.h"

using namespace OpenScenarioEngine::v1_3;
using namespace testing::OpenScenarioEngine::v1_3;
using testing::OpenScenarioEngine::v1_3::FakeRootNode;
using testing::OpenScenarioEngine::v1_3::OpenScenarioEngineLibraryTestBase;

class LightStateActionTestFixture : public OpenScenarioEngineLibraryTestBase
{
protected:
  void SetUp() override
  {
    OpenScenarioEngineLibraryTestBase::SetUp();
    auto engine_abort_flags = std::make_shared<EngineAbortFlags>(EngineAbortFlags::kNoAbort);
    auto entity_broker = std::make_shared<EntityBroker>(false);
    entity_broker->add("Ego");
    root_node_ = std::make_shared<FakeRootNode>(env_, engine_abort_flags, entity_broker);
  }

  std::shared_ptr<FakeRootNode> root_node_{nullptr};
};

TEST_F(LightStateActionTestFixture,
       GivenLightStateAction_WhenStartAction_ThenExpectCorrectStrategyAndNoException)
{
  std::string vehicle_light_type_string = "indicatorLeft";
  auto vehicle_light_type = NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType(vehicle_light_type_string);
  auto vehicle_light = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::VehicleLightImpl>();
  auto light_type = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::LightTypeImpl>();
  vehicle_light->SetVehicleLightType(vehicle_light_type);
  light_type->SetVehicleLight(vehicle_light);

  std::string mode_string = "flashing";
  auto mode = NET_ASAM_OPENSCENARIO::v1_3::LightMode(mode_string);
  auto light_state = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::LightStateImpl>();
  light_state->SetMode(mode);

  auto light_state_action_data =
      FakeLightStateActionBuilder{}
          .WithLightType(light_type)
          .WithLightState(light_state)
          .Build();
  auto light_state_action = std::make_shared<Node::LightStateAction>(light_state_action_data);

  mantle_api::LightType expected_light_type = mantle_api::VehicleLightType::kIndicatorLeft;
  mantle_api::LightState expected_light_state = {mantle_api::LightMode::kFlashing};

  EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_))
      .Times(1)
      .WillRepeatedly([expected_light_type, expected_light_state](
                          std::uint64_t controller_id,
                          std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies)
                      {
                std::ignore = controller_id;
                ASSERT_THAT(control_strategies, testing::SizeIs(1));
                ASSERT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kUpdateVehicleLightStates);
                auto control_strategy = dynamic_cast<mantle_api::VehicleLightStatesControlStrategy*>(control_strategies[0].get());
                EXPECT_EQ(control_strategy->light_type, expected_light_type);
                EXPECT_EQ(control_strategy->light_state, expected_light_state); });

  root_node_->setChild(light_state_action);
  root_node_->distributeData();
  EXPECT_NO_THROW(root_node_->onInit());
  EXPECT_NO_THROW(root_node_->executeTick());
}
