/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <gtest/gtest.h>

#include "Storyboard/GenericAction/VisibilityAction.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"

using namespace units::literals;
using namespace OpenScenarioEngine::v1_3;
using namespace testing::OpenScenarioEngine::v1_3;

class VisibilityActionTestFixture : public OpenScenarioEngineLibraryTestBase
{
};

MATCHER_P(VisConfigEqualTo, expected_vis_config, "")
{
  bool result = true;
  for (auto& sensor_name : arg.sensor_names)
  {
    if (std::find(expected_vis_config.sensor_names.begin(), expected_vis_config.sensor_names.end(), sensor_name) == expected_vis_config.sensor_names.end())
    {
      result = false;
    }
  }
  return arg.graphics == expected_vis_config.graphics && arg.traffic == expected_vis_config.traffic &&
         arg.sensors == expected_vis_config.sensors && result;
}

TEST_F(VisibilityActionTestFixture, GivenVisibilityActionWithOneActor_WhenStepAction_ThenSetVisibilityCallOnce)
{
  auto fake_visibility_action = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::VisibilityActionImpl>();
  auto visibility_action = std::make_shared<Node::VisibilityAction>(fake_visibility_action);

  bool expected_graphic_value = false;
  bool expected_sensors_value = true;
  bool expected_traffic_value = true;

  fake_visibility_action->SetTraffic(expected_traffic_value);
  fake_visibility_action->SetSensors(expected_sensors_value);
  fake_visibility_action->SetGraphics(expected_graphic_value);

  const mantle_api::EntityVisibilityConfig expected_vis_config{
      expected_graphic_value, expected_traffic_value, expected_sensors_value, {}};

  EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("")->get()),
              SetVisibility(VisConfigEqualTo(expected_vis_config)))
      .Times(1);

  auto engine_abort_flags = std::make_shared<EngineAbortFlags>(EngineAbortFlags::kNoAbort);
  auto entity_broker = std::make_shared<EntityBroker>(false);
  entity_broker->add("TrafficVehicle");
  auto root_node = std::make_shared<FakeRootNode>(env_, engine_abort_flags, entity_broker);
  root_node->setChild(visibility_action);
  root_node->distributeData();
  EXPECT_NO_THROW(root_node->onInit());
  EXPECT_NO_THROW(root_node->executeTick());
}

TEST_F(VisibilityActionTestFixture, GivenVisibilityActionWithTwoActor_WhenStepAction_ThenSetVisibilityCallTwice)
{
  auto fake_visibility_action = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::VisibilityActionImpl>();
  auto visibility_action = std::make_shared<Node::VisibilityAction>(fake_visibility_action);

  bool expected_default_value = false;

  const mantle_api::EntityVisibilityConfig expected_vis_config{
      expected_default_value, expected_default_value, expected_default_value, {}};

  EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("")->get()),
              SetVisibility(VisConfigEqualTo(expected_vis_config)))
      .Times(2);

  auto engine_abort_flags = std::make_shared<EngineAbortFlags>(EngineAbortFlags::kNoAbort);
  auto entity_broker = std::make_shared<EntityBroker>(false);
  entity_broker->add(std::vector<std::string>{"TrafficVehicle1", "TrafficVehicle2"});
  auto root_node = std::make_shared<FakeRootNode>(env_, engine_abort_flags, entity_broker);
  root_node->setChild(visibility_action);
  root_node->distributeData();
  EXPECT_NO_THROW(root_node->onInit());
  EXPECT_NO_THROW(root_node->executeTick());
}

TEST_F(VisibilityActionTestFixture, GivenVisibilityActionWithMultipleSensors_WhenStepAction_ThenSetVisibilityCalledWithMultipleSensors)
{
  auto fake_visibility_action = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::VisibilityActionImpl>();
  auto visibility_action = std::make_shared<Node::VisibilityAction>(fake_visibility_action);

  bool expected_default_value = false;
  const std::string expected_sensor_name_1 = "abc";
  const std::string expected_sensor_name_2 = "def";
  const std::string expected_sensor_name_3 = "ghi";

  fake_visibility_action->SetTraffic(expected_default_value);
  fake_visibility_action->SetSensors(expected_default_value);
  fake_visibility_action->SetGraphics(expected_default_value);

  auto sensor_reference_1 = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::SensorReferenceImpl>();
  sensor_reference_1->SetName(expected_sensor_name_1);
  auto sensor_reference_2 = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::SensorReferenceImpl>();
  sensor_reference_2->SetName(expected_sensor_name_2);
  auto sensor_reference_3 = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::SensorReferenceImpl>();
  sensor_reference_3->SetName(expected_sensor_name_3);
  std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ISensorReferenceWriter>> sensor_references;
  sensor_references.push_back(sensor_reference_1);
  sensor_references.push_back(sensor_reference_2);
  sensor_references.push_back(sensor_reference_3);
  auto sensor_reference_set = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::SensorReferenceSetImpl>();
  sensor_reference_set->SetSensorReferences(sensor_references);
  fake_visibility_action->SetSensorReferenceSet(sensor_reference_set);

  const mantle_api::EntityVisibilityConfig expected_vis_config{
      expected_default_value, expected_default_value, expected_default_value, {expected_sensor_name_1, expected_sensor_name_2, expected_sensor_name_3}};

  EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("")->get()),
              SetVisibility(VisConfigEqualTo(expected_vis_config)))
      .Times(1);

  auto engine_abort_flags = std::make_shared<EngineAbortFlags>(EngineAbortFlags::kNoAbort);
  auto entity_broker = std::make_shared<EntityBroker>(false);
  entity_broker->add("TrafficVehicle");
  auto root_node = std::make_shared<FakeRootNode>(env_, engine_abort_flags, entity_broker);
  root_node->setChild(visibility_action);
  root_node->distributeData();
  EXPECT_NO_THROW(root_node->onInit());
  EXPECT_NO_THROW(root_node->executeTick());
}
