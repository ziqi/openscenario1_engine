/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <MantleAPI/Traffic/i_controller.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MantleAPI/Common/i_identifiable.h"
#include "Storyboard/GenericAction/ActivateControllerAction_impl.h"
#include "TestUtils/MockControllerService.h"
#include "gmock/gmock.h"

using testing::_;
using testing::DoAll;
using testing::Eq;
using testing::Return;
using testing::SaveArg;

using ActivateControllerTestInputType = std::tuple<mantle_api::IController::LateralState, mantle_api::IController::LongitudinalState>;

// clang-format off
const auto kActivateControllerTestInputs = testing::Values(
  std::make_tuple( mantle_api::IController::LateralState::kNoChange,   mantle_api::IController::LongitudinalState::kNoChange  ),
  std::make_tuple( mantle_api::IController::LateralState::kNoChange,   mantle_api::IController::LongitudinalState::kDeactivate  ),
  std::make_tuple( mantle_api::IController::LateralState::kNoChange,   mantle_api::IController::LongitudinalState::kActivate    ),
  std::make_tuple( mantle_api::IController::LateralState::kDeactivate, mantle_api::IController::LongitudinalState::kNoChange  ),
  std::make_tuple( mantle_api::IController::LateralState::kDeactivate, mantle_api::IController::LongitudinalState::kDeactivate  ),
  std::make_tuple( mantle_api::IController::LateralState::kDeactivate, mantle_api::IController::LongitudinalState::kActivate    ),
  std::make_tuple( mantle_api::IController::LateralState::kActivate,   mantle_api::IController::LongitudinalState::kNoChange  ),
  std::make_tuple( mantle_api::IController::LateralState::kActivate,   mantle_api::IController::LongitudinalState::kDeactivate  ),
  std::make_tuple( mantle_api::IController::LateralState::kActivate,   mantle_api::IController::LongitudinalState::kActivate    )
);
// clang-format on

constexpr mantle_api::UniqueId TEST_ENTITY_ID{1234};
constexpr mantle_api::UniqueId TEST_CONTROLLER_ID{5678};
const std::optional<std::string> TEST_CONTROLLER_REF{"TEST_CONTROLLER"};
constexpr std::optional<bool> DONT_CARE{std::nullopt};

class ActivateControllerAction : public testing::TestWithParam<ActivateControllerTestInputType>
{
protected:
  /// The mantle mock returns a MockVehicle, regardless of the queried name!
  /// Get the entity, cast it and set the ID
  void SET_ENTITY_ID(mantle_api::UniqueId id)
  {
    auto& entity = mock_environment_->GetEntityRepository().Get("DOES_NOT_MATTER").value().get();
    auto& vehicle = dynamic_cast<mantle_api::MockVehicle&>(entity);
    ON_CALL(vehicle, GetUniqueId()).WillByDefault(Return(id));
  }

  std::shared_ptr<mantle_api::MockEnvironment> mock_environment_{
      std::make_shared<mantle_api::MockEnvironment>()};
  std::shared_ptr<testing::OpenScenarioEngine::v1_3::MockControllerService> mock_controller_service_{
      std::make_shared<testing::OpenScenarioEngine::v1_3::MockControllerService>()};
};

TEST_P(ActivateControllerAction,
       GivenLongitudinalAndLateralControllerFlags_WhenActionIsStepped_ThenLongitudinalAndLateralControllersAreSetOnlyIfNecessaryAndActionFinishes)
{
  auto [desired_lateral_state, desired_longitudinal_state] = GetParam();

  OpenScenarioEngine::v1_3::ActivateControllerAction activate_controller_action{
      {.entities = {"DONT_CARE"},
       .animation = DONT_CARE,
       .controllerRef = TEST_CONTROLLER_REF,
       .lateral_state = desired_lateral_state,
       .lighting = DONT_CARE,
       .longitudinal_state = desired_longitudinal_state},
      {.environment = mock_environment_},
      {.controllerService = mock_controller_service_}};

  mantle_api::MockController mockController;
  std::optional<OpenScenarioEngine::v1_3::EntityControllers> entity_controllers = OpenScenarioEngine::v1_3::EntityControllers{};
  entity_controllers->user_defined.emplace(TEST_CONTROLLER_ID, &mockController);

  SET_ENTITY_ID(TEST_ENTITY_ID);
  ON_CALL(*mock_controller_service_, GetControllers(TEST_ENTITY_ID))
      .WillByDefault(Return(entity_controllers));

  ON_CALL(*mock_controller_service_, GetControllerId(TEST_CONTROLLER_REF, _))
      .WillByDefault(Return(TEST_CONTROLLER_ID));

  mantle_api::IController::LateralState new_lateral_state{};
  mantle_api::IController::LongitudinalState new_longitudinal_state{};

  EXPECT_CALL(*mock_controller_service_, ChangeState(TEST_ENTITY_ID, TEST_CONTROLLER_ID, _, _))
      .WillOnce(DoAll(
          SaveArg<2>(&new_lateral_state),
          SaveArg<3>(&new_longitudinal_state)));

  ASSERT_TRUE(activate_controller_action.Step());
  ASSERT_THAT(new_lateral_state, Eq(desired_lateral_state));
  ASSERT_THAT(new_longitudinal_state, Eq(desired_longitudinal_state));
}

INSTANTIATE_TEST_SUITE_P(ActivateControllerActionCombinations,
                         ActivateControllerAction,
                         kActivateControllerTestInputs);
