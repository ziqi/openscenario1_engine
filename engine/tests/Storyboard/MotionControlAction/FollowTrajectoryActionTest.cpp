/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioTimeReference.h"
#include "Conversion/OscToMantle/ConvertScenarioTrajectoryFollowingMode.h"
#include "Conversion/OscToMantle/ConvertScenarioTrajectoryRef.h"
#include "Storyboard/MotionControlAction/FollowTrajectoryAction.h"
#include "Storyboard/MotionControlAction/FollowTrajectoryAction_impl.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"
#include "builders/PositionBuilder.h"
#include "gmock/gmock.h"

using namespace mantle_api;
using testing::_;
using testing::Return;
using testing::SaveArg;
using namespace units::literals;
using namespace OpenScenarioEngine::v1_3;
using namespace testing::OpenScenarioEngine::v1_3;

class FollowTrajectoryActionTestFixture : public OpenScenarioEngineLibraryTestBase
{
protected:
  void SetUp() override
  {
    OpenScenarioEngineLibraryTestBase::SetUp();
    auto engine_abort_flags = std::make_shared<EngineAbortFlags>(EngineAbortFlags::kNoAbort);
    auto entity_broker = std::make_shared<EntityBroker>(false);
    entity_broker->add("Ego");
    root_node_ = std::make_shared<FakeRootNode>(env_, engine_abort_flags, entity_broker);
  }

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPolylineWriter> GetPolylineWithRelativeLanePositions()
  {
    int relative_lane{1};
    double offset1{1.0};
    double offset2{1.0};

    testing::OpenScenarioEngine::v1_3::FakeRelativeLanePositionBuilder fake_relative_lane_position_builder1{relative_lane,
                                                                                                            offset1};
    testing::OpenScenarioEngine::v1_3::FakeRelativeLanePositionBuilder fake_relative_lane_position_builder2{relative_lane,
                                                                                                            offset2};

    fake_relative_lane_position_builder1.WithDs(1.0);
    fake_relative_lane_position_builder1.WithDsLane(2.0);

    fake_relative_lane_position_builder2.WithDs(1.0);
    fake_relative_lane_position_builder2.WithDsLane(2.0);
    return FakePolylineBuilder()
        .WithVertices({FakeVertexBuilder(0)
                           .WithPosition(FakePositionBuilder{}
                                             .WithRelativeLanePosition(fake_relative_lane_position_builder1.Build())
                                             .Build())
                           .Build(),
                       FakeVertexBuilder(1)
                           .WithPosition(FakePositionBuilder{}
                                             .WithRelativeLanePosition(fake_relative_lane_position_builder2.Build())
                                             .Build())
                           .Build()})
        .Build();
  }
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPolylineWriter> GetPolylineWithGeoPositions()
  {
    mantle_api::LatLonPosition lat_lon_position1{42.123_deg, 11.65874_deg};
    mantle_api::LatLonPosition lat_lon_position2{52.123_deg, 11.65874_deg};

    return FakePolylineBuilder()
        .WithVertices({FakeVertexBuilder(0)
                           .WithPosition(FakePositionBuilder{}
                                             .WithGeoPosition(FakeGeoPositionBuilder(lat_lon_position1.latitude(),
                                                                                     lat_lon_position1.longitude())
                                                                  .Build())
                                             .Build())
                           .Build(),
                       FakeVertexBuilder(1)
                           .WithPosition(FakePositionBuilder{}
                                             .WithGeoPosition(FakeGeoPositionBuilder(lat_lon_position2.latitude(),
                                                                                     lat_lon_position2.longitude())
                                                                  .Build())
                                             .Build())
                           .Build()})
        .Build();
  }
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPolylineWriter> GetPolylineWithLanePositions()
  {
    mantle_api::OpenDriveLanePosition open_drive_position1{"1", 2, 3.0_m, 0.0_m};
    mantle_api::OpenDriveLanePosition open_drive_position2{"1", 2, 4.0_m, 0.0_m};
    return FakePolylineBuilder()
        .WithVertices({FakeVertexBuilder(0)
                           .WithPosition(FakePositionBuilder{}
                                             .WithLanePosition(
                                                 FakeLanePositionBuilder(open_drive_position1.road,
                                                                         std::to_string(open_drive_position1.lane),
                                                                         open_drive_position1.t_offset(),
                                                                         open_drive_position1.s_offset())
                                                     .Build())
                                             .Build())
                           .Build(),
                       FakeVertexBuilder(1)
                           .WithPosition(FakePositionBuilder{}
                                             .WithLanePosition(
                                                 FakeLanePositionBuilder(open_drive_position2.road,
                                                                         std::to_string(open_drive_position2.lane),
                                                                         open_drive_position2.t_offset(),
                                                                         open_drive_position2.s_offset())
                                                     .Build())
                                             .Build())
                           .Build()})
        .Build();
  }
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPolylineWriter> GetPolylineWithWorldPositions()
  {
    mantle_api::Vec3<units::length::meter_t> position1{1.0_m, 2.0_m, 0.0_m};
    mantle_api::Orientation3<units::angle::radian_t> orientation1{0.1_rad, 0.0_rad, 0.0_rad};
    mantle_api::Vec3<units::length::meter_t> position2{1.1_m, 2.1_m, 0.0_m};

    return FakePolylineBuilder()
        .WithVertices({FakeVertexBuilder(0)
                           .WithPosition(FakePositionBuilder{}
                                             .WithWorldPosition(FakeWorldPositionBuilder(position1.x.value(),
                                                                                         position1.y.value(),
                                                                                         position1.z.value(),
                                                                                         orientation1.yaw.value(),
                                                                                         orientation1.pitch.value(),
                                                                                         orientation1.roll.value())
                                                                    .Build())
                                             .Build())
                           .Build(),
                       FakeVertexBuilder(1)
                           .WithPosition(FakePositionBuilder{}
                                             .WithWorldPosition(FakeWorldPositionBuilder(position2.x.value(),
                                                                                         position2.y.value(),
                                                                                         position2.z.value(),
                                                                                         orientation1.yaw.value(),
                                                                                         orientation1.pitch.value(),
                                                                                         orientation1.roll.value())
                                                                    .Build())
                                             .Build())
                           .Build()})
        .Build();
  }

  std::shared_ptr<FakeRootNode> root_node_{nullptr};
};

TEST_F(FollowTrajectoryActionTestFixture,
       GivenFollowTrajectoryActionWithWorldPosition_WhenStartAction_ThenTransformedIntoCorrespondingControlStrategy)
{
  auto fake_follow_trajectory_action =
      FakeFollowTrajectoryActionBuilder{}
          .WithTimeReference(FakeTimeReferenceBuilder().Build())
          .WithTrajectory(FakeTrajectoryBuilder("TestTrajectory", false)
                              .WithShape(FakeShapeBuilder().WithPolyline(GetPolylineWithWorldPositions()).Build())
                              .Build())
          .Build();

  auto follow_trajectory_action = std::make_shared<Node::FollowTrajectoryAction>(fake_follow_trajectory_action);

  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};

  mantle_api::Vec3<units::length::meter_t> expected_position1{1_m, 2_m, 3_m};
  mantle_api::Vec3<units::length::meter_t> expected_position2{2_m, 3_m, 4_m};
  mantle_api::Orientation3<units::angle::radian_t> expected_orientation1{0.1_rad, 0.0_rad, 0.0_rad};

  mantle_api::Pose expected_pose1{expected_position1, expected_orientation1};
  mantle_api::Pose expected_pose2{expected_position2, expected_orientation1};

  EXPECT_CALL(dynamic_cast<const mantle_api::MockGeometryHelper&>(*(env_->GetGeometryHelper())),
              TranslateGlobalPositionLocally(testing::_, testing::_, testing::_))
      .WillOnce(testing::Return(expected_position1))
      .WillOnce(testing::Return(expected_position2));

  EXPECT_CALL(*env_, UpdateControlStrategies(testing::_, testing::_))
      .WillOnce(testing::SaveArg<1>(&control_strategies));

  root_node_->setChild(follow_trajectory_action);
  root_node_->distributeData();
  root_node_->onInit();
  EXPECT_NO_THROW(root_node_->executeTick());

  ASSERT_THAT(control_strategies, testing::SizeIs(1));
  EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowTrajectory);
  auto& trajectory =
      std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy>(control_strategies.front())->trajectory;
  auto& polyline = std::get<mantle_api::PolyLine>(trajectory.type);
  EXPECT_THAT(polyline, testing::SizeIs(2));
  EXPECT_EQ(polyline.at(0).time, 0.0_s);
  EXPECT_EQ(polyline.at(0).pose, expected_pose1);
  EXPECT_EQ(polyline.at(1).time, 1.0_s);
  EXPECT_EQ(polyline.at(1).pose, expected_pose2);
}

TEST_F(FollowTrajectoryActionTestFixture,
       GivenFollowTrajectoryActionWithLanePosition_WhenStartAction_ThenTransformedIntoCorrespondingControlStrategy)
{
  using namespace OpenScenarioEngine::v1_3;
  auto fake_follow_trajectory_action =
      FakeFollowTrajectoryActionBuilder{}
          .WithTimeReference(FakeTimeReferenceBuilder().Build())
          .WithTrajectory(FakeTrajectoryBuilder("TestTrajectory", false)
                              .WithShape(FakeShapeBuilder().WithPolyline(GetPolylineWithLanePositions()).Build())
                              .Build())
          .Build();

  auto follow_trajectory_action = std::make_shared<Node::FollowTrajectoryAction>(fake_follow_trajectory_action);

  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};

  mantle_api::Vec3<units::length::meter_t> expected_position1{1_m, 2_m, 3_m};
  mantle_api::Vec3<units::length::meter_t> expected_position2{2_m, 3_m, 4_m};
  mantle_api::Orientation3<units::angle::radian_t> orientation{0.0_rad, 0.0_rad, 0.0_rad};

  mantle_api::Pose expected_pose1{expected_position1, orientation};
  mantle_api::Pose expected_pose2{expected_position2, orientation};

  EXPECT_CALL(dynamic_cast<const mantle_api::MockGeometryHelper&>(*(env_->GetGeometryHelper())),
              TranslateGlobalPositionLocally(testing::_, testing::_, testing::_))
      .WillOnce(testing::Return(expected_position1))
      .WillOnce(testing::Return(expected_position2));

  EXPECT_CALL(*env_, UpdateControlStrategies(testing::_, testing::_))
      .WillOnce(testing::SaveArg<1>(&control_strategies));

  EXPECT_CALL(dynamic_cast<mantle_api::MockConverter&>(*(env_->GetConverter())), Convert(testing::_))
      .WillOnce(testing::Return(expected_position1))
      .WillOnce(testing::Return(expected_position2));

  root_node_->setChild(follow_trajectory_action);
  root_node_->distributeData();
  root_node_->onInit();
  EXPECT_NO_THROW(root_node_->executeTick());

  ASSERT_THAT(control_strategies, testing::SizeIs(1));
  EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowTrajectory);
  auto& trajectory =
      std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy>(control_strategies.front())->trajectory;
  auto& polyline = std::get<mantle_api::PolyLine>(trajectory.type);
  EXPECT_THAT(polyline, testing::SizeIs(2));
  EXPECT_EQ(polyline.at(0).time, 0.0_s);
  EXPECT_EQ(polyline.at(0).pose, expected_pose1);
  EXPECT_EQ(polyline.at(1).time, 1.0_s);
  EXPECT_EQ(polyline.at(1).pose, expected_pose2);
}

TEST_F(FollowTrajectoryActionTestFixture,
       GivenFollowTrajectoryActionWithGeoPosition_WhenStartAction_ThenTransformedIntoCorrespondingControlStrategy)
{
  auto fake_follow_trajectory_action =
      FakeFollowTrajectoryActionBuilder{}
          .WithTimeReference(FakeTimeReferenceBuilder().Build())
          .WithTrajectory(FakeTrajectoryBuilder("TestTrajectory", false)
                              .WithShape(FakeShapeBuilder().WithPolyline(GetPolylineWithGeoPositions()).Build())
                              .Build())
          .Build();

  auto follow_trajectory_action = std::make_shared<Node::FollowTrajectoryAction>(fake_follow_trajectory_action);

  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};

  mantle_api::Vec3<units::length::meter_t> expected_position1{1_m, 2_m, 3_m};
  mantle_api::Vec3<units::length::meter_t> expected_position2{2_m, 3_m, 4_m};
  mantle_api::Orientation3<units::angle::radian_t> orientation{0.0_rad, 0.0_rad, 0.0_rad};

  mantle_api::Pose expected_pose1{expected_position1, orientation};
  mantle_api::Pose expected_pose2{expected_position2, orientation};

  EXPECT_CALL(*env_, UpdateControlStrategies(testing::_, testing::_))
      .WillOnce(testing::SaveArg<1>(&control_strategies));

  EXPECT_CALL(dynamic_cast<const mantle_api::MockGeometryHelper&>(*(env_->GetGeometryHelper())),
              TranslateGlobalPositionLocally(testing::_, testing::_, testing::_))
      .WillOnce(testing::Return(expected_position1))
      .WillOnce(testing::Return(expected_position2));

  EXPECT_CALL(dynamic_cast<mantle_api::MockConverter&>(*(env_->GetConverter())), Convert(testing::_))
      .WillOnce(testing::Return(expected_position1))
      .WillOnce(testing::Return(expected_position2));

  root_node_->setChild(follow_trajectory_action);
  root_node_->distributeData();
  root_node_->onInit();
  EXPECT_NO_THROW(root_node_->executeTick());

  ASSERT_THAT(control_strategies, testing::SizeIs(1));
  EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowTrajectory);
  auto& trajectory =
      std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy>(control_strategies.front())->trajectory;
  auto& polyline = std::get<mantle_api::PolyLine>(trajectory.type);
  EXPECT_THAT(polyline, testing::SizeIs(2));
  EXPECT_EQ(polyline.at(0).time, 0.0_s);
  EXPECT_EQ(polyline.at(0).pose, expected_pose1);
  EXPECT_EQ(polyline.at(1).time, 1.0_s);
  EXPECT_EQ(polyline.at(1).pose, expected_pose2);
}

TEST_F(FollowTrajectoryActionTestFixture,
       GivenFollowTrajectoryActionWithRelativeLane_WhenStartAction_ThenTransformedIntoCorrespondingControlStrategy)
{
  auto fake_follow_trajectory_action =
      FakeFollowTrajectoryActionBuilder{}
          .WithTimeReference(FakeTimeReferenceBuilder().Build())
          .WithTrajectory(
              FakeTrajectoryBuilder("TestTrajectory", false)
                  .WithShape(FakeShapeBuilder().WithPolyline(GetPolylineWithRelativeLanePositions()).Build())
                  .Build())
          .Build();

  auto follow_trajectory_action = std::make_shared<Node::FollowTrajectoryAction>(fake_follow_trajectory_action);

  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};

  mantle_api::Vec3<units::length::meter_t> expected_position1{1_m, 2_m, 3_m};
  mantle_api::Vec3<units::length::meter_t> expected_position2{2_m, 3_m, 4_m};
  mantle_api::Orientation3<units::angle::radian_t> orientation{0.0_rad, 0.0_rad, 0.0_rad};

  mantle_api::Pose expected_pose1{expected_position1, orientation};
  mantle_api::Pose expected_pose2{expected_position2, orientation};

  EXPECT_CALL(*env_, UpdateControlStrategies(testing::_, testing::_))
      .WillOnce(testing::SaveArg<1>(&control_strategies));

  EXPECT_CALL(dynamic_cast<const mantle_api::MockGeometryHelper&>(*(env_->GetGeometryHelper())),
              TranslateGlobalPositionLocally(testing::_, testing::_, testing::_))
      .WillOnce(testing::Return(expected_position1))
      .WillOnce(testing::Return(expected_position2));

  root_node_->setChild(follow_trajectory_action);
  root_node_->distributeData();
  root_node_->onInit();
  EXPECT_NO_THROW(root_node_->executeTick());

  ASSERT_THAT(control_strategies, testing::SizeIs(1));
  EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowTrajectory);
  auto& trajectory =
      std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy>(control_strategies.front())->trajectory;
  auto& polyline = std::get<mantle_api::PolyLine>(trajectory.type);
  EXPECT_THAT(polyline, testing::SizeIs(2));
  EXPECT_EQ(polyline.at(0).time, 0.0_s);
  EXPECT_EQ(polyline.at(0).pose, expected_pose1);
  EXPECT_EQ(polyline.at(1).time, 1.0_s);
  EXPECT_EQ(polyline.at(1).pose, expected_pose2);
}

TEST_F(FollowTrajectoryActionTestFixture,
       GivenFollowTrajectoryActionWithRelativeLane_WhenStepAction_ThenOnlyCompleteAfterControlStrategyGoalReached)
{
  auto fake_follow_trajectory_action =
      FakeFollowTrajectoryActionBuilder{}
          .WithTimeReference(FakeTimeReferenceBuilder().Build())
          .WithTrajectory(
              FakeTrajectoryBuilder("TestTrajectory", false)
                  .WithShape(FakeShapeBuilder().WithPolyline(GetPolylineWithRelativeLanePositions()).Build())
                  .Build())
          .Build();

  auto follow_trajectory_action = std::make_shared<Node::FollowTrajectoryAction>(fake_follow_trajectory_action);

  EXPECT_CALL(*env_, HasControlStrategyGoalBeenReached(0, mantle_api::ControlStrategyType::kFollowTrajectory))
      .Times(2)
      .WillOnce(::testing::Return(false))
      .WillRepeatedly(::testing::Return(true));
  EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_)).Times(3);

  root_node_->setChild(follow_trajectory_action);
  root_node_->distributeData();
  root_node_->onInit();
  EXPECT_NO_THROW(root_node_->executeTick());
  EXPECT_EQ(follow_trajectory_action->status(), yase::NodeStatus::kRunning);
  root_node_->executeTick();
  EXPECT_EQ(follow_trajectory_action->status(), yase::NodeStatus::kSuccess);
  root_node_->executeTick();
}

TEST(FollowTrajectoryAction, GivenMatchingControlStrategyGoalIsReached_ReturnsTrue)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  ON_CALL(*mockEnvironment,
          HasControlStrategyGoalBeenReached(_, ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(true));

  OpenScenarioEngine::v1_3::FollowTrajectoryAction followTrajectoryAction({std::vector<std::string>{"Vehicle1"},
                                                                           0.0,
                                                                           mantle_api::Trajectory{},
                                                                           OpenScenarioEngine::v1_3::TimeReference{},
                                                                           OpenScenarioEngine::v1_3::TrajectoryFollowingMode{},
                                                                           OpenScenarioEngine::v1_3::TrajectoryRef{}},
                                                                          {mockEnvironment});

  ASSERT_TRUE(followTrajectoryAction.HasControlStrategyGoalBeenReached("Vehicle1"));
}

TEST(FollowTrajectoryAction, GivenUnmatchingControlStrategyGoalIsReached_ReturnsFalse)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  ON_CALL(*mockEnvironment,
          HasControlStrategyGoalBeenReached(_, ControlStrategyType::kUndefined))
      .WillByDefault(Return(false));

  OpenScenarioEngine::v1_3::FollowTrajectoryAction followTrajectoryAction({std::vector<std::string>{"Vehicle1"},
                                                                           0.0,
                                                                           mantle_api::Trajectory{},
                                                                           OpenScenarioEngine::v1_3::TimeReference{},
                                                                           OpenScenarioEngine::v1_3::TrajectoryFollowingMode{},
                                                                           OpenScenarioEngine::v1_3::TrajectoryRef{}},
                                                                          {mockEnvironment});

  ASSERT_FALSE(followTrajectoryAction.HasControlStrategyGoalBeenReached("Vehicle1"));
}

TEST(FollowTrajectoryAction, GivenFollowTrajectoryAction_WhenNoTrajectoryRef_ThenThrowError)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();

  OpenScenarioEngine::v1_3::FollowTrajectoryAction followTrajectoryAction({std::vector<std::string>{"Vehicle1"},
                                                                           0.0,
                                                                           mantle_api::Trajectory{},
                                                                           OpenScenarioEngine::v1_3::TimeReference{},
                                                                           OpenScenarioEngine::v1_3::TrajectoryFollowingMode{},
                                                                           OpenScenarioEngine::v1_3::TrajectoryRef{}},
                                                                          {mockEnvironment});

  EXPECT_THROW(followTrajectoryAction.SetControlStrategy(), std::runtime_error);
}

TEST(FollowTrajectoryAction, GivenFollowTrajectoryActionAndTrajectoryRef_UpdatesControlStrategies)
{
  Pose pose1{{1.0_m, 2.0_m, 0.0_m}, {0.1_rad, 0.0_rad, 0.0_rad}};
  Pose pose2{{1.1_m, 2.1_m, 0.0_m}, {0.2_rad, 0.0_rad, 0.0_rad}};
  Pose pose3{{1.2_m, 2.2_m, 0.0_m}, {0.3_rad, 0.0_rad, 0.0_rad}};

  PolyLine polyLine{
      {pose1, std::nullopt},
      {pose2, std::nullopt},
      {pose3, std::nullopt}};

  mantle_api::Trajectory setTrajectory{};
  setTrajectory.type = polyLine;

  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto mockGeometryHelper = static_cast<const MockGeometryHelper*>(mockEnvironment->GetGeometryHelper());

  ON_CALL(*mockGeometryHelper, TranslateGlobalPositionLocally(pose1.position, _, _)).WillByDefault(Return(pose1.position));
  ON_CALL(*mockGeometryHelper, TranslateGlobalPositionLocally(pose2.position, _, _)).WillByDefault(Return(pose2.position));
  ON_CALL(*mockGeometryHelper, TranslateGlobalPositionLocally(pose3.position, _, _)).WillByDefault(Return(pose3.position));

  std::vector<std::shared_ptr<ControlStrategy>> control_strategies{};
  EXPECT_CALL(*mockEnvironment, UpdateControlStrategies(_, _)).WillOnce(SaveArg<1>(&control_strategies));

  OpenScenarioEngine::v1_3::FollowTrajectoryAction followTrajectoryAction({std::vector<std::string>{"Vehicle1"},
                                                                           0.0,
                                                                           mantle_api::Trajectory{},
                                                                           OpenScenarioEngine::v1_3::TimeReference{},
                                                                           OpenScenarioEngine::v1_3::TrajectoryFollowingMode{},
                                                                           setTrajectory},
                                                                          {mockEnvironment});

  EXPECT_NO_THROW(followTrajectoryAction.SetControlStrategy());

  EXPECT_THAT(control_strategies, testing::SizeIs(1));
  EXPECT_EQ(control_strategies.front()->type, ControlStrategyType::kFollowTrajectory);
  auto& trajectory = std::dynamic_pointer_cast<FollowTrajectoryControlStrategy>(control_strategies.front())->trajectory;
  auto& polyline = std::get<PolyLine>(trajectory.type);
  EXPECT_THAT(polyline, testing::SizeIs(3));
  EXPECT_EQ(polyline.at(0).pose, pose1);
  EXPECT_EQ(polyline.at(1).pose, pose2);
  EXPECT_EQ(polyline.at(2).pose, pose3);
}

TEST(FollowTrajectoryAction,
     GivenFollowTrajectoryActionWithTimeReference_WhenStartAction_ThenControlStrategyHasMovementDomainBoth)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  std::vector<std::shared_ptr<ControlStrategy>> control_strategies{};
  EXPECT_CALL(*mockEnvironment, UpdateControlStrategies(_, _)).WillOnce(SaveArg<1>(&control_strategies));

  OpenScenarioEngine::v1_3::FollowTrajectoryAction followTrajectoryAction({std::vector<std::string>{"Vehicle1"},
                                                                           0.0,
                                                                           mantle_api::Trajectory{},
                                                                           {mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference{}},
                                                                           OpenScenarioEngine::v1_3::TrajectoryFollowingMode{},
                                                                           mantle_api::Trajectory{}},
                                                                          {mockEnvironment});

  followTrajectoryAction.SetControlStrategy();
  ASSERT_THAT(control_strategies, testing::SizeIs(1));
  auto control_strategy = std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy>(control_strategies.front());

  EXPECT_TRUE(control_strategy->timeReference.has_value());
  EXPECT_THAT(control_strategy->movement_domain, mantle_api::MovementDomain::kBoth);
}

TEST(FollowTrajectoryAction,
     GivenFollowTrajectoryActionWithoutTimeReference_WhenStartAction_ThenControlStrategyHasMovementDomainLateral)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  std::vector<std::shared_ptr<ControlStrategy>> control_strategies{};
  EXPECT_CALL(*mockEnvironment, UpdateControlStrategies(_, _)).WillOnce(SaveArg<1>(&control_strategies));

  OpenScenarioEngine::v1_3::FollowTrajectoryAction followTrajectoryAction({std::vector<std::string>{"Vehicle1"},
                                                                           0.0,
                                                                           mantle_api::Trajectory{},
                                                                           std::nullopt,
                                                                           OpenScenarioEngine::v1_3::TrajectoryFollowingMode{},
                                                                           mantle_api::Trajectory{}},
                                                                          {mockEnvironment});

  followTrajectoryAction.SetControlStrategy();
  ASSERT_THAT(control_strategies, testing::SizeIs(1));
  auto control_strategy = std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy>(control_strategies.front());

  EXPECT_FALSE(control_strategy->timeReference.has_value());
  EXPECT_THAT(control_strategy->movement_domain, mantle_api::MovementDomain::kLateral);
}
