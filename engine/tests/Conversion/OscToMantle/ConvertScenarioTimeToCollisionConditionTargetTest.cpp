/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

#include "Conversion/OscToMantle/ConvertScenarioTimeToCollisionConditionTarget.h"

namespace testing::OpenScenarioEngine::v1_3
{
using namespace units::literals;
class ConvertScenarioTimeToCollisionConditionTargetTest : public ::testing::Test
{
protected:
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPosition> GetWorldPosition()
  {
    auto world_pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::WorldPositionImpl>();
    world_pos->SetX(1);
    world_pos->SetY(2);
    world_pos->SetZ(3);
    world_pos->SetH(4);
    world_pos->SetP(5);
    world_pos->SetR(6);
    auto pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PositionImpl>();
    pos->SetWorldPosition(world_pos);
    return pos;
  }

  mantle_api::Vec3<units::length::meter_t> expected_position_{1_m, 2_m, 3_m};
};

TEST_F(ConvertScenarioTimeToCollisionConditionTargetTest, GetThePosition_ThenConvertIntoMantleAPIPose)
{
  auto absolute_target_lane_ = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::TimeToCollisionConditionTargetImpl>();
  //  TODO: Implement once TTC issue is resolved.
}
}  // namespace testing::OpenScenarioEngine::v1_3