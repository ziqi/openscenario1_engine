/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioRule.h"

TEST(RuleTest, GivenUnkownRule_WhenInstantiate_ThenThrow)
{
  ASSERT_ANY_THROW(OpenScenarioEngine::v1_3::Rule(NET_ASAM_OPENSCENARIO::v1_3::Rule::UNKNOWN, 1));
}

TEST(RuleTest, GivenRuleWithGreaterThanOne_WhenEvaluateWithZero_ThenIsSatisfiedReturnsFalse)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::GREATER_THAN, 1};

  ASSERT_FALSE(rule.IsSatisfied(0));
}

TEST(RuleTest, GivenRuleWithGreaterThanOne_WhenEvaluateWithOne_ThenIsSatisfiedReturnsFalse)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::GREATER_THAN, 1};

  ASSERT_FALSE(rule.IsSatisfied(1));
}

TEST(RuleTest, GivenRuleWithGreaterThanOne_WhenEvaluateWithTwo_ThenIsSatisfiedReturnsTrue)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::GREATER_THAN, 1};

  ASSERT_TRUE(rule.IsSatisfied(2));
}

TEST(RuleTest, GivenRuleWithGreaterOrEqualThanOne_WhenEvaluateWithZero_ThenIsSatisfiedReturnsFalse)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::GREATER_OR_EQUAL, 1};

  ASSERT_FALSE(rule.IsSatisfied(0));
}

TEST(RuleTest, GivenRuleWithGreaterOrEqualThanOne_WhenEvaluateWithOne_ThenIsSatisfiedReturnsTrue)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::GREATER_OR_EQUAL, 1};

  ASSERT_TRUE(rule.IsSatisfied(1));
}

TEST(RuleTest, GivenRuleWithGreaterOrEqualThanOne_WhenEvaluateWithTwo_ThenIsSatisfiedReturnsTrue)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::GREATER_OR_EQUAL, 1};

  ASSERT_TRUE(rule.IsSatisfied(2));
}

TEST(RuleTest, GivenRuleWithLessThanOne_WhenEvaluateWithZero_ThenIsSatisfiedReturnsTrue)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::LESS_THAN, 1};

  ASSERT_TRUE(rule.IsSatisfied(0));
}

TEST(RuleTest, GivenRuleWithLessThanOne_WhenEvaluateWithOne_ThenIsSatisfiedReturnsFalse)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::LESS_THAN, 1};

  ASSERT_FALSE(rule.IsSatisfied(1));
}

TEST(RuleTest, GivenRuleWithLessThanOne_WhenEvaluateWithTwo_ThenIsSatisfiedReturnsFalse)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::LESS_THAN, 1};

  ASSERT_FALSE(rule.IsSatisfied(2));
}

TEST(RuleTest, GivenRuleWithLessOrEqualOne_WhenEvaluateWithZero_ThenIsSatisfiedReturnsTrue)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::LESS_OR_EQUAL, 1};

  ASSERT_TRUE(rule.IsSatisfied(0));
}

TEST(RuleTest, GivenRuleWithLessOrEqualOne_WhenEvaluateWithOne_ThenIsSatisfiedReturnsTrue)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::LESS_OR_EQUAL, 1};

  ASSERT_TRUE(rule.IsSatisfied(1));
}

TEST(RuleTest, GivenRuleWithLessOrEqualOne_WhenEvaluateWithTwo_ThenIsSatisfiedReturnsFalse)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::LESS_OR_EQUAL, 1};

  ASSERT_FALSE(rule.IsSatisfied(2));
}

TEST(RuleTest, GivenRuleWithEqualToOne_WhenEvaluateWithZero_ThenIsSatisfiedReturnsFalse)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::EQUAL_TO, 1};

  ASSERT_FALSE(rule.IsSatisfied(0));
}

TEST(RuleTest, GivenRuleWithEqualToOne_WhenEvaluateWithOne_ThenIsSatisfiedReturnsTrue)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::EQUAL_TO, 1};

  ASSERT_TRUE(rule.IsSatisfied(1));
}

TEST(RuleTest, GivenRuleWithEqualToOne_WhenEvaluateWithTwo_ThenIsSatisfiedReturnsFalse)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::EQUAL_TO, 1};

  ASSERT_FALSE(rule.IsSatisfied(2));
}

TEST(RuleTest, GivenRuleWithNotEqualToOne_WhenEvaluateWithZero_ThenIsSatisfiedReturnsTrue)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::NOT_EQUAL_TO, 1};

  ASSERT_TRUE(rule.IsSatisfied(0));
}

TEST(RuleTest, GivenRuleWithNotEqualToOne_WhenEvaluateWithOne_ThenIsSatisfiedReturnsFalse)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::NOT_EQUAL_TO, 1};

  ASSERT_FALSE(rule.IsSatisfied(1));
}

TEST(RuleTest, GivenRuleWithNotEqualToOne_WhenEvaluateWithTwo_ThenIsSatisfiedReturnsTrue)
{
  OpenScenarioEngine::v1_3::Rule rule{NET_ASAM_OPENSCENARIO::v1_3::Rule::NOT_EQUAL_TO, 1};

  ASSERT_TRUE(rule.IsSatisfied(2));
}
