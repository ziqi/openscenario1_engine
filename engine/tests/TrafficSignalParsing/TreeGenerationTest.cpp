/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <ApiClassWriterInterfacesV1_3.h>
#include <MantleAPI/Common/position.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Test/test_utils.h>
#include <agnostic_behavior_tree/composite/parallel_node.h>
#include <agnostic_behavior_tree/decorator/stop_at_node.h>
#include <agnostic_behavior_tree/utils/tree_print.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

#include <initializer_list>
#include <memory>

#include "Conversion/OscToNode/ParseTrafficSignals.h"
#include "Node/RepeatingSequenceNode.h"
#include "Node/TrafficSignalPhaseNode.h"

using testing::NotNull;

using namespace NET_ASAM_OPENSCENARIO::v1_3;
using namespace units::literals;

struct ThreeTestSignals
{
  void CREATE_PHASES(size_t controller_nr,
                     std::string name,
                     std::initializer_list<double> durations,
                     double delay = 0,
                     std::string reference = "")
  {
    auto impl = std::static_pointer_cast<TrafficSignalControllerImpl>(traffic_signals_.at(controller_nr));

    static size_t phase_nr{0};
    impl->SetName(name);
    if (delay > 0)
    {
      impl->SetDelay(delay);
      impl->SetReference(reference);
    }

    std::vector<std::shared_ptr<IPhaseWriter>> phases;
    for (auto& duration : durations)
    {
      auto phase = std::make_shared<PhaseImpl>();
      phase->SetDuration(duration);
      phase->SetName(name + "_" + std::to_string(phase_nr++));
      auto traffic_signal_state = traffic_signal_states_.emplace_back(std::make_shared<TrafficSignalStateImpl>());
      traffic_signal_state->SetTrafficSignalId("signal_name");
      traffic_signal_state->SetState("signal_state");
      phase->SetTrafficSignalStates(traffic_signal_states_);
      phases.push_back(phase);
    }
    impl->SetPhases(phases);
  }

  static ThreeTestSignals THREE_INDEPENDENT_SIGNALS()
  {
    ThreeTestSignals TEST_SIGNALS;
    TEST_SIGNALS.CREATE_PHASES(0, "TEST_SIGNAL_0", {100.0, 200.0, 300.0, 400.0});
    TEST_SIGNALS.CREATE_PHASES(1, "TEST_SIGNAL_1", {100.0, 200.0, 300.0, 400.0});
    TEST_SIGNALS.CREATE_PHASES(2, "TEST_SIGNAL_2", {100.0, 200.0, 300.0, 400.0});
    return TEST_SIGNALS;
  }

  std::vector<std::shared_ptr<ITrafficSignalController>> traffic_signals_{
      std::make_shared<TrafficSignalControllerImpl>(),
      std::make_shared<TrafficSignalControllerImpl>(),
      std::make_shared<TrafficSignalControllerImpl>()};

  std::vector<std::shared_ptr<ITrafficSignalStateWriter>> traffic_signal_states_{};

  const auto& operator()()
  {
    return traffic_signals_;
  }
};

///////////////////////////////////////////////////////////////////////////////

TEST(TrafficSignalNodeGeneration, GivenThreeSignals_WithFourPhases_GeneratesTree)
{
  auto TEST_SIGNALS = ThreeTestSignals::THREE_INDEPENDENT_SIGNALS();
  auto root = std::dynamic_pointer_cast<yase::StopAtNode>(OpenScenarioEngine::v1_3::parse(TEST_SIGNALS()));
  auto& traffic_signals = dynamic_cast<yase::ParallelNode&>(root->child());

  static constexpr size_t NR_OF_CONTROLLERS{3};
  static constexpr size_t NR_OF_PHASES{4};

  ASSERT_THAT(root, NotNull());
  ASSERT_THAT(traffic_signals.childrenCount(), NR_OF_CONTROLLERS);
  for (size_t i = 0; i < NR_OF_CONTROLLERS; ++i)
  {
    auto& controller_node = dynamic_cast<OpenScenarioEngine::v1_3::Node::RepeatingSequenceNode&>(traffic_signals.child(i));
    ASSERT_THAT(controller_node.childrenCount(), NR_OF_PHASES);
    for (size_t j = 0; j < NR_OF_CONTROLLERS; ++j)
    {
      ASSERT_NO_THROW([[maybe_unused]] auto& _ = dynamic_cast<OpenScenarioEngine::v1_3::Node::TrafficSignalPhaseNode&>(controller_node.child(j)));
    }
  }

  yase::printTree(*root);
}
