/*******************************************************************************
 * Copyright (c) 2023-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#pragma once

#include <gmock/gmock.h>

#include "Utils/IControllerService.h"

namespace testing::OpenScenarioEngine::v1_3
{
class MockControllerService : public ::OpenScenarioEngine::v1_3::IControllerService
{
public:
  MOCK_METHOD(std::optional<::OpenScenarioEngine::v1_3::EntityControllers>,
              GetControllers,
              (mantle_api::UniqueId),
              (const, override));
  MOCK_METHOD(mantle_api::UniqueId,
              GetControllerId,
              (std::optional<std::string>, const ::OpenScenarioEngine::v1_3::EntityControllers&),
              (const, override));
  MOCK_METHOD(void,
              ChangeState,
              (mantle_api::UniqueId, mantle_api::UniqueId, mantle_api::IController::LateralState, mantle_api::IController::LongitudinalState),
              (override));
  MOCK_METHOD(void,
              ResetControllerMappings,
              (),
              (override));
  MOCK_METHOD(std::vector<mantle_api::UniqueId>,
              GetControllerIds,
              (mantle_api::UniqueId entity_id),
              (override));
};

}  // namespace testing::OpenScenarioEngine::v1_3
