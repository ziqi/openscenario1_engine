/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <gmock/gmock.h>

#include "Utils/IProbabilityService.h"

namespace OpenScenarioEngine::v1_3
{
class MockProbabilityService : public IProbabilityService
{
public:
  MOCK_METHOD(double, SampleRealUniformValue, (double), (noexcept, override));

  static inline std::shared_ptr<MockProbabilityService> make_shared()
  {
    return std::make_shared<MockProbabilityService>();
  }
};

}  // namespace testing::OpenScenarioEngine::v1_3