/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/composite_node.h>
#include <gmock/gmock.h>

#include "Utils/EntityBroker.h"

namespace testing::OpenScenarioEngine::v1_3
{
class FakeCompositeNode : public yase::CompositeNode
{
public:
  FakeCompositeNode(std::shared_ptr<mantle_api::IEnvironment> environment, bool with_entity_broker = false)
      : CompositeNode("FakeCompositeNode"),
        environment_{environment},
        with_entity_broker_{with_entity_broker}
  {
    entity_broker_ = std::make_shared<::OpenScenarioEngine::v1_3::EntityBroker>(true);
  }
  MOCK_METHOD(void, onInit, (), (override));
  yase::NodeStatus tick() override { return yase::NodeStatus::kSuccess; }
  std::shared_ptr<::OpenScenarioEngine::v1_3::EntityBroker> getEntityBroker() { return entity_broker_; }

protected:
  void lookupAndRegisterData(yase::Blackboard &blackboard)
  {
    blackboard.set("TriggeringEntity", std::string("vehicle1"));
    blackboard.set("Environment", environment_);
    if (with_entity_broker_)
    {
      blackboard.set("EntityBroker", entity_broker_);
    }
  }

private:
  std::shared_ptr<mantle_api::IEnvironment> environment_{nullptr};
  bool with_entity_broker_{false};
  std::shared_ptr<::OpenScenarioEngine::v1_3::EntityBroker> entity_broker_;
};

}  // namespace testing::OpenScenarioEngine::v1_3
