/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Node/AnyTriggeringEntityNode.h"
#include "TestUtils.h"

using testing::OpenScenarioEngine::v1_3::OpenScenarioEngineLibraryTestBase;

class AnyTriggeringEntityNodeFixture : public OpenScenarioEngineLibraryTestBase
{
protected:
  void SetUp() override
  {
    OpenScenarioEngineLibraryTestBase::SetUp();
    auto engine_abort_flags = std::make_shared<OpenScenarioEngine::v1_3::EngineAbortFlags>(OpenScenarioEngine::v1_3::EngineAbortFlags::kNoAbort);
    auto entity_broker = std::make_shared<OpenScenarioEngine::v1_3::EntityBroker>(false);
    root_node_ = std::make_shared<testing::OpenScenarioEngine::v1_3::FakeRootNode>(env_, engine_abort_flags, entity_broker);
  }

  std::shared_ptr<testing::OpenScenarioEngine::v1_3::FakeRootNode> root_node_{nullptr};
};

class MockEntityConditionNode : public yase::DecoratorNode
{
public:
  MockEntityConditionNode()
      : DecoratorNode("") {}
  MOCK_METHOD(yase::NodeStatus, tick, (), (final));
};

TEST_F(AnyTriggeringEntityNodeFixture, GivenAnyTriggeringEntityNode_WhenChildReturnsFailed_ThenThrow)
{
  auto mock_entity_condition_node = std::make_shared<MockEntityConditionNode>();
  EXPECT_CALL(*mock_entity_condition_node, tick()).WillRepeatedly(::testing::Return(yase::NodeStatus::kFailure));
  auto any_triggering_entity_node_under_test = std::make_shared<OpenScenarioEngine::v1_3::Node::AnyTriggeringEntityNode>("SUT");
  any_triggering_entity_node_under_test->addChild(mock_entity_condition_node);

  root_node_->setChild(any_triggering_entity_node_under_test);
  root_node_->onInit();

  EXPECT_ANY_THROW(root_node_->executeTick());
}

TEST_F(AnyTriggeringEntityNodeFixture, GivenAnyTriggeringEntityNode_WhenTickMultipleTimes_ThenDoesntSaveStatus)
{
  auto mock_entity_condition_node = std::make_shared<MockEntityConditionNode>();
  EXPECT_CALL(*mock_entity_condition_node, tick())
      .WillOnce(::testing::Return(yase::NodeStatus::kSuccess))
      .WillRepeatedly(::testing::Return(yase::NodeStatus::kRunning));
  auto any_triggering_entity_node_under_test = std::make_shared<OpenScenarioEngine::v1_3::Node::AnyTriggeringEntityNode>("SUT");
  any_triggering_entity_node_under_test->addChild(mock_entity_condition_node);

  root_node_->setChild(any_triggering_entity_node_under_test);
  root_node_->onInit();

  EXPECT_THAT(root_node_->executeTick(), yase::NodeStatus::kSuccess);
  EXPECT_THAT(root_node_->executeTick(), yase::NodeStatus::kRunning);
}

class AnyTriggeringEntityNodeParamFixture : public ::testing::WithParamInterface<std::tuple<yase::NodeStatus, yase::NodeStatus, yase::NodeStatus>>,
                                            public AnyTriggeringEntityNodeFixture
{
};

INSTANTIATE_TEST_SUITE_P(EntityConditionReturnValues,
                         AnyTriggeringEntityNodeParamFixture,
                         ::testing::Values(
                             std::make_tuple(yase::NodeStatus::kRunning, yase::NodeStatus::kRunning, yase::NodeStatus::kRunning),
                             std::make_tuple(yase::NodeStatus::kSuccess, yase::NodeStatus::kRunning, yase::NodeStatus::kSuccess),
                             std::make_tuple(yase::NodeStatus::kRunning, yase::NodeStatus::kSuccess, yase::NodeStatus::kSuccess),
                             std::make_tuple(yase::NodeStatus::kSuccess, yase::NodeStatus::kSuccess, yase::NodeStatus::kSuccess)));

TEST_P(AnyTriggeringEntityNodeParamFixture, GivenAnyTriggeringEntityNode_WhenTick_ThenReturnSuccessOnAnyChildSuccess)
{
  auto mock_entity_condition_node = std::make_shared<MockEntityConditionNode>();
  EXPECT_CALL(*mock_entity_condition_node, tick()).WillRepeatedly(::testing::Return(std::get<0>(GetParam())));
  auto mock_entity_condition_node_2 = std::make_shared<MockEntityConditionNode>();
  EXPECT_CALL(*mock_entity_condition_node_2, tick()).WillRepeatedly(::testing::Return(std::get<1>(GetParam())));
  auto any_triggering_entity_node_under_test = std::make_shared<OpenScenarioEngine::v1_3::Node::AnyTriggeringEntityNode>("SUT");
  any_triggering_entity_node_under_test->addChild(mock_entity_condition_node);
  any_triggering_entity_node_under_test->addChild(mock_entity_condition_node_2);

  root_node_->setChild(any_triggering_entity_node_under_test);
  root_node_->onInit();

  EXPECT_THAT(root_node_->executeTick(), std::get<2>(GetParam()));
}
