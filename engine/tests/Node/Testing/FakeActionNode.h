/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <agnostic_behavior_tree/action_node.h>
#include <gmock/gmock.h>

namespace testing::OpenScenarioEngine::v1_3
{
inline void PrintTo(yase::NodeStatus status, ::std::ostream* os)
{
  if (status == yase::NodeStatus::kRunning) *os << "NodeStatus::kRunning";
  if (status == yase::NodeStatus::kSuccess) *os << "NodeStatus::kSuccess";
  if (status == yase::NodeStatus::kFailure) *os << "NodeStatus::kFailure";
  if (status == yase::NodeStatus::kIdle) *os << "NodeStatus::kIdle";
}

class FakeActionNode : public yase::ActionNode
{
public:
  FakeActionNode()
      : ActionNode("FakeActionNode"){};
  MOCK_METHOD(void, onInit, (), (override));
  MOCK_METHOD(yase::NodeStatus, tick, (), (override));

  static inline auto CREATE_PTR()
  {
    return std::make_shared<testing::NiceMock<FakeActionNode>>();
  }
};

}  // namespace testing::OpenScenarioEngine::v1_3