/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Node/Testing/FakeActionNode.h"
#include "Node/TriggerableCompositeNode.h"

using namespace OpenScenarioEngine::v1_3::Node;

TEST(TriggerableCompositeNode, GivenTriggerableCompositeNode_WhenTick_ThenTriggersAndChildAreTickedInRightOrder)
{
  auto node = OpenScenarioEngine::v1_3::Node::TriggerableCompositeNode("SUT");
  auto start_trigger = testing::OpenScenarioEngine::v1_3::FakeActionNode::CREATE_PTR();
  auto child = testing::OpenScenarioEngine::v1_3::FakeActionNode::CREATE_PTR();
  auto stop_trigger = testing::OpenScenarioEngine::v1_3::FakeActionNode::CREATE_PTR();
  node.set(child, StopTriggerPtr{stop_trigger}, StartTriggerPtr{start_trigger});

  {
    testing::InSequence s;
    EXPECT_CALL(*start_trigger, tick()).Times(1).WillOnce(testing::Return(yase::NodeStatus::kSuccess));
    EXPECT_CALL(*child, tick()).Times(1).WillOnce(testing::Return(yase::NodeStatus::kSuccess));
    EXPECT_CALL(*stop_trigger, tick()).Times(1).WillOnce(testing::Return(yase::NodeStatus::kSuccess));
  }
  node.executeTick();
}
