/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/catalog/CatalogHelperV1_3.h>

#include <filesystem>

#include "MantleAPI/Traffic/control_strategy.h"
#include "OpenScenarioEngine/OpenScenarioEngine.h"
#include "TestUtils.h"
#include "Utils/Constants.h"

using testing::OpenScenarioEngine::v1_3::OpenScenarioEngineTestBase;
using OpenScenarioEngine::v1_3::GetDefaultWeather;
using testing::OpenScenarioEngine::v1_3::GetScenariosPath;

using testing::_;
using testing::Return;
using testing::ReturnRef;
using testing::Pointee;
using testing::SaveArgPointee;

using namespace units::literals;
using namespace std::string_literals;

class OpenScenarioEngineTest : public OpenScenarioEngineTestBase
{
  protected:
    void SetUp() override {
        OpenScenarioEngineTestBase::SetUp();
        ON_CALL(controller_, GetName()).WillByDefault(ReturnRef(controller_name));
        ON_CALL(controller_, GetUniqueId()).WillByDefault(Return(1234));
    }

    std::string controller_name {"TestController"s};
};

TEST_F(OpenScenarioEngineTest, GivenValidScenarioFile_WhenInitialized_ThenDefaultRoutingBehaviourIsSetBeforeControllersAreCreated)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};

    {
        testing::InSequence s;
        EXPECT_CALL(*env_, SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute)).Times(1);
        EXPECT_CALL(env_->GetControllerRepository(), Create(_)).Times(2);
    }

    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_NO_THROW(engine.Init());
    engine.SetupDynamicContent();
}

TEST_F(OpenScenarioEngineTest, GivenValidScenarioFile_WhenInitializedAndSteppedOnce_ThenNoThrow)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};

    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.Init());
    engine.SetupDynamicContent();
    EXPECT_NO_THROW(engine.Step(1_ms));
}

MATCHER_P(EqualWeather, expected_weather, "The Weather does not match the default Weather")
{
    return arg.fog == expected_weather.fog && arg.precipitation == expected_weather.precipitation &&
           arg.illumination == expected_weather.illumination && arg.humidity == expected_weather.humidity &&
           arg.temperature == expected_weather.temperature &&
           arg.atmospheric_pressure == expected_weather.atmospheric_pressure;
}

TEST_F(OpenScenarioEngineTest, GivenValidScenarioFile_WhenEnvironmentDefaultInitialized_ThenVerifyDefaultWeatherSet)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_CALL(*env_, SetWeather(EqualWeather(GetDefaultWeather()))).Times(1);
    EXPECT_NO_THROW(engine.Init());
    engine.SetupDynamicContent();
}

TEST_F(OpenScenarioEngineTest, GivenOscVersion1_1ScenarioFile_WhenParsingScenario_ThenScenarioContainsNewFeature)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_1_test.xosc"};

    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.Init());

    OpenScenarioEngine::v1_3::OpenScenarioEngine::OpenScenarioPtr scenario_ptr = engine.GetScenario();

    ASSERT_TRUE(scenario_ptr != nullptr);
    auto file_header = scenario_ptr->GetFileHeader();
    ASSERT_TRUE(file_header != nullptr);
    auto license = file_header->GetLicense();
    ASSERT_TRUE(license != nullptr);

    EXPECT_EQ("GLWTPL", license->GetSpdxId());
}

TEST_F(OpenScenarioEngineTest, GivenOscVersion1_1ScenarioFile_WhenParsingScenario_ThenExpressionsAreResolved)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_1_test.xosc"};

    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.Init());

    OpenScenarioEngine::v1_3::OpenScenarioEngine::OpenScenarioPtr scenario_ptr = engine.GetScenario();

    ASSERT_TRUE(scenario_ptr != nullptr);
    auto open_scenario_category = scenario_ptr->GetOpenScenarioCategory();
    ASSERT_TRUE(open_scenario_category != nullptr);
    auto scenario_definition = open_scenario_category->GetScenarioDefinition();
    ASSERT_TRUE(scenario_definition != nullptr);

    // Expression in parameter declaration
    double value_parametrized = scenario_definition->GetStoryboard()
                                    ->GetInit()
                                    ->GetActions()
                                    ->GetPrivates()[0]
                                    ->GetPrivateActions()[1]
                                    ->GetLongitudinalAction()
                                    ->GetSpeedAction()
                                    ->GetSpeedActionTarget()
                                    ->GetAbsoluteTargetSpeed()
                                    ->GetValue();
    EXPECT_EQ(20.0, value_parametrized);

    // Expression in storyboard element
    auto value = scenario_definition->GetStoryboard()
                     ->GetStopTrigger()
                     ->GetConditionGroupsAtIndex(0)
                     ->GetConditionsAtIndex(0)
                     ->GetByValueCondition()
                     ->GetSimulationTimeCondition()
                     ->GetValue();
    EXPECT_EQ(300.0, value);
}

TEST_F(OpenScenarioEngineTest, GivenInvalidOscVersion1_3ScenarioFile_WhenParsingScenario_ThenThrow)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_3_test_invalid.xosc"};

    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_ANY_THROW(engine.Init());
}

TEST_F(OpenScenarioEngineTest, GivenValidOscVersion1_3ScenarioFile_WhenParsingScenario_ThenScenarioContainsNewFeature)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_3_test.xosc"};

    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.Init());

    OpenScenarioEngine::v1_3::OpenScenarioEngine::OpenScenarioPtr scenario_ptr = engine.GetScenario();

    ASSERT_TRUE(scenario_ptr != nullptr);
    auto scenario_objects = scenario_ptr->GetOpenScenarioCategory()->GetScenarioDefinition()->GetEntities()->GetScenarioObjects();
    ASSERT_NE(scenario_objects.size(), 0);
    auto trailer_coupler = scenario_objects[0]->GetEntityObject()->GetVehicle()->GetTrailerCoupler();
    ASSERT_TRUE(trailer_coupler != nullptr);

    EXPECT_EQ(trailer_coupler->GetDx(), -1.1);
    EXPECT_EQ(trailer_coupler->GetDz(), 0.3);
}

TEST_F(OpenScenarioEngineTest, GivenValidScenarioFile_WhenParsingScenario_ThenScenarioContainsHost)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};

    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.Init());

    OpenScenarioEngine::v1_3::OpenScenarioEngine::OpenScenarioPtr scenario_ptr = engine.GetScenario();

    ASSERT_TRUE(scenario_ptr != nullptr);
    const auto& scenario_objects =
        scenario_ptr->GetOpenScenarioCategory()->GetScenarioDefinition()->GetEntities()->GetScenarioObjects();

    ASSERT_EQ(1, scenario_objects.size());
    EXPECT_EQ("Ego", scenario_objects.front()->GetName());

    auto catalogue_ref = scenario_objects.front()->GetEntityObject()->GetCatalogReference();
    ASSERT_TRUE(catalogue_ref != nullptr);

    EXPECT_EQ("car_ego", catalogue_ref->GetEntryName());

    auto the_ref = catalogue_ref->GetRef();

    ASSERT_TRUE(the_ref != nullptr);
    ASSERT_TRUE(NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::IsVehicle(the_ref));
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithLogicFileReference_WhenInitScenario_ThenCreateMapIsCalled)
{
    const std::string relative_odr_file_path_{"../../Maps/xodr/ALKS_Road_Different_Curvatures.xodr"};
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_NO_THROW(engine.Init());
    auto scenario_info = engine.GetScenarioInfo();
    EXPECT_NE("", scenario_info.full_map_path);
}

bool IsMapDetailsWithLatLon(mantle_api::MapDetails& actual_map_details)
{
    const auto& map_region = actual_map_details.map_region;
    if (map_region.size() != 2)
    {
        return false;
    }

    if (const auto* lat_lon_position = std::get_if<mantle_api::LatLonPosition>(&map_region.front()))
    {
        if (*lat_lon_position != mantle_api::LatLonPosition{48.298065_deg, 11.583095_deg})
        {
            return false;
        }
    }

    if (const auto* lat_lon_position = std::get_if<mantle_api::LatLonPosition>(&map_region.back()))
    {
        if (*lat_lon_position != mantle_api::LatLonPosition{48.346524_deg, 11.720233_deg})
        {
            return false;
        }
    }

    return true;
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithUsedArea_WhenCallInit_ThenMapDetailsAreContainedInScenarioInfo)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) +
                               "AutomatedLaneKeepingSystemScenarios/ALKS_Scenario_4.1_1_FreeDriving_UsedArea_TEMPLATE.xosc"};
    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_NO_THROW(engine.Init());
    auto scenario_info = engine.GetScenarioInfo();
    EXPECT_TRUE(IsMapDetailsWithLatLon(*scenario_info.map_details));
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithEntities_WhenInitScenario_ThenCreateIsCalledInEnvironmentForEveryEntity)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);

    mantle_api::MockVehicle mock_vehicle{};

    mantle_api::VehicleProperties ego_properties{};
    ego_properties.type = mantle_api::EntityType::kVehicle;
    ego_properties.classification = mantle_api::VehicleClass::kMedium_car;
    ego_properties.model = "medium_car";
    ego_properties.bounding_box.dimension.length = 5.0_m;
    ego_properties.bounding_box.dimension.width = 2.0_m;
    ego_properties.bounding_box.dimension.height = 1.8_m;
    ego_properties.bounding_box.geometric_center.x = 1.4_m;
    ego_properties.bounding_box.geometric_center.y = 0.0_m;
    ego_properties.bounding_box.geometric_center.z = 0.9_m;
    ego_properties.performance.max_acceleration = 10_mps_sq;
    ego_properties.performance.max_deceleration = 10_mps_sq;
    ego_properties.performance.max_speed = 70_mps;
    ego_properties.front_axle.bb_center_to_axle_center = {1.58_m, 0.0_m, -0.5_m};
    ego_properties.front_axle.max_steering = 0.5_rad;
    ego_properties.front_axle.track_width = 1.68_m;
    ego_properties.front_axle.wheel_diameter = 0.8_m;
    ego_properties.rear_axle.bb_center_to_axle_center = {-1.4_m, 0.0_m, -0.5_m};
    ego_properties.rear_axle.max_steering = 0_rad;
    ego_properties.rear_axle.track_width = 1.68_m;
    ego_properties.rear_axle.wheel_diameter = 0.8_m;
    ego_properties.is_host = true;

    EXPECT_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(env_->GetEntityRepository()),
                Create(std::string("Ego"), ego_properties))
        .Times(1)
        .WillRepeatedly(ReturnRef(mock_vehicle));

    EXPECT_NO_THROW(engine.Init());
    engine.SetupDynamicContent();
}

TEST_F(OpenScenarioEngineTest,
       GivenScenarioWithExternallyControlledEntity_WhenInit_ThenControllerWithExternalConfigIsCreatedAndAssigned)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);

    mantle_api::MockController mock_default_controller;
    const mantle_api::UniqueId DEFAULT_CONTROLLER_ID {1234};
    const std::string DEFAULT_CONTROLLER_NAME {"TestDefaultController"};
    ON_CALL(mock_default_controller, GetUniqueId()).WillByDefault(Return(DEFAULT_CONTROLLER_ID));
    ON_CALL(mock_default_controller, GetName()).WillByDefault(ReturnRef(DEFAULT_CONTROLLER_NAME));

    mantle_api::MockController mock_user_defined_controller;
    const mantle_api::UniqueId USER_DEFINED_CONTROLLER_ID {5678};
    const std::string USER_DEFINED_CONTROLLER_NAME {"TestUserDefinedController"};
    ON_CALL(mock_user_defined_controller, GetUniqueId()).WillByDefault(Return(USER_DEFINED_CONTROLLER_ID));
    ON_CALL(mock_user_defined_controller, GetName()).WillByDefault(ReturnRef(USER_DEFINED_CONTROLLER_NAME));

    mantle_api::InternalControllerConfig internal_config_actual;
    mantle_api::ExternalControllerConfig external_config_actual;
    ON_CALL(env_->GetControllerRepository(), Create(_))
    .WillByDefault(::testing::Invoke([&](std::unique_ptr<mantle_api::IControllerConfig> received_controller) -> mantle_api::IController&
    {
        if (auto internal = dynamic_cast<mantle_api::InternalControllerConfig*>(received_controller.get()))
        {
            internal_config_actual = *internal; // copy assignment, as we drop the unique_ptr
            return mock_default_controller;
        }

        if (auto external = dynamic_cast<mantle_api::ExternalControllerConfig*>(received_controller.get()))
        {
            external_config_actual = *external; // copy assignment, as we drop the unique_ptr
            return mock_user_defined_controller;
        }

        throw std::runtime_error("Untested ControllerConfigType");
    }));

    EXPECT_CALL(*env_, AddEntityToController(_, DEFAULT_CONTROLLER_ID));
    EXPECT_CALL(*env_, AddEntityToController(_, USER_DEFINED_CONTROLLER_ID));

    EXPECT_NO_THROW(engine.Init());
    engine.SetupDynamicContent();

    mantle_api::InternalControllerConfig internal_config_expected{};
    internal_config_expected.name = OpenScenarioEngine::v1_3::CONTROLLER_NAME_DEFAULT;
    internal_config_expected.control_strategies = {
        std::make_shared<mantle_api::KeepVelocityControlStrategy>(),
        std::make_shared<mantle_api::KeepLaneOffsetControlStrategy>()};

    mantle_api::ExternalControllerConfig external_config_expected{};
    external_config_expected.name = "ALKSController",
    external_config_expected.parameters.emplace("controller_property", "3");

    EXPECT_EQ(internal_config_expected, internal_config_actual);
    EXPECT_EQ(external_config_expected, external_config_actual);
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithInitTeleportAction_WhenInitAndStepEngine_ThenSetPositionIsCalled)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(_))
        .Times(1);

    EXPECT_NO_THROW(engine.Init());
    engine.SetupDynamicContent();
    EXPECT_NO_THROW(engine.Step(1_ms));
}

TEST_F(OpenScenarioEngineTest, GivenExistingScenarioFileWithWrongCatalog_WhenValidateScenario_ThenReturnsNumberofErrors)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "scenario_with_wrong_catalog_path.xosc"};
    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_EQ(3, engine.ValidateScenario());
}

TEST_F(OpenScenarioEngineTest, GivenExistingScenarioFileWithWrongCatalog_WhenValidateScenario_ThenReturnsZero)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_EQ(0, engine.ValidateScenario());
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithOneSimulationTimeStopTrigger_WhenGetDuration_ThenReturnsDuration)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_1_test.xosc"};

    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.Init());

    EXPECT_EQ(engine.GetScenarioInfo().scenario_timeout_duration, units::time::second_t(300.0));
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithTwoSimulationTimeStopTrigger_WhenGetDuration_ThenReturnsMaxDuration)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_1_test_TwoSimulationTimeConditions.xosc"};

    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.Init());

    EXPECT_EQ(engine.GetScenarioInfo().scenario_timeout_duration, units::time::second_t(300.0));
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithNoSimulationTimeStopTrigger_WhenGetDuration_ThenReturnsMaxDouble)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_1_test_NoSimulationTimeCondition.xosc"};

    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.Init());

    EXPECT_EQ(engine.GetScenarioInfo().scenario_timeout_duration, mantle_api::Time{std::numeric_limits<double>::max()});
}

TEST_F(OpenScenarioEngineTest, GivenValidScenarioFile_WhenActivateExternalHostControlIsCalledWithoutInit_ThenNoThrow)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};

    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.ActivateExternalHostControl());
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithSceneGraphFile_WhenParsingScenario_ThenSceneGraphFileRead)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_1_test.xosc"};

    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.Init());

    EXPECT_EQ(engine.GetScenarioInfo().map_model_reference, "/File/Path/To/Unexisted/3D/ModelFile");
}

class MapRegionTypeParamFixture : public ::testing::WithParamInterface<std::tuple<std::string, mantle_api::MapRegionType>>,
                                  public OpenScenarioEngineTest
{
};

TEST_P(MapRegionTypeParamFixture, GivenScenarioWithUsedAreaType_WhenCallGetScenarioInfo_ThenCorrectMapRegionType)
{
    const auto [scenario, map_region_type] = GetParam();
    std::string xosc_file_path{GetScenariosPath(testing::UnitTest::GetInstance()->current_test_info()->file()) + scenario};
    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_NO_THROW(engine.Init());
    auto scenario_info = engine.GetScenarioInfo();
    EXPECT_EQ(scenario_info.map_details->map_region_type, map_region_type);
}

INSTANTIATE_TEST_SUITE_P(MapRegionTypeTests,
                         MapRegionTypeParamFixture,
                         ::testing::Values(
                             std::make_tuple("used_area_type_polygon.xosc", mantle_api::MapRegionType::kPolygon),
                             std::make_tuple("used_area_type_route.xosc", mantle_api::MapRegionType::kRoute),
                             std::make_tuple("used_area_type_undefined.xosc", mantle_api::MapRegionType::kUndefined)));

TEST_F(OpenScenarioEngineTest, GivenScenarioWithWrongUsedAreaType_WhenCallGetScenarioInfo_ThenThrowError)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) +
                               "wrong_used_area_type.xosc"};
    OpenScenarioEngine::v1_3::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_NO_THROW(engine.Init());
    EXPECT_THROW(engine.GetScenarioInfo(), std::runtime_error);
}
