/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Utils/Ellipse.h"

#include <gtest/gtest.h>

using namespace units::literals;
using namespace OpenScenarioEngine::v1_3;

namespace
{
const mantle_api::Vec3<units::length::meter_t> kEllipseCenter{0_m, 0_m, 0_m};

struct EllipseTestParameters
{
    mantle_api::Vec3<units::length::meter_t> query_point;
    units::length::meter_t ellipse_semi_major_axis;
    units::length::meter_t ellipse_semi_minor_axis;
    units::angle::radian_t ellipse_angle;
};
}

using EllipseTestPointsOutside = testing::TestWithParam<EllipseTestParameters>;
using EllipseTestPointsInside = testing::TestWithParam<EllipseTestParameters>;

INSTANTIATE_TEST_SUITE_P(TestingPointsOutsideOfEllipse,
                         EllipseTestPointsOutside,
                         testing::Values(EllipseTestParameters{{5_m, 5_m, 0_m}, 5.0_m, 5.0_m, 0_rad},
                                         EllipseTestParameters{{5_m, 5_m, 0_m}, 5.0_m, 5.0_m, units::angle::radian_t(M_PI_2)},
                                         EllipseTestParameters{{5_m, 5_m, 0_m}, 5.0_m, 5.0_m, units::angle::radian_t(-1.0 * M_PI_2)},
                                         EllipseTestParameters{{5.1_m, 0_m, 0_m}, 10.0_m, 5.0_m, units::angle::radian_t(M_PI_2)},
                                         EllipseTestParameters{{5.1_m, 0_m, 0_m}, 10.0_m, 5.0_m, units::angle::radian_t(-1.0 * M_PI_2)},
                                         EllipseTestParameters{{0_m, 5.1_m, 0_m}, 5.0_m, 10.0_m, units::angle::radian_t(M_PI_2)},
                                         EllipseTestParameters{{0_m, 5.1_m, 0_m}, 5.0_m, 10.0_m, units::angle::radian_t(-1.0 * M_PI_2)}));

INSTANTIATE_TEST_SUITE_P(TestingPointsInsideOfEllipse,
                         EllipseTestPointsInside,
                         testing::Values(EllipseTestParameters{{3_m, 3_m, 0_m}, 5.0_m, 5.0_m, 0_rad},
                                         EllipseTestParameters{{3_m, 3_m, 0_m}, 5.0_m, 5.0_m, units::angle::radian_t(M_PI_2)},
                                         EllipseTestParameters{{3_m, 3_m, 0_m}, 5.0_m, 5.0_m, units::angle::radian_t(-1.0 * M_PI_2)},
                                         EllipseTestParameters{{5_m, 0_m, 0_m}, 5.0_m, 5.0_m, 0_rad},
                                         EllipseTestParameters{{5.1_m, 0_m, 0_m}, 10.0_m, 5.0_m, 0_rad},
                                         EllipseTestParameters{{0_m, 5.1_m, 0_m}, 5.0_m, 10.0_m, 0_rad}));

TEST_P(EllipseTestPointsOutside, GivenQueryPointsAndEllipseParameters_WhenCheckingIfQueryPointIsOutsideOfEllipse_ThenReturnTrue)
{
    const auto [query_point, major_axis, minor_axis, angle] = GetParam();

    const bool point_outside_of_ellipse{IsPointOutsideOfEllipse(query_point,
                                                                kEllipseCenter,
                                                                major_axis,
                                                                minor_axis,
                                                                angle)};

    EXPECT_TRUE(point_outside_of_ellipse);
}

TEST_P(EllipseTestPointsInside, GivenQueryPointsAndEllipseParameters_WhenCheckingIfQueryPointIsOutsideOfEllipse_ThenReturnFalse)
{
    const auto [query_point, major_axis, minor_axis, angle] = GetParam();

    const bool point_outside_of_ellipse{IsPointOutsideOfEllipse(query_point,
                                                                kEllipseCenter,
                                                                major_axis,
                                                                minor_axis,
                                                                angle)};

    EXPECT_FALSE(point_outside_of_ellipse);
}
