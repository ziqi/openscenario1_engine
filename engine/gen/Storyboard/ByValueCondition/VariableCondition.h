/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>
#include <utility>

#include "Storyboard/ByValueCondition/VariableCondition_impl.h"

namespace OpenScenarioEngine::v1_3::Node
{
class VariableCondition : public yase::ActionNode
{
public:
  VariableCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IVariableCondition> variableCondition)
      : yase::ActionNode{"VariableCondition"},
        variableCondition_{variableCondition}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    return impl_->IsSatisfied() ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");

    impl_ = std::make_unique<OpenScenarioEngine::v1_3::VariableCondition>(
        OpenScenarioEngine::v1_3::VariableCondition::Values{
            ConvertScenarioRule(variableCondition_->GetRule(), variableCondition_->GetValue())},
        OpenScenarioEngine::v1_3::VariableCondition::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_3::VariableCondition> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IVariableCondition> variableCondition_;
};

}  // namespace OpenScenarioEngine::v1_3::Node
