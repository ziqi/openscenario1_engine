/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <cassert>
#include <utility>

#include "Storyboard/GenericAction/OverrideSteeringWheelAction_impl.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_3::Node
{
class OverrideSteeringWheelAction : public yase::ActionNode
{
public:
  OverrideSteeringWheelAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IOverrideSteeringWheelAction> overrideSteeringWheelAction)
      : yase::ActionNode{"OverrideSteeringWheelAction"},
        overrideSteeringWheelAction_{overrideSteeringWheelAction}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    std::shared_ptr<mantle_api::IEnvironment> environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const EntityBroker::Ptr entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_3::OverrideSteeringWheelAction>(
        OpenScenarioEngine::v1_3::OverrideSteeringWheelAction::Values{
            entityBroker->GetEntities(),
            overrideSteeringWheelAction_->GetActive(),
            overrideSteeringWheelAction_->GetMaxRate(),
            overrideSteeringWheelAction_->GetMaxTorque(),
            overrideSteeringWheelAction_->GetValue()},
        OpenScenarioEngine::v1_3::OverrideSteeringWheelAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_3::OverrideSteeringWheelAction> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IOverrideSteeringWheelAction> overrideSteeringWheelAction_;
};

}  // namespace OpenScenarioEngine::v1_3::Node
