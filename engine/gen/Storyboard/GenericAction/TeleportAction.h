/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <cassert>
#include <utility>

#include "Storyboard/GenericAction/TeleportAction_impl.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_3::Node
{
class TeleportAction : public yase::ActionNode
{
public:
  TeleportAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITeleportAction> teleportAction)
      : yase::ActionNode{"TeleportAction"},
        teleportAction_{teleportAction}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    std::shared_ptr<mantle_api::IEnvironment> environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const EntityBroker::Ptr entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_3::TeleportAction>(
        OpenScenarioEngine::v1_3::TeleportAction::Values{
            entityBroker->GetEntities(),
            [=]()
            { return ConvertScenarioPosition(environment, teleportAction_->GetPosition()); }},
        OpenScenarioEngine::v1_3::TeleportAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_3::TeleportAction> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITeleportAction> teleportAction_;
};

}  // namespace OpenScenarioEngine::v1_3::Node
