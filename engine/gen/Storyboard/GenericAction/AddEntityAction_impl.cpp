/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/AddEntityAction_impl.h"

#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{

bool AddEntityAction::Step()
{
  // Note:
  // - Access to values parse to mantle/ose datatypes: this->values.xxx
  // - Access to mantle interfaces: this->mantle.xxx
  Logger::Error("Method AddEntityAction::Step() not implemented yet (returning \"true\" by default)");
  return true;
}

}  // namespace OpenScenarioEngine::v1_3
