/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "Storyboard/MotionControlAction/SynchronizeAction_base.h"

namespace OpenScenarioEngine::v1_3
{

class SynchronizeAction : public SynchronizeActionBase
{
public:
  SynchronizeAction(Values values, Interfaces interfaces)
      : SynchronizeActionBase{std::move(values), interfaces} {};

  void SetControlStrategy() override;
  bool HasControlStrategyGoalBeenReached(const std::string& actor) override;
  mantle_api::MovementDomain GetMovementDomain() const override;
};

}  // namespace OpenScenarioEngine::v1_3