/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseTrafficSignalAction.h"

#include <memory>

#include "Conversion/OscToNode/ParseTrafficSignalControllerAction.h"
#include "Conversion/OscToNode/ParseTrafficSignalStateAction.h"

namespace OpenScenarioEngine::v1_3
{
yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficSignalAction> trafficSignalAction)
{
  if (auto element = trafficSignalAction->GetTrafficSignalControllerAction(); element)
  {
    return parse(element);
  }
  if (auto element = trafficSignalAction->GetTrafficSignalStateAction(); element)
  {
    return parse(element);
  }
  throw std::runtime_error("Corrupted openSCENARIO file: No choice made within std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficSignalAction>");
}

}  // namespace OpenScenarioEngine::v1_3
