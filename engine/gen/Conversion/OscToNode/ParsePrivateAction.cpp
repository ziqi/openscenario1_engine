/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParsePrivateAction.h"

#include <memory>

#include "Conversion/OscToNode/ParseActivateControllerAction.h"
#include "Conversion/OscToNode/ParseAppearanceAction.h"
#include "Conversion/OscToNode/ParseControllerAction.h"
#include "Conversion/OscToNode/ParseLateralAction.h"
#include "Conversion/OscToNode/ParseLongitudinalAction.h"
#include "Conversion/OscToNode/ParseRoutingAction.h"
#include "Conversion/OscToNode/ParseSynchronizeAction.h"
#include "Conversion/OscToNode/ParseTeleportAction.h"
#include "Conversion/OscToNode/ParseTrailerAction.h"
#include "Conversion/OscToNode/ParseVisibilityAction.h"

namespace OpenScenarioEngine::v1_3
{
yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPrivateAction> privateAction)
{
  if (auto element = privateAction->GetActivateControllerAction(); element)
  {
    return parse(element);
  }
  if (auto element = privateAction->GetAppearanceAction(); element)
  {
    return parse(element);
  }
  if (auto element = privateAction->GetControllerAction(); element)
  {
    return parse(element);
  }
  if (auto element = privateAction->GetLateralAction(); element)
  {
    return parse(element);
  }
  if (auto element = privateAction->GetLongitudinalAction(); element)
  {
    return parse(element);
  }
  if (auto element = privateAction->GetRoutingAction(); element)
  {
    return parse(element);
  }
  if (auto element = privateAction->GetSynchronizeAction(); element)
  {
    return parse(element);
  }
  if (auto element = privateAction->GetTeleportAction(); element)
  {
    return parse(element);
  }
  if (auto element = privateAction->GetTrailerAction(); element)
  {
    return parse(element);
  }
  if (auto element = privateAction->GetVisibilityAction(); element)
  {
    return parse(element);
  }
  throw std::runtime_error("Corrupted openSCENARIO file: No choice made within std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPrivateAction>");
}

}  // namespace OpenScenarioEngine::v1_3
