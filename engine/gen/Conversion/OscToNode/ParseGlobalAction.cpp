/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseGlobalAction.h"

#include <memory>

#include "Conversion/OscToNode/ParseEntityAction.h"
#include "Conversion/OscToNode/ParseEnvironmentAction.h"
#include "Conversion/OscToNode/ParseInfrastructureAction.h"
#include "Conversion/OscToNode/ParseParameterAction.h"
#include "Conversion/OscToNode/ParseSetMonitorAction.h"
#include "Conversion/OscToNode/ParseTrafficAction.h"
#include "Conversion/OscToNode/ParseVariableAction.h"

namespace OpenScenarioEngine::v1_3
{
yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IGlobalAction> globalAction)
{
  if (auto element = globalAction->GetEntityAction(); element)
  {
    return parse(element);
  }
  if (auto element = globalAction->GetEnvironmentAction(); element)
  {
    return parse(element);
  }
  if (auto element = globalAction->GetInfrastructureAction(); element)
  {
    return parse(element);
  }
  if (auto element = globalAction->GetParameterAction(); element)
  {
    return parse(element);
  }
  if (auto element = globalAction->GetSetMonitorAction(); element)
  {
    return parse(element);
  }
  if (auto element = globalAction->GetTrafficAction(); element)
  {
    return parse(element);
  }
  if (auto element = globalAction->GetVariableAction(); element)
  {
    return parse(element);
  }
  throw std::runtime_error("Corrupted openSCENARIO file: No choice made within std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IGlobalAction>");
}

}  // namespace OpenScenarioEngine::v1_3
