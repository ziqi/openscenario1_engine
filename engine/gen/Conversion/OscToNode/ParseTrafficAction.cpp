/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseTrafficAction.h"

#include <memory>

#include "Conversion/OscToNode/ParseTrafficAreaAction.h"
#include "Conversion/OscToNode/ParseTrafficSinkAction.h"
#include "Conversion/OscToNode/ParseTrafficSourceAction.h"
#include "Conversion/OscToNode/ParseTrafficStopAction.h"
#include "Conversion/OscToNode/ParseTrafficSwarmAction.h"

namespace OpenScenarioEngine::v1_3
{
yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficAction> trafficAction)
{
  if (auto element = trafficAction->GetTrafficAreaAction(); element)
  {
    return parse(element);
  }
  if (auto element = trafficAction->GetTrafficSinkAction(); element)
  {
    return parse(element);
  }
  if (auto element = trafficAction->GetTrafficSourceAction(); element)
  {
    return parse(element);
  }
  if (auto element = trafficAction->GetTrafficStopAction(); element)
  {
    return parse(element);
  }
  if (auto element = trafficAction->GetTrafficSwarmAction(); element)
  {
    return parse(element);
  }
  throw std::runtime_error("Corrupted openSCENARIO file: No choice made within std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficAction>");
}

}  // namespace OpenScenarioEngine::v1_3
